Event	ID	Name			Wwise Object Path	Notes
	187168666	Leader_Jadwiga_Foley_HappyNeutral			\DLC02\DLC02_Leader_SFX\Jadwiga\Leader_Jadwiga_Foley_HappyNeutral	
	247641572	Leader_Jadwiga_Foley_Defeat			\DLC02\DLC02_Leader_SFX\Jadwiga\Leader_Jadwiga_Foley_Defeat	
	271342051	Leader_Jadwiga_Foley_NeutralUnhappy			\DLC02\DLC02_Leader_SFX\Jadwiga\Leader_Jadwiga_Foley_NeutralUnhappy	
	271358793	Leader_Jadwiga_Foley_NeutralNegA			\DLC02\DLC02_Leader_SFX\Jadwiga\Leader_Jadwiga_Foley_NeutralNegA	
	271358794	Leader_Jadwiga_Foley_NeutralNegB			\DLC02\DLC02_Leader_SFX\Jadwiga\Leader_Jadwiga_Foley_NeutralNegB	
	324254387	Leader_Jadwiga_Foley_Warning			\DLC02\DLC02_Leader_SFX\Jadwiga\Leader_Jadwiga_Foley_Warning	
	576994969	Leader_Jadwiga_Foley_UnhappyNegA			\DLC02\DLC02_Leader_SFX\Jadwiga\Leader_Jadwiga_Foley_UnhappyNegA	
	576994970	Leader_Jadwiga_Foley_UnhappyNegB			\DLC02\DLC02_Leader_SFX\Jadwiga\Leader_Jadwiga_Foley_UnhappyNegB	
	929278077	Leader_Jadwiga_Foley_HappyEnraged			\DLC02\DLC02_Leader_SFX\Jadwiga\Leader_Jadwiga_Foley_HappyEnraged	
	1012509461	Leader_Jadwiga_Foley_Kudos			\DLC02\DLC02_Leader_SFX\Jadwiga\Leader_Jadwiga_Foley_Kudos	
	1562129029	Leader_Jadwiga_Foley_UnhappyPosA			\DLC02\DLC02_Leader_SFX\Jadwiga\Leader_Jadwiga_Foley_UnhappyPosA	
	1562129030	Leader_Jadwiga_Foley_UnhappyPosB			\DLC02\DLC02_Leader_SFX\Jadwiga\Leader_Jadwiga_Foley_UnhappyPosB	
	2077037232	Leader_Jadwiga_Foley_NeutralEnraged			\DLC02\DLC02_Leader_SFX\Jadwiga\Leader_Jadwiga_Foley_NeutralEnraged	
	2082412387	Leader_Jadwiga_Foley_UnhappyNeutral			\DLC02\DLC02_Leader_SFX\Jadwiga\Leader_Jadwiga_Foley_UnhappyNeutral	
	2107278596	Leader_Jadwiga_Foley_FirstMeet			\DLC02\DLC02_Leader_SFX\Jadwiga\Leader_Jadwiga_Foley_FirstMeet	
	2815114253	Leader_Jadwiga_Foley_HappyPosB			\DLC02\DLC02_Leader_SFX\Jadwiga\Leader_Jadwiga_Foley_HappyPosB	
	2815114254	Leader_Jadwiga_Foley_HappyPosA			\DLC02\DLC02_Leader_SFX\Jadwiga\Leader_Jadwiga_Foley_HappyPosA	
	3079461096	Leader_Jadwiga_Foley_NeutralHappy			\DLC02\DLC02_Leader_SFX\Jadwiga\Leader_Jadwiga_Foley_NeutralHappy	
	3318149760	Leader_Jadwiga_Foley_UnhappyEnraged			\DLC02\DLC02_Leader_SFX\Jadwiga\Leader_Jadwiga_Foley_UnhappyEnraged	
	3371772085	Leader_Jadwiga_Foley_NeutralPosA			\DLC02\DLC02_Leader_SFX\Jadwiga\Leader_Jadwiga_Foley_NeutralPosA	
	3371772086	Leader_Jadwiga_Foley_NeutralPosB			\DLC02\DLC02_Leader_SFX\Jadwiga\Leader_Jadwiga_Foley_NeutralPosB	
	3459808583	Leader_Jadwiga_Foley_DeclareWarFromAI			\DLC02\DLC02_Leader_SFX\Jadwiga\Leader_Jadwiga_Foley_DeclareWarFromAI	
	3483921693	Leader_Jadwiga_Foley_HappyNegB			\DLC02\DLC02_Leader_SFX\Jadwiga\Leader_Jadwiga_Foley_HappyNegB	
	3483921694	Leader_Jadwiga_Foley_HappyNegA			\DLC02\DLC02_Leader_SFX\Jadwiga\Leader_Jadwiga_Foley_HappyNegA	
	4175764672	Leader_Jadwiga_Foley_DeclareWarFromHuman			\DLC02\DLC02_Leader_SFX\Jadwiga\Leader_Jadwiga_Foley_DeclareWarFromHuman	

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	10883666	Leader-Jadwiga-Foley-Defeat	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Leader-Jadwiga-Foley-Defeat_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Leader SFX\Leader_SFX\DLC_EXP\DLC2\Jadwiga\Leader-Jadwiga-Foley-Defeat		1341684
	119838061	Leader-Jadwiga-Foley-DeclareWarFromHuman	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Leader-Jadwiga-Foley-DeclareWarFromHuman_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Leader SFX\Leader_SFX\DLC_EXP\DLC2\Jadwiga\Leader-Jadwiga-Foley-DeclareWarFromHuman		1609892
	120466770	Leader-Jadwiga-Foley-Kudos	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Leader-Jadwiga-Foley-Kudos_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Leader SFX\Leader_SFX\DLC_EXP\DLC2\Jadwiga\Leader-Jadwiga-Foley-Kudos		1289788
	143890592	Leader-Jadwiga-Foley-UnhappyNegA	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Leader-Jadwiga-Foley-UnhappyNegA_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Leader SFX\Leader_SFX\DLC_EXP\DLC2\Jadwiga\Leader-Jadwiga-Foley-UnhappyNegA		434704
	180456378	Leader-Jadwiga-Foley-UnhappyPosA	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Leader-Jadwiga-Foley-UnhappyPosA_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Leader SFX\Leader_SFX\DLC_EXP\DLC2\Jadwiga\Leader-Jadwiga-Foley-UnhappyPosA		492008
	202906353	Leader-Jadwiga-Foley-NeutralHappy	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Leader-Jadwiga-Foley-NeutralHappy_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Leader SFX\Leader_SFX\DLC_EXP\DLC2\Jadwiga\Leader-Jadwiga-Foley-NeutralHappy		366976
	301382001	Leader-Jadwiga-Foley-NeutralPosB	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Leader-Jadwiga-Foley-NeutralPosB_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Leader SFX\Leader_SFX\DLC_EXP\DLC2\Jadwiga\Leader-Jadwiga-Foley-NeutralPosB		549492
	322324084	Leader-Jadwiga-Foley-NeutralNegA	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Leader-Jadwiga-Foley-NeutralNegA_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Leader SFX\Leader_SFX\DLC_EXP\DLC2\Jadwiga\Leader-Jadwiga-Foley-NeutralNegA		666152
	347434842	Leader-Jadwiga-Foley-Warning	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Leader-Jadwiga-Foley-Warning_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Leader SFX\Leader_SFX\DLC_EXP\DLC2\Jadwiga\Leader-Jadwiga-Foley-Warning		994960
	375877611	Leader-Jadwiga-Foley-NeutralUnhappy	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Leader-Jadwiga-Foley-NeutralUnhappy_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Leader SFX\Leader_SFX\DLC_EXP\DLC2\Jadwiga\Leader-Jadwiga-Foley-NeutralUnhappy		607420
	388842840	Leader-Jadwiga-Foley-HappyEnraged	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Leader-Jadwiga-Foley-HappyEnraged_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Leader SFX\Leader_SFX\DLC_EXP\DLC2\Jadwiga\Leader-Jadwiga-Foley-HappyEnraged		738772
	395407691	Leader-Jadwiga-Foley-DeclareWarFromAI	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Leader-Jadwiga-Foley-DeclareWarFromAI_24309D6E.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Leader SFX\Leader_SFX\DLC_EXP\DLC2\Jadwiga\Leader-Jadwiga-Foley-DeclareWarFromAI		1382184
	424335410	Leader-Jadwiga-Foley-HappyPosA	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Leader-Jadwiga-Foley-HappyPosA_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Leader SFX\Leader_SFX\DLC_EXP\DLC2\Jadwiga\Leader-Jadwiga-Foley-HappyPosA		634164
	453610870	Leader-Jadwiga-Foley-NeutralNegB	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Leader-Jadwiga-Foley-NeutralNegB_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Leader SFX\Leader_SFX\DLC_EXP\DLC2\Jadwiga\Leader-Jadwiga-Foley-NeutralNegB		560784
	479772827	Leader-Jadwiga-Foley-UnhappyPosB	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Leader-Jadwiga-Foley-UnhappyPosB_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Leader SFX\Leader_SFX\DLC_EXP\DLC2\Jadwiga\Leader-Jadwiga-Foley-UnhappyPosB		491972
	670489061	Leader-Jadwiga-Foley-UnhappyNegB	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Leader-Jadwiga-Foley-UnhappyNegB_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Leader SFX\Leader_SFX\DLC_EXP\DLC2\Jadwiga\Leader-Jadwiga-Foley-UnhappyNegB		434704
	682391375	Leader-Jadwiga-Foley-HappyNeutral	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Leader-Jadwiga-Foley-HappyNeutral_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Leader SFX\Leader_SFX\DLC_EXP\DLC2\Jadwiga\Leader-Jadwiga-Foley-HappyNeutral		724480
	805789914	Leader-Jadwiga-Foley-HappyPosB	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Leader-Jadwiga-Foley-HappyPosB_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Leader SFX\Leader_SFX\DLC_EXP\DLC2\Jadwiga\Leader-Jadwiga-Foley-HappyPosB		613468
	828958946	Leader-Jadwiga-Foley-UnhappyEnraged	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Leader-Jadwiga-Foley-UnhappyEnraged_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Leader SFX\Leader_SFX\DLC_EXP\DLC2\Jadwiga\Leader-Jadwiga-Foley-UnhappyEnraged		757876
	882676117	Leader-Jadwiga-Foley-NeutralPosA	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Leader-Jadwiga-Foley-NeutralPosA_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Leader SFX\Leader_SFX\DLC_EXP\DLC2\Jadwiga\Leader-Jadwiga-Foley-NeutralPosA		466704
	925376490	Leader-Jadwiga-Foley-UnhappyNeutral	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Leader-Jadwiga-Foley-UnhappyNeutral_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Leader SFX\Leader_SFX\DLC_EXP\DLC2\Jadwiga\Leader-Jadwiga-Foley-UnhappyNeutral		699140
	933013832	Leader-Jadwiga-Foley-FirstMeet	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Leader-Jadwiga-Foley-FirstMeet_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Leader SFX\Leader_SFX\DLC_EXP\DLC2\Jadwiga\Leader-Jadwiga-Foley-FirstMeet		1012368
	983699866	Leader-Jadwiga-Foley-NeutralEnraged	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Leader-Jadwiga-Foley-NeutralEnraged_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Leader SFX\Leader_SFX\DLC_EXP\DLC2\Jadwiga\Leader-Jadwiga-Foley-NeutralEnraged		719984
	1007094767	Leader-Jadwiga-Foley-HappyNegB	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Leader-Jadwiga-Foley-HappyNegB_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Leader SFX\Leader_SFX\DLC_EXP\DLC2\Jadwiga\Leader-Jadwiga-Foley-HappyNegB		775284
	1022123863	Leader-Jadwiga-Foley-HappyNegA	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Leader-Jadwiga-Foley-HappyNegA_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Leader SFX\Leader_SFX\DLC_EXP\DLC2\Jadwiga\Leader-Jadwiga-Foley-HappyNegA		684968

