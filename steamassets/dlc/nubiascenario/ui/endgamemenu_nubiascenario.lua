
-- Include new entry for Nubian Scenario victory
Styles["VICTORY_NUBIA_SCENARIO"] = {
	RibbonIcon = "ICON_VICTORY_RELIGIOUS",
	Ribbon = "EndGame_Ribbon_Religion",
	RibbonTile = "EndGame_RibbonTile_Religion",
	Background = "EndGame_BG_Religion",
	Movie = "Religion.bk2",
	SndStart = "Play_Cinematic_Endgame_Religion",
	SndStop = "Stop_Cinematic_Endgame_Religion",
	Color = "COLOR_VICTORY_RELIGION",
};
