include "SupportFunctions.lua"

-- ===========================================================================
--	Nubia Scenario
-- ===========================================================================

-- Script globals
local eEgyptPlayer = -1;
local eNubiaPlayer = -1;
local eHyksosPlayer = 2;
local eLibyaPlayer = 3;
local eAssyriaPlayer = 4;
local ePersiaPlayer = 5;
local eMacedonPlayer = 6;
local eRomePlayer = 7;
local eAshumeinPlayer = eRomePlayer + 1;
local eBahariyaPlayer = eRomePlayer + 2;
local eByblosPlayer = eRomePlayer + 3;
local eDakhlaPlayer = eRomePlayer + 4;
local eIhnasiyaPlayer = eRomePlayer + 5;
local eKhargaPlayer = eRomePlayer + 6;
local eNekhenPlayer = eRomePlayer + 7;
local eOnPlayer = eRomePlayer + 8;
local ePuntPlayer = eRomePlayer + 9;
local eSerabitPlayer = eRomePlayer + 10;
local eSharuhenPlayer = eRomePlayer + 11;
local eThinisPlayer = eRomePlayer + 12;
local eIremPlayer = eRomePlayer + 13;

-- SCENARIO CONSTANTS
-- Specific coordinates
local iEgyptCapitalX = 20;
local iEgyptCapitalY = 45;
local iNubiaCapitalX = 11;
local iNubiaCapitalY = 18;
local iHyksosStartX = 29;
local iHyksosStartY = 77;
local iLibyaStartX = 1;
local iLibyaStartY = 70;
local iSeaPeopleStartX = iHyksosStartX;
local iSeaPeopleStartY = iHyksosStartY;
local iAssyriaStartX = 34;
local iAssyriaStartY = 81;
local iPersiaStartX = 34;
local iPersiaStartY = 87;
local iMacedonStartX = 34;
local iMacedonStartY = 84;
local iRomeStartX = 9;
local iRomeStartY = 71;
local g_iW = 36;
local g_iH = 90;
-- Turns of entry
local iHyksosInvasionTurn = 26;
local iLibyaInvasionTurn = 49;
local iAssyriaInvasionTurn = 74;
local iPersiaInvasionTurn = 81;
local iMacedonInvasionTurn = 91;
local iRomeInvasionTurn = 106;
local iNubiaMercenariesDuration = 6;
local iMontuhotepArrivalTurn = 4;
local iPiyeArrivalTurn = 65;
local iAmaniranesArrivalTurn = 100;
local iGreatGeneralDuration = 15;

-- Event global data
local g_eInvasionReadyForPlayerID = -1;
local g_iInvaderX = -1;
local g_iInvaderY = -1;

-- SCORING SCENARIO FIELD USAGE
--
-- ScoringScenario1 is used to track if a player has received a high priestess
-- ScoringScenerio2 is used to track if a player has founded a religion

-- ===========================================================================
-- Utitity function: find closest city NOT of this player
-- ===========================================================================
function FindClosestTargetCity(iAttackingPlayer, iStartX, iStartY)

	local pCity = nullptr;
	local iShortestDistance = 10000;

	local aPlayers = PlayerManager.GetAlive();
	for loop, pPlayer in ipairs(aPlayers) do
		local iPlayer = pPlayer:GetID();
		if (iPlayer ~= iAttackingPlayer and pPlayer:GetDiplomacy():IsAtWarWith(iAttackingPlayer)) then
			local pPlayerCities:table = pPlayer:GetCities();
			for i, pLoopCity in pPlayerCities:Members() do
				local iDistance = Map.GetPlotDistance(iStartX, iStartY, pLoopCity:GetX(), pLoopCity:GetY());
				if (iDistance < iShortestDistance) then
					pCity = pLoopCity;
					iShortestDistance = iDistance;
				end
			end
		end
	end
	if (pCity == nullptr) then
		print ("No target city found for player " .. tostring(iAttackingPlayer) .. " from " .. tostring(iStartX) .. ", " .. tostring(iStartY));
	end

    return pCity;
end
-- ===========================================================================
-- Event Handlers
-- ===========================================================================
local function AddGreatGeneral(iPlayer, szGeneralName)
	local individual = GameInfo.GreatPersonIndividuals[szGeneralName].Hash;
	local class = GameInfo.GreatPersonClasses["GREAT_PERSON_CLASS_GENERAL"].Hash;
	local era = GameInfo.Eras["ERA_ANCIENT"].Hash;
	local cost = 0;
	Game.GetGreatPeople():GrantPerson(individual, class, era, cost, iPlayer, false);
end
-- ===========================================================================
local function RemoveGreatGeneral(iPlayer)
	local pUnits :table = Players[iPlayer]:GetUnits(); 	
	for i, pUnit in pUnits:Members() do
		if (GameInfo.Units[pUnit:GetType()].UnitType == "UNIT_GREAT_GENERAL") then
			local individual:number = pUnit:GetGreatPerson():GetIndividual();
			if individual >= 0 then
				local individualType:string = GameInfo.GreatPersonIndividuals[individual].GreatPersonIndividualType;
				if (individualType == "GREAT_PERSON_INDIVIDUAL_MONTUHOTEP" or
				    individualType == "GREAT_PERSON_INDIVIDUAL_PIYE" or
					individualType == "GREAT_PERSON_INDIVIDUAL_AMANIRENAS") then
					UnitManager.Kill(pUnit);
				end
			end
		end
	end
end
-- ===========================================================================
local function ReturnNubianMercenaries()

	local aPlayers = PlayerManager.GetAliveMajors();
	for loop, pPlayer in ipairs(aPlayers) do
		local iPlayer = pPlayer:GetID();
		if (iPlayer == eEgyptPlayer) then
			local pUnits :table = pPlayer:GetUnits(); 	
			for i, pUnit in pUnits:Members() do
				if (GameInfo.Units[pUnit:GetType()].UnitType == "UNIT_NUBIAN_PITATI") then
					UnitManager.Kill(pUnit);
				end
			end
		elseif (iPlayer == eNubiaPlayer) then
			UnitManager.InitUnitValidAdjacentHex(eNubiaPlayer, "UNIT_NUBIAN_PITATI", iNubiaCapitalX, iNubiaCapitalY, 1);
			UnitManager.InitUnitValidAdjacentHex(eNubiaPlayer, "UNIT_NUBIAN_PITATI", iNubiaCapitalX, iNubiaCapitalY, 1);
			pPlayer:GetTreasury():ChangeGoldBalance(100);
			local pUnits :table = pPlayer:GetUnits(); 	
			for i, pUnit in pUnits:Members() do
				if (GameInfo.Units[pUnit:GetType()].UnitType == "UNIT_NUBIAN_PITATI") then
					local iXP = pUnit:GetExperience():GetExperienceForNextLevel() - pUnit:GetExperience():GetExperiencePoints();
					pUnit:GetExperience():ChangeExperience(iXP);
				end
			end
		end
	end
end
-- ===========================================================================
local function MeetAndDOWNileNeighbors(player)

	-- Meet players in Nile Valley
	local pAttacker = Players[player];
	pAttacker:GetDiplomacy():SetHasMet(eEgyptPlayer);
	pAttacker:GetDiplomacy():SetHasMet(eNubiaPlayer);
	pAttacker:GetDiplomacy():SetHasMet(eAshumeinPlayer);
	pAttacker:GetDiplomacy():SetHasMet(eIhnasiyaPlayer);
	pAttacker:GetDiplomacy():SetHasMet(eNekhenPlayer);
	pAttacker:GetDiplomacy():SetHasMet(eOnPlayer);
	pAttacker:GetDiplomacy():SetHasMet(eSharuhenPlayer);
	pAttacker:GetDiplomacy():SetHasMet(eThinisPlayer);

	-- DOW players in Nile Valley
	pAttacker:GetDiplomacy():DeclareWarOn(eEgyptPlayer);
	pAttacker:GetDiplomacy():DeclareWarOn(eNubiaPlayer);
	pAttacker:GetDiplomacy():DeclareWarOn(eAshumeinPlayer);
	pAttacker:GetDiplomacy():DeclareWarOn(eIhnasiyaPlayer);
	pAttacker:GetDiplomacy():DeclareWarOn(eNekhenPlayer);
	pAttacker:GetDiplomacy():DeclareWarOn(eOnPlayer);
	pAttacker:GetDiplomacy():DeclareWarOn(eSharuhenPlayer);
	pAttacker:GetDiplomacy():DeclareWarOn(eThinisPlayer);

	-- Reveal oceans and coasts
	local pCurPlayerVisibility = PlayersVisibility[pAttacker:GetID()];
	if(pCurPlayerVisibility ~= nil) then
		for iX = 0, g_iW - 1 do
			for iY = 0, g_iH - 1 do
				local iPlotIndex = iY * g_iW + iX;
				local pPlot = Map.GetPlotByIndex(iPlotIndex);
				if (pPlot:IsWater() and not pPlot:IsLake()) then
					pCurPlayerVisibility:ChangeVisibilityCount(iPlotIndex, 1);
				end
			end
		end
	end
	-- Reveal Palestine
	for iX = 14, 34 do
		for iY = 65, 85 do
			local iPlotIndex = iY * g_iW + iX;
			local pPlot = Map.GetPlotByIndex(iPlotIndex);
			pCurPlayerVisibility:ChangeVisibilityCount(iPlotIndex, 1);
		end
	end
	-- Reveal Libya
	for iX = 0, 14 do
		for iY = 65, 73 do
			local iPlotIndex = iY * g_iW + iX;
			local pPlot = Map.GetPlotByIndex(iPlotIndex);
			pCurPlayerVisibility:ChangeVisibilityCount(iPlotIndex, 1);
		end
	end
	-- Reveal top of Nile
	for iX = 10, 14 do
		for iY = 49, 65 do
			local iPlotIndex = iY * g_iW + iX;
			local pPlot = Map.GetPlotByIndex(iPlotIndex);
			pCurPlayerVisibility:ChangeVisibilityCount(iPlotIndex, 1);
		end
	end
end
-- ===========================================================================
local function PrepareInvasion(invasionName)

	if (invasionName == "HYKSOS") then

		local iAttackerID = eHyksosPlayer;
		local pAttacker = Players[iAttackerID];

		-- Give out techs (but not Civics!)
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_FISHING"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_MINING"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_ANIMAL_HUSBANDRY"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_SAILING"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_ARCHERY"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_THE_WHEEL"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_HORSEBACK_RIDING"].Index, true);

		-- Set political situation
		MeetAndDOWNileNeighbors(iAttackerID);

		-- Set variables for operation initiation routine
		g_eInvasionReadyForPlayerID = iAttackerID;
		g_iInvaderX = iHyksosStartX;
		g_iInvaderY = iHyksosStartY;

		-- Build forces
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HEAVY_CHARIOT", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HEAVY_CHARIOT", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HEAVY_CHARIOT", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HEAVY_CHARIOT", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HEAVY_CHARIOT", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SPEARMAN", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SPEARMAN", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_BATTERING_RAM", g_iInvaderX, g_iInvaderY, 3);

	elseif (invasionName == "LIBYA") then

		local iAttackerID = eLibyaPlayer;
		local pAttacker = Players[iAttackerID];

		-- Give out techs (but not Civics!)
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_FISHING"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_MINING"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_ANIMAL_HUSBANDRY"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_SAILING"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_ARCHERY"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_THE_WHEEL"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_HORSEBACK_RIDING"].Index, true);

		-- Set political situation
		MeetAndDOWNileNeighbors(iAttackerID);
		pAttacker:GetDiplomacy():SetHasMet(eHyksosPlayer);
		pAttacker:GetDiplomacy():DeclareWarOn(eHyksosPlayer);

		-- Set variables for operation initiation routine
		g_eInvasionReadyForPlayerID = iAttackerID;
		g_iInvaderX = iLibyaStartX;
		g_iInvaderY = iLibyaStartY;

		-- Build forces
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SCOUT", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HEAVY_CHARIOT", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HEAVY_CHARIOT", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SPEARMAN", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SPEARMAN", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SPEARMAN", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SPEARMAN", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_BATTERING_RAM", g_iInvaderX, g_iInvaderY, 3);

		if (GameInfo.Difficulties[PlayerConfigurations[0]:GetHandicapTypeID()].Index > 5) then
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HEAVY_CHARIOT", g_iInvaderX, g_iInvaderY, 3);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SPEARMAN", g_iInvaderX, g_iInvaderY, 3);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SPEARMAN", g_iInvaderX, g_iInvaderY, 3);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 3);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 3);
		end

	elseif (invasionName == "SEAPEOPLE") then

		local iAttackerID = eLibyaPlayer;
		local pAttacker = Players[iAttackerID];

		-- Set variables for operation initiation routine
		g_eInvasionReadyForPlayerID = iAttackerID;
		g_iInvaderX = iSeaPeopleStartX;
		g_iInvaderY = iSeaPeopleStartY;

		-- Build forces
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SCOUT", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HEAVY_CHARIOT", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HEAVY_CHARIOT", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SPEARMAN", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SPEARMAN", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SPEARMAN", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SPEARMAN", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 3);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_BATTERING_RAM", g_iInvaderX, g_iInvaderY, 3);

		if (GameInfo.Difficulties[PlayerConfigurations[0]:GetHandicapTypeID()].Index > 5) then
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HEAVY_CHARIOT", g_iInvaderX, g_iInvaderY, 3);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SPEARMAN", g_iInvaderX, g_iInvaderY, 3);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SPEARMAN", g_iInvaderX, g_iInvaderY, 3);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 3);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 3);
		end

	elseif (invasionName == "ASSYRIA") then

		local iAttackerID = eAssyriaPlayer;
		local pAttacker = Players[iAttackerID];

		-- Give out techs (but not Civics!)
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_FISHING"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_MINING"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_ANIMAL_HUSBANDRY"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_SAILING"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_MASONRY"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_ARCHERY"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_THE_WHEEL"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_HORSEBACK_RIDING"].Index, true);

		-- Set political situation
		MeetAndDOWNileNeighbors(iAttackerID);
		pAttacker:GetDiplomacy():SetHasMet(eHyksosPlayer);
		pAttacker:GetDiplomacy():DeclareWarOn(eHyksosPlayer);
		pAttacker:GetDiplomacy():SetHasMet(eLibyaPlayer);
		pAttacker:GetDiplomacy():DeclareWarOn(eLibyaPlayer);

		-- Set variables for operation initiation routine
		g_eInvasionReadyForPlayerID = iAttackerID;
		g_iInvaderX = iAssyriaStartX;
		g_iInvaderY = iAssyriaStartY;

		-- Build forces
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HEAVY_CHARIOT", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HEAVY_CHARIOT", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HEAVY_CHARIOT", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SPEARMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SPEARMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SPEARMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_BATTERING_RAM", g_iInvaderX, g_iInvaderY, 4);
		AddGreatGeneral(iAttackerID, "GREAT_PERSON_INDIVIDUAL_SHA_NABU_SHU");

		if (GameInfo.Difficulties[PlayerConfigurations[0]:GetHandicapTypeID()].Index > 5) then
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HEAVY_CHARIOT", g_iInvaderX, g_iInvaderY, 4);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HEAVY_CHARIOT", g_iInvaderX, g_iInvaderY, 4);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 4);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 4);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 4);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SPEARMAN", g_iInvaderX, g_iInvaderY, 4);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SPEARMAN", g_iInvaderX, g_iInvaderY, 4);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 4);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 4);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 4);
		end

	elseif (invasionName == "PERSIA") then

		local iAttackerID = ePersiaPlayer;
		local pAttacker = Players[iAttackerID];

		-- Give out techs (but not Civics!)
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_FISHING"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_MINING"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_ANIMAL_HUSBANDRY"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_SAILING"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_MASONRY"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_ARCHERY"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_THE_WHEEL"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_HORSEBACK_RIDING"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_FORTIFICATION"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_IRON_WORKING"].Index, true);

		-- Set political situation
		MeetAndDOWNileNeighbors(iAttackerID);
		pAttacker:GetDiplomacy():SetHasMet(eHyksosPlayer);
		pAttacker:GetDiplomacy():DeclareWarOn(eHyksosPlayer);
		pAttacker:GetDiplomacy():SetHasMet(eLibyaPlayer);
		pAttacker:GetDiplomacy():DeclareWarOn(eLibyaPlayer);
		pAttacker:GetDiplomacy():SetHasMet(eAssyriaPlayer);
		pAttacker:GetDiplomacy():DeclareWarOn(eAssyriaPlayer);
		pAttacker:GetDiplomacy():SetHasMet(eByblosPlayer);
		pAttacker:GetDiplomacy():DeclareWarOn(eByblosPlayer);

		-- Set variables for operation initiation routine
		g_eInvasionReadyForPlayerID = iAttackerID;
		g_iInvaderX = iPersiaStartX;
		g_iInvaderY = iPersiaStartY;

		-- Build forces
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SWORDSMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SWORDSMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SWORDSMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SWORDSMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HEAVY_CHARIOT", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HEAVY_CHARIOT", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HEAVY_CHARIOT", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HEAVY_CHARIOT", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_BATTERING_RAM", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_BATTERING_RAM", g_iInvaderX, g_iInvaderY, 4);
		AddGreatGeneral(iAttackerID, "GREAT_PERSON_INDIVIDUAL_CAMBYSES");

		if (GameInfo.Difficulties[PlayerConfigurations[0]:GetHandicapTypeID()].Index > 5) then
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SWORDSMAN", g_iInvaderX, g_iInvaderY, 4);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SWORDSMAN", g_iInvaderX, g_iInvaderY, 4);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SWORDSMAN", g_iInvaderX, g_iInvaderY, 4);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HEAVY_CHARIOT", g_iInvaderX, g_iInvaderY, 4);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HEAVY_CHARIOT", g_iInvaderX, g_iInvaderY, 4);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HEAVY_CHARIOT", g_iInvaderX, g_iInvaderY, 4);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 4);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 4);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 4);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 4);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 4);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 4);
		end
	elseif (invasionName == "MACEDON") then

		local iAttackerID = eMacedonPlayer;
		local pAttacker = Players[iAttackerID];

		-- Give out techs (but not Civics!)
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_FISHING"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_MINING"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_ANIMAL_HUSBANDRY"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_SAILING"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_MASONRY"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_ARCHERY"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_THE_WHEEL"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_HORSEBACK_RIDING"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_FORTIFICATION"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_IRON_WORKING"].Index, true);

		-- Set political situation
		MeetAndDOWNileNeighbors(iAttackerID);
		pAttacker:GetDiplomacy():SetHasMet(eHyksosPlayer);
		pAttacker:GetDiplomacy():DeclareWarOn(eHyksosPlayer);
		pAttacker:GetDiplomacy():SetHasMet(eLibyaPlayer);
		pAttacker:GetDiplomacy():DeclareWarOn(eLibyaPlayer);
		pAttacker:GetDiplomacy():SetHasMet(eAssyriaPlayer);
		pAttacker:GetDiplomacy():DeclareWarOn(eAssyriaPlayer);
		pAttacker:GetDiplomacy():SetHasMet(eByblosPlayer);
		pAttacker:GetDiplomacy():DeclareWarOn(eByblosPlayer);
		pAttacker:GetDiplomacy():SetHasMet(ePersiaPlayer);
		pAttacker:GetDiplomacy():DeclareWarOn(ePersiaPlayer);

		-- Set variables for operation initiation routine
		g_eInvasionReadyForPlayerID = iAttackerID;
		g_iInvaderX = iMacedonStartX;
		g_iInvaderY = iMacedonStartY;

		-- Build forces
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SWORDSMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SWORDSMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SWORDSMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SWORDSMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SWORDSMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SWORDSMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SWORDSMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SWORDSMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SIEGE_TOWER", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SIEGE_TOWER", g_iInvaderX, g_iInvaderY, 4);
		AddGreatGeneral(iAttackerID, "GREAT_PERSON_INDIVIDUAL_ALEXANDER");

		if (GameInfo.Difficulties[PlayerConfigurations[0]:GetHandicapTypeID()].Index > 5) then
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SWORDSMAN", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SWORDSMAN", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SWORDSMAN", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SWORDSMAN", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SWORDSMAN", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SWORDSMAN", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SWORDSMAN", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SWORDSMAN", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 5);
		end

	elseif (invasionName == "ROME") then

		local iAttackerID = eRomePlayer;
		local pAttacker = Players[iAttackerID];

		-- Give out techs (but not Civics!)
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_FISHING"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_MINING"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_ANIMAL_HUSBANDRY"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_SAILING"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_MASONRY"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_ARCHERY"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_THE_WHEEL"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_HORSEBACK_RIDING"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_FORTIFICATION"].Index, true);
		pAttacker:GetTechs():SetTech(GameInfo.Technologies["TECH_IRON_WORKING"].Index, true);

		-- Set political situation
		MeetAndDOWNileNeighbors(iAttackerID);
		pAttacker:GetDiplomacy():SetHasMet(eHyksosPlayer);
		pAttacker:GetDiplomacy():DeclareWarOn(eHyksosPlayer);
		pAttacker:GetDiplomacy():SetHasMet(eLibyaPlayer);
		pAttacker:GetDiplomacy():DeclareWarOn(eLibyaPlayer);
		pAttacker:GetDiplomacy():SetHasMet(eAssyriaPlayer);
		pAttacker:GetDiplomacy():DeclareWarOn(eAssyriaPlayer);
		pAttacker:GetDiplomacy():SetHasMet(eByblosPlayer);
		pAttacker:GetDiplomacy():DeclareWarOn(eByblosPlayer);
		pAttacker:GetDiplomacy():SetHasMet(ePersiaPlayer);
		pAttacker:GetDiplomacy():DeclareWarOn(ePersiaPlayer);
		pAttacker:GetDiplomacy():SetHasMet(eMacedonPlayer);
		pAttacker:GetDiplomacy():DeclareWarOn(eMacedonPlayer);

		-- Set variables for operation initiation routine
		g_eInvasionReadyForPlayerID = iAttackerID;
		g_iInvaderX = iRomeStartX;
		g_iInvaderY = iRomeStartY;

		-- Build forces
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ROMAN_LEGION", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ROMAN_LEGION", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ROMAN_LEGION", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ROMAN_LEGION", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ROMAN_LEGION", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ROMAN_LEGION", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ROMAN_LEGION", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ROMAN_LEGION", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ROMAN_LEGION", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ROMAN_LEGION", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SIEGE_TOWER", g_iInvaderX, g_iInvaderY, 4);
		UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_SIEGE_TOWER", g_iInvaderX, g_iInvaderY, 4);

		if (GameInfo.Difficulties[PlayerConfigurations[0]:GetHandicapTypeID()].Index > 5) then
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ROMAN_LEGION", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ROMAN_LEGION", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ROMAN_LEGION", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ROMAN_LEGION", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ROMAN_LEGION", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ROMAN_LEGION", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ROMAN_LEGION", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ROMAN_LEGION", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ROMAN_LEGION", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ROMAN_LEGION", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_HORSEMAN", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 5);
			UnitManager.InitUnitValidAdjacentHex(iAttackerID, "UNIT_ARCHER", g_iInvaderX, g_iInvaderY, 5);
		end
	end
end
-- ===========================================================================
local function InitiateInvasions( player, currentTurn )

	if (g_eInvasionReadyForPlayerID == player) then

		-- Script them to attack nearest city
		local pAttacker = Players[player];
		local pNearestCity = FindClosestTargetCity(player, g_iInvaderX, g_iInvaderY);
		if (pNearestCity ~= nullptr) then
			local pMilitaryAI = pAttacker:GetAi_Military();
			if (pMilitaryAI ~= nullptr) then
				local iOperationID = pMilitaryAI:StartScriptedOperationWithTargetAndRally("Attack Enemy City", pNearestCity:GetOwner(), Map.GetPlot(pNearestCity:GetX(), pNearestCity:GetY()):GetIndex(), Map.GetPlot(g_iInvaderX, g_iInvaderY):GetIndex());
				local pUnits :table = pAttacker:GetUnits(); 	
				for i, pUnit in pUnits:Members() do

					-- Special check for Sea Peoples so we ignore the Libyans coming from the west
					local bSkip = false;
					if (currentTurn == iLibyaInvasionTurn + 1 and player == eLibyaPlayer) then
						if (pUnit:GetX() < 14) then
							bSkip = true;
						end
					end
					if (not bSkip) then
						pMilitaryAI:AddUnitToScriptedOperation(iOperationID, pUnit:GetID());
					end
				end
			end
		end

		g_eInvasionReadyForPlayerID = -1;
	end
end
-- ===========================================================================
local function CheckForScenarioEvent( player, currentTurn )

	local eventKey;
	local eventEffectString = "";
	local bShowEvent = false;

	print (currentTurn);

	if (currentTurn == 1) then
	    if (player == eEgyptPlayer) then
			local pEgyptPlayer = Players[player];
			local pMilitaryAI = pEgyptPlayer:GetAi_Military();
			if (pMilitaryAI ~= nullptr) then
				pMilitaryAI:SetRival(eNubiaPlayer);
				pMilitaryAI:SetRival(eThinisPlayer);
				pMilitaryAI:SetRival(eNekhenPlayer);
				pMilitaryAI:ScriptForceUpdateTargets();
			end
			eventKey = GameInfo.EventPopupData["NUBIA_SCENARIO_START_EGYPT"].Type;
		elseif (player == eNubiaPlayer) then
			local pNubiaPlayer = Players[player];
			local pMilitaryAI = pNubiaPlayer:GetAi_Military();
			if (pMilitaryAI ~= nullptr) then
				pMilitaryAI:SetRival(eEgyptPlayer);
			end
			eventKey = GameInfo.EventPopupData["NUBIA_SCENARIO_START_NUBIA"].Type;
		end
		bShowEvent = true;
	elseif (currentTurn == 1 + iNubiaMercenariesDuration) then
	    if (player == eEgyptPlayer) then
			eventKey = GameInfo.EventPopupData["NUBIA_SCENARIO_MERCENARIES_LOST"].Type;
		else
			eventKey = GameInfo.EventPopupData["NUBIA_SCENARIO_MERCENARIES_GAINED"].Type;
		end
	    if (player == 0) then
			ReturnNubianMercenaries();
		end
		bShowEvent = true;	
	elseif (currentTurn == iHyksosInvasionTurn) then
		eventKey = GameInfo.EventPopupData["NUBIA_SCENARIO_HYKSOS_INVASION"].Type;	
		bShowEvent = true;	
	    if (player == 0) then
			PrepareInvasion("HYKSOS");
		end
	elseif (currentTurn == iLibyaInvasionTurn) then
		eventKey = GameInfo.EventPopupData["NUBIA_SCENARIO_LIBYA_INVASION"].Type;	
		bShowEvent = true;	
	    if (player == 0) then
			PrepareInvasion("LIBYA");
		end
	elseif (currentTurn == iLibyaInvasionTurn + 1) then
	    if (player == 0) then
			PrepareInvasion("SEAPEOPLE");
		end
	elseif (currentTurn == iAssyriaInvasionTurn) then
		eventKey = GameInfo.EventPopupData["NUBIA_SCENARIO_ASSYRIA_INVASION"].Type;	
		bShowEvent = true;	
	    if (player == 0) then
			PrepareInvasion("ASSYRIA");
		end
	elseif (currentTurn == iPersiaInvasionTurn) then
		eventKey = GameInfo.EventPopupData["NUBIA_SCENARIO_PERSIA_INVASION"].Type;	
		bShowEvent = true;	
	    if (player == 0) then
			PrepareInvasion("PERSIA");
		end
	elseif (currentTurn == iMacedonInvasionTurn) then
		eventKey = GameInfo.EventPopupData["NUBIA_SCENARIO_MACEDON_INVASION"].Type;	
		bShowEvent = true;	
	    if (player == 0) then
			PrepareInvasion("MACEDON");
		end
	elseif (currentTurn == iRomeInvasionTurn) then
		eventKey = GameInfo.EventPopupData["NUBIA_SCENARIO_ROME_INVASION"].Type;	
		bShowEvent = true;	
	    if (player == 0) then
			PrepareInvasion("ROME");
		end
	elseif (currentTurn == iMontuhotepArrivalTurn) then
	    if (player == eEgyptPlayer) then
			eventKey = GameInfo.EventPopupData["NUBIA_SCENARIO_MONTUHOTEP_ARRIVES"].Type;
			AddGreatGeneral(eEgyptPlayer, "GREAT_PERSON_INDIVIDUAL_MONTUHOTEP");
			bShowEvent = true;	
		end
	elseif (currentTurn == iMontuhotepArrivalTurn + iGreatGeneralDuration) then
	    if (player == eEgyptPlayer) then
			eventKey = GameInfo.EventPopupData["NUBIA_SCENARIO_MONTUHOTEP_DIES"].Type;
			RemoveGreatGeneral(eEgyptPlayer);
			bShowEvent = true;	
		end
	elseif (currentTurn == iPiyeArrivalTurn) then
	    if (player == eNubiaPlayer) then
			eventKey = GameInfo.EventPopupData["NUBIA_SCENARIO_PIYE_ARRIVES"].Type;
			AddGreatGeneral(eNubiaPlayer, "GREAT_PERSON_INDIVIDUAL_PIYE");
			bShowEvent = true;	
		end
	elseif (currentTurn == iPiyeArrivalTurn + iGreatGeneralDuration) then
	    if (player == eNubiaPlayer) then
			eventKey = GameInfo.EventPopupData["NUBIA_SCENARIO_PIYE_DIES"].Type;
			RemoveGreatGeneral(eNubiaPlayer);
			bShowEvent = true;	
		end
	elseif (currentTurn == iAmaniranesArrivalTurn) then
	    if (player == eNubiaPlayer) then
			eventKey = GameInfo.EventPopupData["NUBIA_SCENARIO_AMANIRENAS_ARRIVES"].Type;
			AddGreatGeneral(eNubiaPlayer, "GREAT_PERSON_INDIVIDUAL_AMANIRENAS");
			bShowEvent = true;	
		end
	elseif (currentTurn == iAmaniranesArrivalTurn + iGreatGeneralDuration) then
	    if (player == eNubiaPlayer) then
			eventKey = GameInfo.EventPopupData["NUBIA_SCENARIO_AMANIRENAS_DIES"].Type;
			RemoveGreatGeneral(eNubiaPlayer);
			bShowEvent = true;	
		end
	end

	if (bShowEvent == true) then
		ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = player, EventKey = eventKey, EventEffect = eventEffectString });
	end
end

-- Initialization functions
-- ===========================================================================
function Initialize()
		
	local aPlayers = PlayerManager.GetAliveMajors();
	for loop, pPlayer in ipairs(aPlayers) do
		local iPlayer = pPlayer:GetID();
		local pkPlot = pPlayer:GetStartingPlot();
		if (pkPlot:GetX() == iEgyptCapitalX and pkPlot:GetY() == iEgyptCapitalY) then
			eEgyptPlayer = iPlayer;		
		elseif (pkPlot:GetX() == iNubiaCapitalX and pkPlot:GetY() == iNubiaCapitalY) then
			eNubiaPlayer = iPlayer;
		end
	end

	print ("Egypt: " .. tostring(eEgyptPlayer));
	print ("Nubia: " .. tostring(eNubiaPlayer));
end

-- ===========================================================================
function InitializeNewGame()

	print ("InitializeNewGame");

	-- Egypt has met the nearby minors
	Players[eEgyptPlayer]:GetDiplomacy():SetHasMet(eAshumeinPlayer);
	Players[eEgyptPlayer]:GetDiplomacy():SetHasMet(eIhnasiyaPlayer);
	Players[eEgyptPlayer]:GetDiplomacy():SetHasMet(eNekhenPlayer);
	Players[eEgyptPlayer]:GetDiplomacy():SetHasMet(eThinisPlayer);

	local pGameReligion:table = Game.GetReligion();
	local pPlayer = Players[eEgyptPlayer];
	local pPlayerCities:table = pPlayer:GetCities();
	local pReligion = pPlayer:GetReligion();

	pGameReligion:FoundPantheon(eEgyptPlayer,  GameInfo.Beliefs["BELIEF_NUN"].Index);
	local eReligion = GameInfo.Religions["RELIGION_CUSTOM_9"].Index;
	for i, pCity in pPlayerCities:Members() do
		if (pPlayer:GetScoringScenario2() == 0) then
			pReligion:SetHolyCity(pCity);
			pReligion:SetFaithBalance(0);
			pGameReligion:FoundReligion(eEgyptPlayer, eReligion);
			pPlayer:ChangeScoringScenario2(1);
		end
		local pCityReligion = pCity:GetReligion();
		if (pCityReligion:GetMajorityReligion() ~= eReligion) then
			pCityReligion:SetAllCityToReligion(eReligion);
		end
	end

	-- Add extra at-start forces on Immortal and Deity for primary opponent
	if (GameInfo.Difficulties[PlayerConfigurations[0]:GetHandicapTypeID()].Index > 5) then
		if (not pPlayer:IsHuman()) then
			UnitManager.InitUnitValidAdjacentHex(pPlayer:GetID(), "UNIT_SETTLER", iEgyptCapitalX, iEgyptCapitalY, 2);
			UnitManager.InitUnitValidAdjacentHex(pPlayer:GetID(), "UNIT_BUILDER", iEgyptCapitalX, iEgyptCapitalY, 2);
			UnitManager.InitUnitValidAdjacentHex(pPlayer:GetID(), "UNIT_SPEARMAN", iEgyptCapitalX, iEgyptCapitalY, 2);
			UnitManager.InitUnitValidAdjacentHex(pPlayer:GetID(), "UNIT_SLINGER", iEgyptCapitalX, iEgyptCapitalY, 2);

			if (GameInfo.Difficulties[PlayerConfigurations[0]:GetHandicapTypeID()].Index > 6) then
				UnitManager.InitUnitValidAdjacentHex(pPlayer:GetID(), "UNIT_SETTLER", iEgyptCapitalX, iEgyptCapitalY, 3);
				UnitManager.InitUnitValidAdjacentHex(pPlayer:GetID(), "UNIT_BUILDER", iEgyptCapitalX, iEgyptCapitalY, 3);
				UnitManager.InitUnitValidAdjacentHex(pPlayer:GetID(), "UNIT_SPEARMAN", iEgyptCapitalX, iEgyptCapitalY, 3);
				UnitManager.InitUnitValidAdjacentHex(pPlayer:GetID(), "UNIT_SLINGER", iEgyptCapitalX, iEgyptCapitalY, 3);
			end
		end

		local pPlayer = Players[eNubiaPlayer];
		if (not pPlayer:IsHuman()) then
			UnitManager.InitUnitValidAdjacentHex(pPlayer:GetID(), "UNIT_SETTLER", iNubiaCapitalX, iNubiaCapitalY, 2);
			UnitManager.InitUnitValidAdjacentHex(pPlayer:GetID(), "UNIT_BUILDER", iNubiaCapitalX, iNubiaCapitalY, 2);
			UnitManager.InitUnitValidAdjacentHex(pPlayer:GetID(), "UNIT_SPEARMAN", iNubiaCapitalX, iNubiaCapitalY, 2);
			UnitManager.InitUnitValidAdjacentHex(pPlayer:GetID(), "UNIT_SLINGER", iNubiaCapitalX, iNubiaCapitalY, 2);

			if (GameInfo.Difficulties[PlayerConfigurations[0]:GetHandicapTypeID()].Index > 6) then
				UnitManager.InitUnitValidAdjacentHex(pPlayer:GetID(), "UNIT_SETTLER", iNubiaCapitalX, iNubiaCapitalY, 3);
				UnitManager.InitUnitValidAdjacentHex(pPlayer:GetID(), "UNIT_BUILDER", iNubiaCapitalX, iNubiaCapitalY, 3);
				UnitManager.InitUnitValidAdjacentHex(pPlayer:GetID(), "UNIT_SPEARMAN", iNubiaCapitalX, iNubiaCapitalY, 3);
				UnitManager.InitUnitValidAdjacentHex(pPlayer:GetID(), "UNIT_SLINGER", iNubiaCapitalX, iNubiaCapitalY, 3);
			end
		end
	end
end
LuaEvents.NewGameInitialized.Add(InitializeNewGame);

-- ===========================================================================
-- Event Handlers
-- ===========================================================================
local function OnPlayerTurnActivated( player )
	local currentTurn = Game.GetCurrentGameTurn();
	
	print ("OnPlayerTurnActivated: Player " .. tostring(player) .. ", Turn " .. tostring(currentTurn));

	InitiateInvasions(player, currentTurn);
end
GameEvents.PlayerTurnStartComplete.Add(OnPlayerTurnActivated);

-- ===========================================================================
local function OnGameTurnStarted( player )
	local currentTurn = Game.GetCurrentGameTurn();
	
	print ("OnGameTurnStarted: Turn " .. tostring(currentTurn));
	local aPlayers = PlayerManager.GetAliveMajors();
	for loop, pPlayer in ipairs(aPlayers) do
		local iPlayer = pPlayer:GetID();
		-- Removing this check at least temporarily so I can do some AI autoplays with it
		--if (pPlayer:IsHuman() == true) then 
		    CheckForScenarioEvent(iPlayer, currentTurn);
		--end
	end
end
GameEvents.OnGameTurnStarted.Add(OnGameTurnStarted);

-- ===========================================================================
local function OnCityAddedToMap( playerID: number, cityID : number, cityX : number, cityY : number )
	
	print("CityAddedToMap - " .. tostring(playerID) .. ":" .. tostring(cityID) .. " " .. tostring(cityX) .. "x" .. tostring(cityY));

	local pGameReligion:table = Game.GetReligion();
	local pPlayer = Players[playerID];
	local pPlayerCities:table = pPlayer:GetCities();
	local pReligion = pPlayer:GetReligion();

	if (playerID == eNubiaPlayer and pPlayer:GetScoringScenario2() == 0) then
		pGameReligion:FoundPantheon(playerID,  GameInfo.Beliefs["BELIEF_ATUM"].Index);
		local eReligion = GameInfo.Religions["RELIGION_CUSTOM_7"].Index;
		pReligion:SetHolyCity(pPlayerCities:FindID(cityID));
		pReligion:SetFaithBalance(0);
		pGameReligion:FoundReligion(playerID, eReligion);
		for i, pCity in pPlayerCities:Members() do
			local pCityReligion = pCity:GetReligion();
			if (pCityReligion:GetMajorityReligion() ~= eReligion) then
				pCityReligion:SetAllCityToReligion(eReligion);
			end
		end
		pPlayer:ChangeScoringScenario2(1);
	
	else
		local pThisCity = pPlayerCities:FindID(cityID);
		if (pThisCity ~= nullptr) then
	
			-- Set this city to an existing player religion
			if (playerID < 2) then	
				local eReligion = pReligion:GetReligionTypeCreated();
				local pCityReligion = pThisCity:GetReligion();
				pCityReligion:SetAllCityToReligion(eReligion);

			-- Set this city to no religion
			else
				local pCityReligion = pThisCity:GetReligion();
				pCityReligion:SetAllCityToReligion(-1);
			end
		end
	end
end
GameEvents.CityBuilt.Add(OnCityAddedToMap );
-- ===========================================================================
function OnCityConquered(capturerID,  ownerID, cityID , cityX, cityY)

	local pPlayer = Players[capturerID];
	local pPlayerCities:table = pPlayer:GetCities();
	local pReligion = pPlayer:GetReligion();

	local pThisCity = pPlayerCities:FindID(cityID);
	if (pThisCity ~= nullptr) then
	
		-- Set this city to an existing player religion
		if (capturerID < 2) then	
			local eReligion = pReligion:GetReligionTypeCreated();
			local pCityReligion = pThisCity:GetReligion();
			pCityReligion:SetAllCityToReligion(eReligion);

		-- Captured by someone other than Egypt or Nubia; set this city to no religion
		else
			local pCityReligion = pThisCity:GetReligion();
			pCityReligion:SetAllCityToReligion(-1);

			-- Perhaps give them extra units to keep their attack going
			if (capturerID >= eHyksosPlayer and capturerID <= eRomePlayer) then

				UnitManager.InitUnitValidAdjacentHex(capturerID, "UNIT_HEAVY_CHARIOT", pThisCity:GetX(), pThisCity:GetY(), 3);
				UnitManager.InitUnitValidAdjacentHex(capturerID, "UNIT_HORSEMAN", pThisCity:GetX(), pThisCity:GetY(), 3);
				UnitManager.InitUnitValidAdjacentHex(capturerID, "UNIT_SPEARMAN", pThisCity:GetX(), pThisCity:GetY(), 3);
				UnitManager.InitUnitValidAdjacentHex(capturerID, "UNIT_ARCHER", pThisCity:GetX(), pThisCity:GetY(), 3);

			end
		end
	end
end
GameEvents.CityConquered.Add(OnCityConquered);
-- ===========================================================================
function OnDistrictConstructed(iPlayer, eDistrictType, iX, iY)

	if (eDistrictType == GameInfo.Districts["DISTRICT_HOLY_SITE"].Index and iPlayer < 2) then
		local pPlayer = Players[iPlayer];
		if (pPlayer:GetScoringScenario1() == 0) then
			UnitManager.InitUnitValidAdjacentHex(iPlayer, "UNIT_HIGH_PRIESTESS", iX, iY, 0);
			pPlayer:ChangeScoringScenario1(1);

			if (pPlayer:IsHuman()) then
				local eventKey = GameInfo.EventPopupData["NUBIA_SCENARIO_HIGH_PRIESTESS_ARISES"].Type;
				ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = iPlayer, EventKey = eventKey });
			end
		end
	end
end
GameEvents.OnDistrictConstructed.Add(OnDistrictConstructed);
-- ===========================================================================
function OnCivicCulturevated(iPlayer, eCivic)

	if (eCivic == GameInfo.Civics["CIVIC_BOARDGAMES_ATHLETICS"].Index and iPlayer < 2) then
		if (iPlayer == eEgyptPlayer) then
				AddGreatGeneral(iPlayer, "GREAT_PERSON_INDIVIDUAL_HOREMHEB");
		elseif (iPlayer == eNubiaPlayer) then
				AddGreatGeneral(iPlayer, "GREAT_PERSON_INDIVIDUAL_AMANISHAKETO");
		end
	end
end
GameEvents.OnCivicCulturevated.Add(OnCivicCulturevated);
-- ===========================================================================

Initialize();