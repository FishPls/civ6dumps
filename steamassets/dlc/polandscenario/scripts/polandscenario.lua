-- ===========================================================================
--	Poland Scenario
-- ===========================================================================
local lastLocalTurnNumber = 0;

-- Serialize these after creation in the future when we have scenario-specific DBs!
local iMarienburgTribeNumber = 0;
local iRigaTribeNumber = 1;
local iNovgorodTribeNumber = 2;
local iMoscowTribeNumber = 3;
local iHordeTribeNumber = 4;
local iMoldovaTribeNumber = 5;
local iOttomanTribeNumber = 6;
local iSwedishLivoniaTribeNumber = 7;
local iSwedishPrussiaTribeNumber = 8;
local iSwedishPomeraniaTribeNumber = 9;

local eOstrogskiPlayer = -1;
local ePotockiPlayer = -1;
local eRadziwillPlayer = -1;
local eGdansk = 3;
local eVienna = 4;
local ePrague = 5;

local bCatholicismFounded = false;
local bOrthodoxyFounded = false;

local iDifficulty = 3;

-- ===========================================================================
function Initialize()
		
	-- Compute average difficulty of the three player civs
	iDifficulty = GameInfo.Difficulties[PlayerConfigurations[0]:GetHandicapTypeID()].Index;
	iDifficulty = iDifficulty + GameInfo.Difficulties[PlayerConfigurations[1]:GetHandicapTypeID()].Index;
	iDifficulty = iDifficulty + GameInfo.Difficulties[PlayerConfigurations[2]:GetHandicapTypeID()].Index;
	iDifficulty = iDifficulty / 3;
	print ("iDifficulty: ", tostring(iDifficulty));

	local aPlayers = PlayerManager.GetAliveMajors();
	for loop, pPlayer in ipairs(aPlayers) do
		local iPlayer = pPlayer:GetID();
		local pkPlot = pPlayer:GetStartingPlot();
		if (pkPlot:GetX() == 8 and pkPlot:GetY() == 7) then
			ePotockiPlayer = iPlayer;		
		elseif (pkPlot:GetX() == 18 and pkPlot:GetY() == 19) then
			eRadziwillPlayer = iPlayer;
		elseif (pkPlot:GetX() == 20 and pkPlot:GetY() == 7) then
			eOstrogskiPlayer = iPlayer;
		end
	end

	print ("Potocki: " .. tostring(ePotockiPlayer));
	print ("Radziwill: " .. tostring(eRadziwillPlayer));
	print ("Ostrogski: " .. tostring(eOstrogskiPlayer));
end

-- ===========================================================================
function InitializeNewGame()

	-- NEED A FINAL APPROACH TO TURN AND YEAR
	--    *** ALSO FIX TURN BEFORE CALL TO SetInitialVisibility AT BOTTOM OF FILE
	-- Game.SetCurrentGameTurn(1);

	print ("InitializeNewGame");

	-- Majors have met
	Players[0]:GetDiplomacy():SetHasMet(1);
	Players[0]:GetDiplomacy():SetHasMet(2);
	Players[1]:GetDiplomacy():SetHasMet(2);
	
	-- Majors are allied
	Players[0]:GetDiplomacy():SetPermanentAlliance(1);
	Players[0]:GetDiplomacy():SetPermanentAlliance(2);
	Players[1]:GetDiplomacy():SetPermanentAlliance(2);

	-- Majors also have delegations
	Players[0]:GetDiplomacy():SetHasDelegationAt(1, true);
	Players[0]:GetDiplomacy():SetHasDelegationAt(2, true);
	Players[1]:GetDiplomacy():SetHasDelegationAt(0, true);
	Players[1]:GetDiplomacy():SetHasDelegationAt(2, true);
	Players[2]:GetDiplomacy():SetHasDelegationAt(0, true);
	Players[2]:GetDiplomacy():SetHasDelegationAt(1, true);
	Players[0]:GetDiplomacy():RecheckVisibilityOnAll();
	Players[1]:GetDiplomacy():RecheckVisibilityOnAll();
	Players[2]:GetDiplomacy():RecheckVisibilityOnAll();

	-- Apply updates to all majors
	local aPlayers = PlayerManager.GetAliveMajors();
	for loop, pPlayer in ipairs(aPlayers) do
		local iPlayer = pPlayer:GetID();
	
		-- Almost finished with Medieval era
		local pCulture:table = pPlayer:GetCulture();
		local pScience:table = pPlayer:GetTechs();
		pCulture:SetCivic(GameInfo.Civics["CIVIC_MERCENARIES"].Index, false);
		pScience:SetTech(GameInfo.Technologies["TECH_MILITARY_ENGINEERING"].Index, false);
	end
end
LuaEvents.NewGameInitialized.Add(InitializeNewGame);

-- ===========================================================================
local function SetInitialVisibility()

	print ("SetInitialVisibility");

	-- Apply updates to all majors
	local aPlayers = PlayerManager.GetAliveMajors();
	for loop, pPlayer in ipairs(aPlayers) do
		local iPlayer = pPlayer:GetID();
	
		local pCurPlayerVisibility = PlayersVisibility[pPlayer:GetID()];
		if(pCurPlayerVisibility ~= nil) then
		
			-- Reveal Vienna
			for iX = 0, 7, 1 do
				for iY = 0, 3, 1 do
					local iPlotIndex = Map.GetPlot(iX, iY):GetIndex();
					pCurPlayerVisibility:ChangeVisibilityCount(iPlotIndex, 1);
				end
			end

			-- Reveal Poland
			for iX = 0, 30, 1 do
				for iY = 4, 20, 1 do

					-- Except hole for Prussia
					if (iX < 10 or iX > 14 or iY < 15) then 
						local iPlotIndex = Map.GetPlot(iX, iY):GetIndex();
						pCurPlayerVisibility:ChangeVisibilityCount(iPlotIndex, 1);
					end
				end
			end

			-- Reveal Lithuania
			for iX = 16, 30, 1 do
				for iY = 21, 25, 1 do
					local iPlotIndex = Map.GetPlot(iX, iY):GetIndex();
					pCurPlayerVisibility:ChangeVisibilityCount(iPlotIndex, 1);
				end
			end
		end
	end
end

-- ===========================================================================
local function CreateTribeAt( eType, iPlotIndex )

	local pBarbManager = Game.GetBarbarianManager();

   -- Clear improvement in case one already here
   local pPlot = Map.GetPlotByIndex(iPlotIndex);
   ImprovementBuilder.SetImprovementType(pPlot, -1, NO_PLAYER);   

   local iTribeNumber = pBarbManager:CreateTribeOfType(eType, iPlotIndex);
   return iTribeNumber;
end

-- ===========================================================================
local function FindNearestTargetCity( eTargetPlayer, iX, iY )

    local pCity = nullptr;
    local iShortestDistance = 10000;
	local pPlayer = Players[eTargetPlayer];
   
	local pPlayerCities:table = pPlayer:GetCities();
	for i, pLoopCity in pPlayerCities:Members() do
		local iDistance = Map.GetPlotDistance(iX, iY, pLoopCity:GetX(), pLoopCity:GetY());
		if (iDistance < iShortestDistance) then
			pCity = pLoopCity;
			iShortestDistance = iDistance;
		end
	end

	if (pCity == nullptr) then
		print ("No target city found of player " .. tostring(eTargetPlayer) .. "in attack from " .. tostring(iX) .. ", " .. tostring(iY));
	end
   
    return pCity;
end

-- ===========================================================================
local function TimeToAddForces ( iTribe, iTurn)

	local iTurnAddition = 0;
	local iQuickestTurnaround = 1;
	local iFirstTurn = 0;
	local iLastTurn = 60;

	if (iDifficulty < 2) then
		iTurnAddition = 3;
	elseif (iDifficulty < 4) then
		iTurnAddition = 2;
	elseif (iDifficulty < 6) then
		iTurnAddition = 1;
	end

	if (iTribe == iMarienburgTribeNumber) then
		iQuickestTurnaround = 3;
		iFirstTurn = 5;
		iLastTurn = 20;
	elseif (iTribe == iMoscowTribeNumber) then
		iQuickestTurnaround = 7;
		iFirstTurn = 20;
		iLastTurn = 55;
	elseif (iTribe == iHordeTribeNumber) then
		iQuickestTurnaround = 6;
		iFirstTurn = 15;
		iLastTurn = 55;
	elseif (iTribe == iOttomanTribeNumber) then
		iQuickestTurnaround = 5;
		iFirstTurn = 25;
		iLastTurn = 48;
	end

	local iTurnMultiple = iQuickestTurnaround + iTurnAddition;

	if iTurn < iFirstTurn then 
		return false;
	elseif iTurn > iLastTurn then
		return false;
	elseif (iTurn % iTurnMultiple == 0) then
		local szTribe = "";
		if (iTribe == iMarienburgTribeNumber) then
			szTribe = "LOC_BARBARIAN_KNIGHTS_1";
		elseif (iTribe == iMoscowTribeNumber) then
			szTribe = "LOC_BARBARIAN_RUS_1";
		elseif (iTribe == iHordeTribeNumber) then
			szTribe = "LOC_BARBARIAN_CAVALRY_2";
		elseif (iTribe == iOttomanTribeNumber) then
			szTribe = "LOC_BARBARIAN_OTTOMAN_1";
		end
		print ("Turn " .. tostring(iTurn) .. ", Adding forces for tribe: " .. szTribe);
		local eventKey = GameInfo.EventPopupData["SCENARIO_POLAND_INVASION_HERE"].Type;
		local eventEffectString = Locale.Lookup("LOC_POLAND_SCENARIO_INVASION_HERE", szTribe);
		ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = 0, EventKey = eventKey, EventEffect = eventEffectString });
		ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = 1, EventKey = eventKey, EventEffect = eventEffectString });
		ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = 2, EventKey = eventKey, EventEffect = eventEffectString });
		return true;
	end
   
    return false;
end
-- ===========================================================================
local function AddBarbarians(iTurn)

	print ("Adding Barbarians for Turn: " .. tostring(iTurn));

	local pBarbManager = Game.GetBarbarianManager();
	local eBarbarianTribeType = 1;  -- Cavalry
	local iPlotIndex;
	local iTribe;
	local iRange = 3;

	-- Teutonic Knights
	if (iTurn == 2) then
		eBarbarianTribeType = 3;  -- Knights
		iPlotIndex = Map.GetPlot(11,20):GetIndex();  -- Marienburg
		iMarienburgTribeNumber = CreateTribeAt(eBarbarianTribeType, iPlotIndex);
		pBarbManager:CreateTribeUnits(iMarienburgTribeNumber, "CLASS_HEAVY_CAVALRY", 5, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iMarienburgTribeNumber, "CLASS_RANGED", 4, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iMarienburgTribeNumber, "CLASS_SIEGE", 2, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iMarienburgTribeNumber, "CLASS_BATTERING_RAM", 1, iPlotIndex, iRange);

		iPlotIndex = Map.GetPlot(16,27):GetIndex();  -- Riga
		iRigaTribeNumber = CreateTribeAt(eBarbarianTribeType, iPlotIndex);
		pBarbManager:CreateTribeUnits(iRigaTribeNumber, "CLASS_HEAVY_CAVALRY", 4, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iRigaTribeNumber, "CLASS_RANGED", 3, iPlotIndex, iRange);

	--     Knight Attacks
	elseif (iTurn == 3) then
		local pTargetCity = FindNearestTargetCity(eGdansk, 11, 20);
		if (pTargetCity ~= nullptr) then
			pBarbManager:StartOperationWithCityTarget(iMarienburgTribeNumber, "Barbarian City Assault", eGdansk, pTargetCity:GetID());
		end

	elseif (iTurn == 4) then
		local pTargetCity = FindNearestTargetCity(eRadziwillPlayer, 16, 27);
		if (pTargetCity ~= nullptr) then
			pBarbManager:StartOperationWithCityTarget(iRigaTribeNumber, "Barbarian Attack", eRadziwillPlayer, pTargetCity:GetID());
		end
			
	--     Knight reinforcements
	elseif (TimeToAddForces(iMarienburgTribeNumber, iTurn)) then
		eBarbarianTribeType = 3;  -- Knights
		iPlotIndex = Map.GetPlot(11,20):GetIndex();  -- Marienburg
		pBarbManager:CreateTribeUnits(iMarienburgTribeNumber, "CLASS_HEAVY_CAVALRY", 3, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iMarienburgTribeNumber, "CLASS_RANGED", 2, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iMarienburgTribeNumber, "CLASS_SIEGE", 1, iPlotIndex, iRange);

		iPlotIndex = Map.GetPlot(16,27):GetIndex();  -- Riga
		pBarbManager:CreateTribeUnits(iRigaTribeNumber, "CLASS_HEAVY_CAVALRY", 3, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iRigaTribeNumber, "CLASS_RANGED", 2, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iRigaTribeNumber, "CLASS_SIEGE", 1, iPlotIndex, iRange);
	end

	-- Rus
	if (iTurn == 2) then
		eBarbarianTribeType = 4; 
		iPlotIndex = Map.GetPlot(27,28):GetIndex();  -- Novgorod
		iNovgorodTribeNumber = CreateTribeAt(eBarbarianTribeType, iPlotIndex);
		pBarbManager:CreateTribeUnits(iNovgorodTribeNumber, "CLASS_MELEE", 2, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iNovgorodTribeNumber, "CLASS_RANGED", 1, iPlotIndex, iRange);

		iPlotIndex = Map.GetPlot(37,21):GetIndex();  -- Moscow
		iMoscowTribeNumber = CreateTribeAt(eBarbarianTribeType, iPlotIndex);
		pBarbManager:CreateTribeUnits(iMoscowTribeNumber, "CLASS_HEAVY_CAVALRY", 2, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iMoscowTribeNumber, "CLASS_MELEE", 5, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iMoscowTribeNumber, "CLASS_RANGED", 4, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iMoscowTribeNumber, "CLASS_SIEGE", 2, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iMoscowTribeNumber, "CLASS_BATTERING_RAM", 1, iPlotIndex, iRange);
	
	elseif (iTurn == 10) then
		local pTargetCity = FindNearestTargetCity(eRadziwillPlayer, 37, 21);
		if (pTargetCity ~= nullptr) then
			pBarbManager:StartOperationWithCityTarget(iMoscowTribeNumber, "Barbarian City Assault", eRadziwillPlayer, pTargetCity:GetID());
		end

	--     Rus reinforcements
	elseif (TimeToAddForces(iMoscowTribeNumber, iTurn)) then
		eBarbarianTribeType = 4;
		iPlotIndex = Map.GetPlot(37,21):GetIndex();  -- Moscow
		pBarbManager:CreateTribeUnits(iMoscowTribeNumber, "CLASS_HEAVY_CAVALRY", 2, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iMoscowTribeNumber, "CLASS_MELEE", 5, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iMoscowTribeNumber, "CLASS_RANGED", 4, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iMoscowTribeNumber, "CLASS_SIEGE", 2, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iMoscowTribeNumber, "CLASS_BATTERING_RAM", 1, iPlotIndex, iRange);
		local pTargetCity = FindNearestTargetCity(eRadziwillPlayer, 37, 21);
		if (pTargetCity ~= nullptr) then
			pBarbManager:StartOperationWithCityTarget(iMoscowTribeNumber, "Barbarian City Assault", eRadziwillPlayer, pTargetCity:GetID());
		end
	end

	-- Golden Horde
	if (iTurn == 2) then
		eBarbarianTribeType = 6;  -- Horde
		iPlotIndex = Map.GetPlot(32, 3):GetIndex();  
		iHordeTribeNumber = CreateTribeAt(eBarbarianTribeType, iPlotIndex);
		pBarbManager:CreateTribeUnits(iHordeTribeNumber, "CLASS_LIGHT_CAVALRY", 5, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iHordeTribeNumber, "CLASS_RANGED_CAVALRY", 4, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iHordeTribeNumber, "CLASS_SIEGE", 2, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iHordeTribeNumber, "CLASS_BATTERING_RAM", 1, iPlotIndex, iRange);

	--       Horde Attacks
	elseif (iTurn == 4) then
		local pTargetCity = FindNearestTargetCity(eOstrogskiPlayer, 32, 3);
		if (pTargetCity ~= nullptr) then
			pBarbManager:StartOperationWithCityTarget(iHordeTribeNumber, "Barbarian City Assault", eOstrogskiPlayer, pTargetCity:GetID());
		end

	--		 Horde reinforcements
	elseif (TimeToAddForces(iHordeTribeNumber, iTurn)) then
		eBarbarianTribeType = 6;  -- Horde
		iPlotIndex = Map.GetPlot(32, 3):GetIndex();  
		pBarbManager:CreateTribeUnits(iHordeTribeNumber, "CLASS_LIGHT_CAVALRY", 5, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iHordeTribeNumber, "CLASS_RANGED_CAVALRY", 4, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iHordeTribeNumber, "CLASS_SIEGE", 2, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iHordeTribeNumber, "CLASS_BATTERING_RAM", 1, iPlotIndex, iRange);
		local pTargetCity = FindNearestTargetCity(eOstrogskiPlayer, 32, 3);
		if (pTargetCity ~= nullptr) then
			pBarbManager:StartOperationWithCityTarget(iHordeTribeNumber, "Barbarian City Assault", eOstrogskiPlayer, pTargetCity:GetID());
		end
	end

	-- Moldava
	if (iTurn == 2) then
		eBarbarianTribeType = 2;  -- Melee
		iPlotIndex = Map.GetPlot(21, 0):GetIndex();  
		iMoldovaTribeNumber = CreateTribeAt(eBarbarianTribeType, iPlotIndex);
		pBarbManager:CreateTribeUnits(iMoldovaTribeNumber, "CLASS_MELEE", 2, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iMoldovaTribeNumber, "CLASS_RANGED", 1, iPlotIndex, iRange);

	--     Moldava reinforcements
	elseif (iTurn == 20 or iTurn == 30 or iTurn == 40) then
		eBarbarianTribeType = 2;  -- Melee
		iPlotIndex = Map.GetPlot(21, 0):GetIndex();  
		pBarbManager:CreateTribeUnits(iMoldovaTribeNumber, "CLASS_MELEE", 2, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iMoldovaTribeNumber, "CLASS_RANGED", 1, iPlotIndex, iRange);
	end

	-- Ottoman
	if (iTurn == 13) then
		eBarbarianTribeType = 5;  -- Ottoman
		iPlotIndex = Map.GetPlot(9, 0):GetIndex();  
		iOttomanTribeNumber = CreateTribeAt(eBarbarianTribeType, iPlotIndex);
		pBarbManager:CreateTribeUnits(iOttomanTribeNumber, "CLASS_JANISSARY", 4, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iOttomanTribeNumber, "CLASS_RANGED", 3, iPlotIndex, iRange);
		local pTargetCity = FindNearestTargetCity(ePotockiPlayer, 9, 0);
		if (pTargetCity ~= nullptr) then
			pBarbManager:StartOperationWithCityTarget(iOttomanTribeNumber, "Barbarian Attack", ePotockiPlayer, pTargetCity:GetID());
		end

	--     Ottoman reinforcements
	elseif (TimeToAddForces(iOttomanTribeNumber, iTurn)) then
		eBarbarianTribeType = 5;  -- Ottoman;
		iPlotIndex = Map.GetPlot(9, 0):GetIndex();
		pBarbManager:CreateTribeUnits(iOttomanTribeNumber, "CLASS_JANISSARY", 6, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iOttomanTribeNumber, "CLASS_RANGED", 4, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iOttomanTribeNumber, "CLASS_SIEGE", 2, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iOttomanTribeNumber, "CLASS_BATTERING_RAM", 1, iPlotIndex, iRange);
		local pTargetCity = FindNearestTargetCity(eVienna, 9, 0);
		if (pTargetCity ~= nullptr) then
			pBarbManager:StartOperationWithCityTarget(iOttomanTribeNumber, "Barbarian City Assault", eVienna, pTargetCity:GetID());
		else
			pTargetCity = FindNearestTargetCity(ePotockiPlayer, 9, 0);
			if (pTargetCity ~= nullptr) then
				pBarbManager:StartOperationWithCityTarget(iOttomanTribeNumber, "Barbarian City Assault", ePotockiPlayer, pTargetCity:GetID());
			end
		end

	--     Ottoman final assault
	elseif (iTurn == 50) then
		eBarbarianTribeType = 5;  -- Ottoman;
		iPlotIndex = Map.GetPlot(9, 0):GetIndex();
		pBarbManager:CreateTribeUnits(iOttomanTribeNumber, "CLASS_JANISSARY", 10, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iOttomanTribeNumber, "CLASS_RANGED", 6, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iOttomanTribeNumber, "CLASS_SIEGE", 3, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iOttomanTribeNumber, "CLASS_BATTERING_RAM", 2, iPlotIndex, iRange);
		local pTargetCity = FindNearestTargetCity(eVienna, 9, 0);
		if (pTargetCity ~= nullptr) then
			pBarbManager:StartOperationWithCityTarget(iOttomanTribeNumber, "Barbarian City Assault", eVienna, pTargetCity:GetID());
		else
			pTargetCity = FindNearestTargetCity(ePotockiPlayer, 9, 0);
			if (pTargetCity ~= nullptr) then
				pBarbManager:StartOperationWithCityTarget(iOttomanTribeNumber, "Barbarian City Assault", ePotockiPlayer, pTargetCity:GetID());
			end
		end
		local eventKey = GameInfo.EventPopupData["SCENARIO_POLAND_INVASION_HERE"].Type;
		local eventEffectString = Locale.Lookup("LOC_POLAND_SCENARIO_INVASION_HERE", "LOC_BARBARIAN_OTTOMAN_1");
		ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = 0, EventKey = eventKey, EventEffect = eventEffectString });
		ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = 1, EventKey = eventKey, EventEffect = eventEffectString });
		ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = 2, EventKey = eventKey, EventEffect = eventEffectString });
	end

	-- Swedes, Livonia
	if (iTurn == 40) then
		local pTargetCity = FindNearestTargetCity(eRadziwillPlayer, 19, 33);
		if (pTargetCity ~= nullptr) then
			local eventKey = GameInfo.EventPopupData["SCENARIO_POLAND_EARLY_WARNING"].Type;
			local eventEffectString = Locale.Lookup("LOC_POLAND_SCENARIO_EARLY_WARNING", "LOC_BARBARIAN_SWEDES_1", 2, pTargetCity:GetName());
			ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = 0, EventKey = eventKey, EventEffect = eventEffectString });
			ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = 1, EventKey = eventKey, EventEffect = eventEffectString });
			ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = 2, EventKey = eventKey, EventEffect = eventEffectString });
		end
	end
	if (iTurn == 42) then
		eBarbarianTribeType = 2;  -- Swedes
		iPlotIndex = Map.GetPlot(19, 33):GetIndex();  
		iSwedishLivoniaTribeNumber = CreateTribeAt(eBarbarianTribeType, iPlotIndex);
		pBarbManager:CreateTribeUnits(iSwedishLivoniaTribeNumber, "CLASS_MELEE", 8, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iSwedishLivoniaTribeNumber, "CLASS_RANGED", 4, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iSwedishLivoniaTribeNumber, "CLASS_SIEGE", 4, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iSwedishLivoniaTribeNumber, "CLASS_BATTERING_RAM", 1, iPlotIndex, iRange);
		local pTargetCity = FindNearestTargetCity(eRadziwillPlayer, 19, 33);
		if (pTargetCity ~= nullptr) then
			pBarbManager:StartOperationWithCityTarget(iSwedishLivoniaTribeNumber, "Barbarian City Assault", eRadziwillPlayer, pTargetCity:GetID());
		end
		local eventKey = GameInfo.EventPopupData["SCENARIO_POLAND_INVASION_HERE"].Type;
		local eventEffectString = Locale.Lookup("LOC_POLAND_SCENARIO_INVASION_HERE", "LOC_BARBARIAN_SWEDES_1");
		ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = 0, EventKey = eventKey, EventEffect = eventEffectString });
		ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = 1, EventKey = eventKey, EventEffect = eventEffectString });
		ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = 2, EventKey = eventKey, EventEffect = eventEffectString });
	end

	-- Swedes, Prussia
	if (iTurn == 45) then
		local pTargetCity = FindNearestTargetCity(ePotockiPlayer, 11, 20);
		if (pTargetCity ~= nullptr) then
			local eventKey = GameInfo.EventPopupData["SCENARIO_POLAND_EARLY_WARNING"].Type;
			local eventEffectString = Locale.Lookup("LOC_POLAND_SCENARIO_EARLY_WARNING", "LOC_BARBARIAN_SWEDES_1", 2, pTargetCity:GetName());
			ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = 0, EventKey = eventKey, EventEffect = eventEffectString });
			ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = 1, EventKey = eventKey, EventEffect = eventEffectString });
			ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = 2, EventKey = eventKey, EventEffect = eventEffectString });
		end
	end
	if (iTurn == 47) then
		eBarbarianTribeType = 2;  -- Swedes
		iPlotIndex = Map.GetPlot(11, 20):GetIndex();  
		iSwedishPrussiaTribeNumber = CreateTribeAt(eBarbarianTribeType, iPlotIndex);
		pBarbManager:CreateTribeUnits(iSwedishPrussiaTribeNumber, "CLASS_MELEE", 10, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iSwedishPrussiaTribeNumber, "CLASS_RANGED", 6, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iSwedishPrussiaTribeNumber, "CLASS_SIEGE", 4, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iSwedishPrussiaTribeNumber, "CLASS_BATTERING_RAM", 1, iPlotIndex, iRange);
		local pTargetCity = FindNearestTargetCity(ePotockiPlayer, 11, 20);
		if (pTargetCity ~= nullptr) then
			pBarbManager:StartOperationWithCityTarget(iSwedishLivoniaTribeNumber, "Barbarian City Assault", ePotockiPlayer, pTargetCity:GetID());
		end
		local eventKey = GameInfo.EventPopupData["SCENARIO_POLAND_INVASION_HERE"].Type;
		local eventEffectString = Locale.Lookup("LOC_POLAND_SCENARIO_INVASION_HERE", "LOC_BARBARIAN_SWEDES_1");
		ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = 0, EventKey = eventKey, EventEffect = eventEffectString });
		ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = 1, EventKey = eventKey, EventEffect = eventEffectString });
		ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = 2, EventKey = eventKey, EventEffect = eventEffectString });
	end

	-- Swedes, Pomerania
	if (iTurn == 51) then
		local pTargetCity = FindNearestTargetCity(ePotockiPlayer, 0, 16);
		if (pTargetCity ~= nullptr) then
			local eventKey = GameInfo.EventPopupData["SCENARIO_POLAND_EARLY_WARNING"].Type;
			local eventEffectString = Locale.Lookup("LOC_POLAND_SCENARIO_EARLY_WARNING", "LOC_BARBARIAN_SWEDES_1", 2, pTargetCity:GetName());
			ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = 0, EventKey = eventKey, EventEffect = eventEffectString });
			ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = 1, EventKey = eventKey, EventEffect = eventEffectString });
			ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = 2, EventKey = eventKey, EventEffect = eventEffectString });
		end
	end
	if (iTurn == 53) then
		eBarbarianTribeType = 2;  -- Swedes
		iPlotIndex = Map.GetPlot(0, 16):GetIndex();  
		iSwedishPomeraniaTribeNumber = CreateTribeAt(eBarbarianTribeType, iPlotIndex);
		pBarbManager:CreateTribeUnits(iSwedishPomeraniaTribeNumber, "CLASS_MELEE", 8, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iSwedishPomeraniaTribeNumber, "CLASS_RANGED", 4, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iSwedishPomeraniaTribeNumber, "CLASS_SIEGE", 4, iPlotIndex, iRange);
		pBarbManager:CreateTribeUnits(iSwedishPomeraniaTribeNumber, "CLASS_BATTERING_RAM", 1, iPlotIndex, iRange);
		local pTargetCity = FindNearestTargetCity(ePotockiPlayer, 0, 16);
		if (pTargetCity ~= nullptr) then
			pBarbManager:StartOperationWithCityTarget(iSwedishPomeraniaTribeNumber, "Barbarian City Assault", ePotockiPlayer, pTargetCity:GetID());
		end
		local eventKey = GameInfo.EventPopupData["SCENARIO_POLAND_INVASION_HERE"].Type;
		local eventEffectString = Locale.Lookup("LOC_POLAND_SCENARIO_INVASION_HERE", "LOC_BARBARIAN_SWEDES_1");
		ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = 0, EventKey = eventKey, EventEffect = eventEffectString });
		ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = 1, EventKey = eventKey, EventEffect = eventEffectString });
		ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = 2, EventKey = eventKey, EventEffect = eventEffectString });
	end
end

-- ===========================================================================
local function OnPlayerTurnActivated( player )
	local currentTurn = Game.GetCurrentGameTurn();
	
	print ("OnPlayerTurnActivated: Player " .. tostring(player) .. ", Turn " .. tostring(currentTurn));

	if (player == 0 and currentTurn == 1) then
		SetInitialVisibility();
	end

	if (player == 0) then
		if (currentTurn > lastLocalTurnNumber) then
			AddBarbarians(currentTurn);
			lastLocalTurnNumber = currentTurn;
		end
	end
end

-- ===========================================================================
GameEvents.PlayerTurnStarted.Add(OnPlayerTurnActivated);

-- ===========================================================================
--	Check ENGINE Events
-- ===========================================================================
function OnCityAddedToMap( playerID: number, cityID : number, cityX : number, cityY : number )
	
	print("CityAddedToMap - " .. tostring(playerID) .. ":" .. tostring(cityID) .. " " .. tostring(cityX) .. "x" .. tostring(cityY));

	local pGameReligion:table = Game.GetReligion();
	local pPlayer = Players[playerID];
	local pPlayerCities:table = pPlayer:GetCities();
	local pReligion = pPlayer:GetReligion();

	if (pReligion:CanCreatePantheon()) then
		return;
	end

	if (playerID == eOstrogskiPlayer) then
		local eReligion = GameInfo.Religions["RELIGION_ORTHODOXY"].Index;
		if (bOrthodoxyFounded == false) then
			pReligion:SetHolyCity(pPlayerCities:FindID(cityID));
			pGameReligion:FoundReligion(pPlayer:GetID(), eReligion);
			pGameReligion:AddBelief(pPlayer:GetID(),  GameInfo.Beliefs["BELIEF_TITHE"].Index);
			pGameReligion:AddBelief(pPlayer:GetID(),  GameInfo.Beliefs["BELIEF_GURDWARA"].Index);
			bOrthodoxyFounded = true;
		end
		for i, pCity in pPlayerCities:Members() do
			local pCityReligion = pCity:GetReligion();
			if (pCityReligion:GetMajorityReligion() ~= eReligion) then
				pCityReligion:SetAllCityToReligion(eReligion);
			end
		end
	else
		local eReligion = GameInfo.Religions["RELIGION_CATHOLICISM"].Index;
		if (bCatholicismFounded == false) then
			pReligion:SetHolyCity(pPlayerCities:FindID(cityID));
			pGameReligion:FoundReligion(pPlayer:GetID(), eReligion);
			pGameReligion:AddBelief(pPlayer:GetID(),  GameInfo.Beliefs["BELIEF_RELIGIOUS_COMMUNITY"].Index);
			pGameReligion:AddBelief(pPlayer:GetID(),  GameInfo.Beliefs["BELIEF_CATHEDRAL"].Index);
			bCatholicismFounded = true;
		end
		for i, pCity in pPlayerCities:Members() do
			local pCityReligion = pCity:GetReligion();
			if (pCityReligion:GetMajorityReligion() ~= eReligion) then
				pCityReligion:SetAllCityToReligion(eReligion);
			end
		end
	end
end
GameEvents.CityBuilt.Add(OnCityAddedToMap );


-- AI function for use in ending a rapid expansion strategy
function HasFourCities(PlayerId : number, Threshold : number)
	local pPlayer = Players[PlayerId];
	if ( pPlayer ~= null ) then
		local pPlayerCities = pPlayer:GetCities();
		return pPlayerCities:GetCount() >= 4;
	end
	return false;
end
GameEvents.HasFourCities.Add(HasFourCities);


Initialize();