-- ===========================================================================
--
--	Viking Scenario
-- ===========================================================================


-- ===========================================================================

function Initialize()
end
-- ===========================================================================
function InitializeNewGame()
	Game.GetGameDiplomacy():SetAlliesShareVisFlag(false);
	local aPlayers = PlayerManager.GetAlive();
	Players[0]:GetDiplomacy():SetHasMet(1);
	Players[0]:GetDiplomacy():SetHasMet(2);
	Players[1]:GetDiplomacy():SetHasMet(2);
	Players[0]:GetDiplomacy():SetHasAllied(1, true);
	Players[0]:GetDiplomacy():SetHasAllied(2, true);
	Players[1]:GetDiplomacy():SetHasAllied(2, true);
	Players[0]:GetDiplomacy():SetHasDelegationAt(1, true);
	Players[0]:GetDiplomacy():SetHasDelegationAt(2, true);
	Players[1]:GetDiplomacy():SetHasDelegationAt(0, true);
	Players[1]:GetDiplomacy():SetHasDelegationAt(2, true);
	Players[2]:GetDiplomacy():SetHasDelegationAt(0, true);
	Players[2]:GetDiplomacy():SetHasDelegationAt(1, true);
	Players[0]:GetDiplomacy():RecheckVisibilityOnAll();
	Players[1]:GetDiplomacy():RecheckVisibilityOnAll();
	Players[2]:GetDiplomacy():RecheckVisibilityOnAll();

	for loop, pPlayer in ipairs(aPlayers) do
		local iPlayer = pPlayer:GetID();
		local pCulture:table = pPlayer:GetCulture();
		local pScience:table = pPlayer:GetTechs();
		local pDiplomacy:table = pPlayer:GetDiplomacy();
		local pReligion:table = pPlayer:GetReligion();
		
		pCulture:SetCivic(GameInfo.Civics["CIVIC_CODE_OF_LAWS"].Index, true);
		pScience:SetTech(GameInfo.Technologies["TECH_SAILING"].Index, true);
		local config = PlayerConfigurations[iPlayer];
		if(GameInfo.Civilizations["CIVILIZATION_AL_ANDALUS"].Hash ==
		config:GetCivilizationTypeID() or 
			GameInfo.Civilizations["CIVILIZATION_BYZANTIUM"].Hash == config:GetCivilizationTypeID() or 
			GameInfo.Civilizations["CIVILIZATION_FRANCIA"].Hash == config:GetCivilizationTypeID() ) then
			pCulture:SetCivic(GameInfo.Civics["CIVIC_MONOTHEISM"].Index, true);
			pCulture:SetCivic( GameInfo.Civics["CIVIC_DIVINE_RIGHT"].Index, true);
			pCulture:SetCivic(GameInfo.Civics["CIVIC_THEOLOGY"].Index, true);
			pCulture:SetCivic(GameInfo.Civics["CIVIC_REFORMED_CHURCH"].Index, true);
			pCulture:UnlockGovernment(GameInfo.Governments["GOVERNMENT_THEOCRACY"].Index);
			pCulture:SetCurrentGovernment(GameInfo.Governments["GOVERNMENT_THEOCRACY"].Index);

			pScience:SetTech(GameInfo.Technologies["TECH_LONGSHIPS"].Index, true);
			pScience:SetTech(GameInfo.Technologies["TECH_MEAD_HALLS"].Index, true);
			pScience:SetTech(GameInfo.Technologies["TECH_APPRENTICESHIP"].Index, true);
			pScience:SetTech(GameInfo.Technologies["TECH_LANCES"].Index, true);
			pScience:SetTech(GameInfo.Technologies["TECH_STIRRUPS"].Index, true);
			pScience:SetTech(GameInfo.Technologies["TECH_CASTLES"].Index, true);
		else
			pCulture:UnlockGovernment(GameInfo.Governments["GOVERNMENT_CHIEFDOM"].Index);
			pCulture:SetCurrentGovernment(GameInfo.Governments["GOVERNMENT_CHIEFDOM"].Index);
		end

		for loop2, pPlayer2 in ipairs(aPlayers) do
			local iPlayer2 = pPlayer2:GetID();
			
			if(iPlayer2 ~= iPlayer and pPlayer2:IsMajor() == true) then
				if(pDiplomacy:HasMet(iPlayer2) == false) then
					pDiplomacy:SetHasMet(iPlayer2);
				end
			end
		end

		for loop3, pPlayer3 in ipairs(aPlayers) do
			local iPlayer3 = pPlayer3:GetID();
			
			if(iPlayer3 ~= iPlayer and pPlayer3:IsMajor() == true) then
				if(pDiplomacy:IsAtWarWith(iPlayer3) == false and pDiplomacy:HasAllied(iPlayer3) == false) then
					pDiplomacy:DeclareWarOn(iPlayer3, 0);
				end
			end
		end
	end
end

GameEvents.OnFaithEarned.Add(function (ePlayer, iFaithAmouth)
	local player = Players[ePlayer];
	if (player ~= nil and player:IsBarbarian() == false) then
		local pReligion:table = player:GetReligion();

		if(pReligion:GetReligionInMajorityOfCities() ~=  GameInfo.Religions["RELIGION_CATHOLICISM"].Index and 
			pReligion:GetReligionInMajorityOfCities() ~=  GameInfo.Religions["RELIGION_ISLAM"].Index) then
			local bAddedFaith = 0;
			for iCity in player:GetCities():Members() do
				if(bAddedFaith == 0) then
					local pCity:table = player:GetCities():FindID(iCity);
					if(pCity ~= nil) then
						if(pCity:GetReligion():GetMajorityReligion() ~= GameInfo.Religions["RELIGION_CATHOLICISM"].Index and 
							pCity:GetReligion():GetMajorityReligion() ~= GameInfo.Religions["RELIGION_ISLAM"].Index) then
							
							local currentTurn = Game.GetCurrentGameTurn();
							if(currentTurn >= 2) then
								local iPressure = math.floor(iFaithAmouth / 2);
								pCity:GetReligion():AddReligiousPressure(0, GameInfo.Religions["RELIGION_CATHOLICISM"].Index, iPressure, -1);
								local message = Locale.Lookup("LOC_KILL_FAITH_PLUS", iPressure, GameInfo.Religions["RELIGION_CATHOLICISM"].Name);
								Game.AddWorldViewText(0, message, pCity:GetX(), pCity:GetY());
								bAddedFaith = 1;
							end
						end
					end
				end
			end
		end

		local iPlayer = player:GetID();
		local config = PlayerConfigurations[iPlayer];
		if(GameInfo.Civilizations["CIVILIZATION_NORWAY"].Hash ~= config:GetCivilizationTypeID() and 
					GameInfo.Civilizations["CIVILIZATION_SWEDEN"].Hash ~= config:GetCivilizationTypeID() and
					GameInfo.Civilizations["CIVILIZATION_DENMARK"].Hash ~= config:GetCivilizationTypeID()) then

					local pGold:table = player:GetTreasury();
					pGold:ChangeGoldBalance(iFaithAmouth * 2);

					pReligion:ChangeFaithBalance(-iFaithAmouth);
		end
	end
end);

GameEvents.OnPillage.Add(function (UnitOwner, UnitId, ImprovementType, BuildingType)
	local player = Players[UnitOwner];
	if (player ~= nil and player:IsBarbarian() == false) then
		local unit = player:GetUnits():FindID(UnitId);
		local pReligion:table = player:GetReligion();

		if(pReligion:GetReligionInMajorityOfCities() ~=  GameInfo.Religions["RELIGION_CATHOLICISM"].Index and 
			pReligion:GetReligionInMajorityOfCities() ~=  GameInfo.Religions["RELIGION_ISLAM"].Index) then
			
			if(pReligion:GetPantheon() == GameInfo.Beliefs["BELIEF_ODIN1"].Index or
				pReligion:GetPantheon() == GameInfo.Beliefs["BELIEF_ODIN2"].Index or
				pReligion:GetPantheon() == GameInfo.Beliefs["BELIEF_ODIN3"].Index) then
				
				if(unit ~= nil) then
					local pGold:table = player:GetTreasury();
					pGold:ChangeGoldBalance(100);
					player:ChangeScoringScenario2(50);

					local message = Locale.Lookup("LOC_WORLD_POINTS_FLOATER", 60);
					Game.AddWorldViewText(0, message, unit:GetX(), unit:GetY());
					local message2 = Locale.Lookup("LOC_KILL_GOLD", 100);
					Game.AddWorldViewText(0, message2, unit:GetX(), unit:GetY());
				end
			end
		else
			local message = Locale.Lookup("LOC_WORLD_POINTS_FLOATER", 10);
			Game.AddWorldViewText(0, message, unit:GetX(), unit:GetY());
		end
	end
end);

GameEvents.OnCityPopulationChanged.Add(function (CityOwner, CityId, ChangeAmount)
	local player = Players[CityOwner];
	if (player ~= nil and player:IsBarbarian() == false and ChangeAmount > 0) then
		local city = player:GetCities():FindID(CityId);
		local pReligion:table = player:GetReligion();

		if(pReligion:GetReligionInMajorityOfCities() ==  GameInfo.Religions["RELIGION_CATHOLICISM"].Index or 
			pReligion:GetReligionInMajorityOfCities() ==  GameInfo.Religions["RELIGION_ISLAM"].Index) then
			
			if(city ~= nil) then
				player:ChangeScoringScenario2(ChangeAmount * 50);
				local message = Locale.Lookup("LOC_WORLD_POINTS_FLOATER", ChangeAmount * 50 + 10);
				Game.AddWorldViewText(0, message, city:GetX(), city:GetY());
			end
		else
			local message = Locale.Lookup("LOC_WORLD_POINTS_FLOATER", 10);
			Game.AddWorldViewText(0, message, city:GetX(), city:GetY());
		end
	end
end);

GameEvents.OnGreatPersonActivated.Add(function (UnitOwner, UnitId, GreatPersonType, GreatPersonClass)
	local player = Players[UnitOwner];
	if (player ~= nil and player:IsBarbarian() == false) then
		local unit = player:GetUnits():FindID(UnitId);
		
		if(GameInfo.GreatPersonClasses["GREAT_PERSON_CLASS_ADMIRAL"].Index == GreatPersonClass) then
			if(unit ~= nil) then
				local iScoreBonus = 0;
				
				local iVinland = 0;
				local aPlayers = PlayerManager.GetAlive();
				for loop, pPlayer in ipairs(aPlayers) do
					if(pPlayer ~= nil) then
						iVinland = iVinland + pPlayer:GetScoringScenario1();
					end
				end
				
				if(iVinland == 0) then
					iScoreBonus = 1000;
				elseif(iVinland == 1000) then
					iScoreBonus = 500;
				elseif(iVinland == 500 or iVinland == 1500) then
					iScoreBonus = 300;
				else
					iScoreBonus = 100;
				end

				local pGold:table = player:GetTreasury();
				pGold:ChangeGoldBalance(500);
				
				player:ChangeScoringScenario1(iScoreBonus);
				local message = Locale.Lookup("LOC_WORLD_POINTS_FLOATER", iScoreBonus);
				Game.AddWorldViewText(0, message, unit:GetX(), unit:GetY());

				local message2 = Locale.Lookup("LOC_KILL_GOLD", 500);
				Game.AddWorldViewText(0, message2, unit:GetX(), unit:GetY());
				iVinland = iVinland + 1;
			end
		end
	end
end);

-- ===========================================================================
local function OnPlayerTurnActivated(player)
	local currentTurn = Game.GetCurrentGameTurn();
	local aPlayers = PlayerManager.GetAlive();

	if (player == 0 and currentTurn == 1) then

		SetInitialVisibility();
		
		local pGameReligion:table = Game.GetReligion();
		for loop, pPlayer in ipairs(aPlayers) do
			local pReligion:table = pPlayer:GetReligion();
			local iPlayer = pPlayer:GetID();
			if(pPlayer ~= nil and pPlayer:IsBarbarian() == false) then
				local config = PlayerConfigurations[iPlayer];
				if(GameInfo.Civilizations["CIVILIZATION_ROME"].Hash == config:GetCivilizationTypeID()) then 
					pGameReligion:FoundPantheon(pPlayer:GetID(),  GameInfo.Beliefs["BELIEF_FERTILITY_RITES"].Index);
					pReligion:SetHolyCity(pPlayer:GetCities():GetCapitalCity());
					pGameReligion:FoundReligion(pPlayer:GetID(),  GameInfo.Religions["RELIGION_CATHOLICISM"].Index);
					pGameReligion:AddBelief(pPlayer:GetID(),  GameInfo.Beliefs["BELIEF_DIVINE_INSPIRATION"].Index);
					pGameReligion:AddBelief(pPlayer:GetID(),  GameInfo.Beliefs["BELIEF_CATHEDRAL"].Index);
				elseif(GameInfo.Civilizations["CIVILIZATION_AL_ANDALUS"].Hash == config:GetCivilizationTypeID()) then 
					pGameReligion:FoundPantheon(pPlayer:GetID(),  GameInfo.Beliefs["BELIEF_GOD_OF_WAR"].Index);
					pReligion:SetHolyCity(pPlayer:GetCities():GetCapitalCity());
					pGameReligion:FoundReligion(pPlayer:GetID(),  GameInfo.Religions["RELIGION_ISLAM"].Index);
					pGameReligion:AddBelief(pPlayer:GetID(),  GameInfo.Beliefs["BELIEF_DIVINE_INSPIRATION"].Index);
					pGameReligion:AddBelief(pPlayer:GetID(),  GameInfo.Beliefs["BELIEF_MOSQUE"].Index);
				elseif(GameInfo.Civilizations["CIVILIZATION_FRANCIA"].Hash == config:GetCivilizationTypeID()) then
					pGameReligion:FoundPantheon(pPlayer:GetID(),  GameInfo.Beliefs["BELIEF_RIVER_GODDESS"].Index);
				elseif(GameInfo.Civilizations["CIVILIZATION_BYZANTIUM"].Hash == config:GetCivilizationTypeID()) then
					pGameReligion:FoundPantheon(pPlayer:GetID(),  GameInfo.Beliefs["BELIEF_GOD_OF_THE_SEA"].Index);
				elseif(GameInfo.Civilizations["CIVILIZATION_DENMARK"].Hash == config:GetCivilizationTypeID()) then
					pGameReligion:FoundPantheon(pPlayer:GetID(),  GameInfo.Beliefs["BELIEF_ODIN1"].Index);
				elseif(GameInfo.Civilizations["CIVILIZATION_NORWAY"].Hash == config:GetCivilizationTypeID()) then
					pGameReligion:FoundPantheon(pPlayer:GetID(),  GameInfo.Beliefs["BELIEF_ODIN2"].Index);
				elseif(GameInfo.Civilizations["CIVILIZATION_SWEDEN"].Hash == config:GetCivilizationTypeID()) then
					pGameReligion:FoundPantheon(pPlayer:GetID(),  GameInfo.Beliefs["BELIEF_ODIN3"].Index);
				end

				if(pPlayer:IsHuman() == false) then
					local iDifficulty = GameInfo.Difficulties[config:GetHandicapTypeID()].Index;
					if (iDifficulty > 3) then
						if(GameInfo.Civilizations["CIVILIZATION_NORWAY"].Hash == config:GetCivilizationTypeID()) then
							UnitManager.InitUnit(iPlayer, "UNIT_NORWEGIAN_BERSERKER", 46, 37)
						elseif(GameInfo.Civilizations["CIVILIZATION_SWEDEN"].Hash == config:GetCivilizationTypeID()) then
							UnitManager.InitUnit(iPlayer, "UNIT_NORWEGIAN_BERSERKER", 64, 37)
						elseif(GameInfo.Civilizations["CIVILIZATION_DENMARK"].Hash == config:GetCivilizationTypeID()) then
							UnitManager.InitUnit(iPlayer, "UNIT_NORWEGIAN_BERSERKER", 48, 32)
						end
					end
					if (iDifficulty > 4) then
						if(GameInfo.Civilizations["CIVILIZATION_NORWAY"].Hash == config:GetCivilizationTypeID()) then
							UnitManager.InitUnit(iPlayer, "UNIT_TRADER", 46, 36)
						elseif(GameInfo.Civilizations["CIVILIZATION_SWEDEN"].Hash == config:GetCivilizationTypeID()) then
							UnitManager.InitUnit(iPlayer, "UNIT_TRADER", 56, 38)
						elseif(GameInfo.Civilizations["CIVILIZATION_DENMARK"].Hash == config:GetCivilizationTypeID()) then
							UnitManager.InitUnit(iPlayer, "UNIT_TRADER", 51, 32)
						end
					end
					if (iDifficulty > 5) then
						if(GameInfo.Civilizations["CIVILIZATION_NORWAY"].Hash == config:GetCivilizationTypeID()) then
							UnitManager.InitUnit(iPlayer, "UNIT_NORWEGIAN_BERSERKER", 47, 38)
						elseif(GameInfo.Civilizations["CIVILIZATION_SWEDEN"].Hash == config:GetCivilizationTypeID()) then
							UnitManager.InitUnit(iPlayer, "UNIT_NORWEGIAN_BERSERKER", 64, 36)
						elseif(GameInfo.Civilizations["CIVILIZATION_DENMARK"].Hash == config:GetCivilizationTypeID()) then
							UnitManager.InitUnit(iPlayer, "UNIT_NORWEGIAN_BERSERKER", 48, 31)
						end
					end
					if (iDifficulty > 6) then
						if(GameInfo.Civilizations["CIVILIZATION_NORWAY"].Hash == config:GetCivilizationTypeID()) then
							local pGold:table = pPlayer:GetTreasury();
							pGold:ChangeGoldBalance(500);
						elseif(GameInfo.Civilizations["CIVILIZATION_SWEDEN"].Hash == config:GetCivilizationTypeID()) then
							local pGold:table = pPlayer:GetTreasury();
							pGold:ChangeGoldBalance(500);
						elseif(GameInfo.Civilizations["CIVILIZATION_DENMARK"].Hash == config:GetCivilizationTypeID()) then
							local pGold:table = pPlayer:GetTreasury();
							pGold:ChangeGoldBalance(500);
						end
					end
				end
			end
		end

		for loop2, pPlayer2 in ipairs(aPlayers) do
			local pReligion:table = pPlayer2:GetReligion();

			if(pPlayer2:IsBarbarian() == false) then
				pReligion:ChangeFaithBalance(20);
			end

			
			local iPlayer = pPlayer2:GetID();
			local iReligion = 0;
			local config = PlayerConfigurations[iPlayer];
			
			if(pPlayer2 ~= nil and pPlayer2:IsBarbarian() == false) then
				if(GameInfo.Civilizations["CIVILIZATION_NORWAY"].Hash == config:GetCivilizationTypeID() or 
					GameInfo.Civilizations["CIVILIZATION_SWEDEN"].Hash == config:GetCivilizationTypeID() or
					GameInfo.Civilizations["CIVILIZATION_DENMARK"].Hash == config:GetCivilizationTypeID()) then
							
						iReligion = -1;
				elseif(GameInfo.Civilizations["CIVILIZATION_AL_ANDALUS"].Hash == config:GetCivilizationTypeID() or
					GameInfo.Civilizations["CIVILIZATION_ASILAH"].Hash == config:GetCivilizationTypeID() or
					GameInfo.Civilizations["CIVILIZATION_BALARM"].Hash == config:GetCivilizationTypeID()or
					GameInfo.Civilizations["CIVILIZATION_KAIROUAN"].Hash == config:GetCivilizationTypeID()) then
						
						iReligion = 1;
				end
			

				if(iReligion == 0 or iReligion == 1) then
					for iCity in pPlayer2:GetCities():Members() do
						local pCity:table = pPlayer2:GetCities():FindID(iCity);
						if(pCity ~= nil) then
							if(iReligion == 0) then
								pCity:GetReligion():SetAllCityToReligion(GameInfo.Religions["RELIGION_CATHOLICISM"].Index);
							else
								pCity:GetReligion():SetAllCityToReligion(GameInfo.Religions["RELIGION_ISLAM"].Index);
							end
						end
					end
				end		
			end
		end
	end

	if (currentTurn == 1) then
		for loop, pPlayer in ipairs(aPlayers) do
			local iPlayer = pPlayer:GetID();
			
			if(pPlayer ~= nil and pPlayer:IsBarbarian() == false) then
				local pCulture:table = pPlayer:GetCulture();
				pCulture:SetCivicCompletedThisTurn(true);
			end
		end
	end

	if (player == 0  and currentTurn == 2) then
		local eBarbarianTribeType = 0;
		local pBarbManager = Game.GetBarbarianManager();
		local iTribe1 = 1;
		local iTribe2 = 2;
		local iPlotIndex1 = 0;
		local iPlotIndex2 = 0;
		local iRange = 3
		iPlotIndex1 = Map.GetPlot(26,44):GetIndex();
		iTribe1 = CreateTribeAt(eBarbarianTribeType, iPlotIndex1);
		pBarbManager:CreateTribeUnits(iTribe1, "PROMOTION_CLASS_NAVAL_RAIDER", 1, iPlotIndex1, iRange);
		pBarbManager:CreateTribeUnits(iTribe1, "PROMOTION_CLASS_NAVAL_RAIDER", 1, iPlotIndex1, iRange);
		iPlotIndex2 = Map.GetPlot(8,40):GetIndex();
		iTribe2 = CreateTribeAt(eBarbarianTribeType, iPlotIndex2);
		pBarbManager:CreateTribeUnits(iTribe2, "PROMOTION_CLASS_NAVAL_RAIDER", 1, iPlotIndex2, iRange);
		pBarbManager:CreateTribeUnits(iTribe2, "PROMOTION_CLASS_NAVAL_RAIDER", 1, iPlotIndex2, iRange);
	end

	if (player == 0 and currentTurn == 100) then
		for loop, pPlayer in ipairs(aPlayers) do
			local iPlayer = pPlayer:GetID();
		
			if(pPlayer ~= nil and pPlayer:IsBarbarian() == false) then
				local pReligion:table = pPlayer:GetReligion();
				local pGold:table = pPlayer:GetTreasury();
				local config = PlayerConfigurations[iPlayer];
				local iScore = 0; 
				if(GameInfo.Civilizations["CIVILIZATION_DENMARK"].Hash == config:GetCivilizationTypeID()) then
					iScore = math.floor(pReligion:GetFaithBalance() / 5);
					iScore = iScore + math.floor(pGold:GetGoldBalance() / 10);
					pPlayer:ChangeScoringScenario3(iScore);
				elseif(GameInfo.Civilizations["CIVILIZATION_NORWAY"].Hash == config:GetCivilizationTypeID()) then
					iScore = math.floor(pReligion:GetFaithBalance() / 5);
					iScore = iScore + math.floor(pGold:GetGoldBalance() / 10);
					pPlayer:ChangeScoringScenario3(iScore);
				elseif(GameInfo.Civilizations["CIVILIZATION_SWEDEN"].Hash == config:GetCivilizationTypeID()) then
					iScore = math.floor(pReligion:GetFaithBalance() / 5);
					iScore = iScore + math.floor(pGold:GetGoldBalance() / 10);
					pPlayer:ChangeScoringScenario3(iScore);
				end

				local message = Locale.Lookup("LOC_WORLD_POINTS_FLOATER", iScore);
				Game.AddWorldViewText(0, message, pPlayer:GetCities():GetCapitalCity():GetX(), pPlayer:GetCities():GetCapitalCity():GetY());
			end
		end
	end
end

-- ===========================================================================
function CreateTribeAt( eType, iPlotIndex )

	local pBarbManager = Game.GetBarbarianManager();

   -- Clear improvement in case one already here
   local pPlot = Map.GetPlotByIndex(iPlotIndex);
   ImprovementBuilder.SetImprovementType(pPlot, -1, NO_PLAYER);   

   local iTribeNumber = pBarbManager:CreateTribeOfType(eType, iPlotIndex);
   return iTribeNumber;
end


function SetInitialVisibility()
	print ("SetInitialVisibility");

	-- Apply updates to all AI Vikings
	local aPlayers = PlayerManager.GetAliveMajors();
	
	for loop, pPlayer in ipairs(aPlayers) do
		if(pPlayer ~= nil and pPlayer:IsBarbarian() == false) then
			if(pPlayer:IsHuman() == false) then
				local iPlayer = pPlayer:GetID();
				local config = PlayerConfigurations[iPlayer];
				if(GameInfo.Civilizations["CIVILIZATION_NORWAY"].Hash == config:GetCivilizationTypeID() or 
					GameInfo.Civilizations["CIVILIZATION_SWEDEN"].Hash == config:GetCivilizationTypeID() or
					GameInfo.Civilizations["CIVILIZATION_DENMARK"].Hash == config:GetCivilizationTypeID()) then
					local pCurPlayerVisibility = PlayersVisibility[iPlayer];
					if(pCurPlayerVisibility ~= nil) then
						for iX = 0, 40, 1 do
							for iY = 25, 45, 1 do
								local iPlotIndex = Map.GetPlot(iX, iY):GetIndex();
								pCurPlayerVisibility:ChangeVisibilityCount(iPlotIndex, 1);
							end
						end
					end
				end
			end
		end
	end
end

-- AI function for use in ending a naval exploration strategy
function FoundNewWorld(PlayerId : number, Threshold : number)
	-- Do something here to indicate when the player has found the new world (unknown at this time) - AWG
	return false;
end
GameEvents.FoundNewWorld.Add(FoundNewWorld);

GameEvents.PlayerTurnStarted.Add(OnPlayerTurnActivated);
LuaEvents.NewGameInitialized.Add(InitializeNewGame);
Initialize();