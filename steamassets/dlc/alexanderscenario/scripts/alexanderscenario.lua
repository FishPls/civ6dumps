-- ===========================================================================
--	Alexander Scenario
-- ===========================================================================


-- ===========================================================================
function Initialize()
end

-- ===========================================================================
function InitializeNewGame()
	-- Players have met
	Players[0]:GetDiplomacy():SetHasMet(1);
	Players[0]:GetDiplomacy():SetHasMet(2);
	Players[0]:GetDiplomacy():SetHasMet(3);
	Players[0]:GetDiplomacy():SetHasMet(4);
	Players[1]:GetDiplomacy():SetHasMet(2);
	Players[1]:GetDiplomacy():SetHasMet(3);
	Players[1]:GetDiplomacy():SetHasMet(4);

	

	-- Players are suzerain
	for i = 0, 6 do
		Players[1]:GetInfluence():GiveFreeTokenToPlayer(2);
		Players[1]:GetInfluence():GiveFreeTokenToPlayer(3);
		Players[1]:GetInfluence():GiveFreeTokenToPlayer(4);
	end

	-- Players are at war
	Players[0]:GetDiplomacy():DeclareWarOn(1);
	Players[0]:GetDiplomacy():DeclareWarOn(2);
	Players[0]:GetDiplomacy():DeclareWarOn(3);
	Players[0]:GetDiplomacy():DeclareWarOn(4);

	Players[0]:GetTechs():SetTech(GameInfo.Technologies["TECH_SAILING"].Index, true);
end

-- ===========================================================================
local function SetInitialVisibility()

	print ("SetInitialVisibility");

	-- Apply updates to all majors
	local aPlayers = PlayerManager.GetAliveMajors();
	for loop, pPlayer in ipairs(aPlayers) do
		if(pPlayer:IsHuman() == true) then
			local iPlayer = pPlayer:GetID();
	
			local pCurPlayerVisibility = PlayersVisibility[pPlayer:GetID()];
			if(pCurPlayerVisibility ~= nil) then
		
				-- Reveal Balkans and Turkey
				for iX = 0, 17, 1 do
					for iY = 25, 36, 1 do
						local iPlotIndex = Map.GetPlot(iX, iY):GetIndex();
						pCurPlayerVisibility:ChangeVisibilityCount(iPlotIndex, 1);
					end
				end

				-- Reveal Egypt
				for iX = 0, 14, 1 do
					for iY = 0, 20, 1 do
						-- Except for Western
						if ((iX > 5 or iY > 15) or (iX == 5 and iY == 13) or (iX == 5 and iY == 14) or (iX == 4 and iY == 15) or (iX == 5 and iY == 15)) then 
							local iPlotIndex = Map.GetPlot(iX, iY):GetIndex();
							pCurPlayerVisibility:ChangeVisibilityCount(iPlotIndex, 1);
						end
					end
				end

				-- Reveal Persia
				for iX = 14, 28, 1 do
					for iY = 14, 28, 1 do
						-- Except for Desert
						if (iY > 21) then 
							local iPlotIndex = Map.GetPlot(iX, iY):GetIndex();
							pCurPlayerVisibility:ChangeVisibilityCount(iPlotIndex, 1);
						elseif(iY > 20 and (iX > 20 or iX < 18)) then
							local iPlotIndex = Map.GetPlot(iX, iY):GetIndex();
							pCurPlayerVisibility:ChangeVisibilityCount(iPlotIndex, 1);
						elseif(iY > 19 and (iX > 22 or iX < 18)) then
							local iPlotIndex = Map.GetPlot(iX, iY):GetIndex();
							pCurPlayerVisibility:ChangeVisibilityCount(iPlotIndex, 1);
						elseif(iY > 18 and (iX > 23 or iX < 17)) then
							local iPlotIndex = Map.GetPlot(iX, iY):GetIndex();
							pCurPlayerVisibility:ChangeVisibilityCount(iPlotIndex, 1);
						elseif(iY > 17 and (iX > 24 or iX < 17)) then
							local iPlotIndex = Map.GetPlot(iX, iY):GetIndex();
							pCurPlayerVisibility:ChangeVisibilityCount(iPlotIndex, 1);
						elseif(iY > 16 and (iX > 24 or iX < 16)) then
							local iPlotIndex = Map.GetPlot(iX, iY):GetIndex();
							pCurPlayerVisibility:ChangeVisibilityCount(iPlotIndex, 1);
						elseif(iY > 15 and (iX > 25 or iX < 16)) then
							local iPlotIndex = Map.GetPlot(iX, iY):GetIndex();
							pCurPlayerVisibility:ChangeVisibilityCount(iPlotIndex, 1);
						elseif(iY > 14 and (iX > 25 or iX < 15)) then
							local iPlotIndex = Map.GetPlot(iX, iY):GetIndex();
							pCurPlayerVisibility:ChangeVisibilityCount(iPlotIndex, 1);
						elseif(iX > 26 or iX < 16) then
							local iPlotIndex = Map.GetPlot(iX, iY):GetIndex();
							pCurPlayerVisibility:ChangeVisibilityCount(iPlotIndex, 1);
						end
					end
				end

				for iX = 29, 34, 1 do
					for iY = 12, 25, 1 do
						local iPlotIndex = Map.GetPlot(iX, iY):GetIndex();
						pCurPlayerVisibility:ChangeVisibilityCount(iPlotIndex, 1);
					end
				end

				for iX = 30, 51, 1 do
					for iY = 9, 29, 1 do
						local iPlotIndex = Map.GetPlot(iX, iY):GetIndex();
						pCurPlayerVisibility:ChangeVisibilityCount(iPlotIndex, 1);
					end
				end

				-- Reveal Marakanda
				for iX = 47, 54, 1 do
					for iY = 30, 37, 1 do
						local iPlotIndex = Map.GetPlot(iX, iY):GetIndex();
						pCurPlayerVisibility:ChangeVisibilityCount(iPlotIndex, 1);
					end
				end

				-- Reveal Cyra
				for iX = 55, 58, 1 do
					for iY = 31, 35, 1 do
						local iPlotIndex = Map.GetPlot(iX, iY):GetIndex();
						pCurPlayerVisibility:ChangeVisibilityCount(iPlotIndex, 1);
					end
				end

				-- Reveal Eastern Persia and Patala
				for iX = 52, 65, 1 do
					for iY = 10, 26, 1 do
						local iPlotIndex = Map.GetPlot(iX, iY):GetIndex();
						pCurPlayerVisibility:ChangeVisibilityCount(iPlotIndex, 1);
					end
				end

				for iX = 52, 59, 1 do
					for iY = 26, 30, 1 do
						local iPlotIndex = Map.GetPlot(iX, iY):GetIndex();
						pCurPlayerVisibility:ChangeVisibilityCount(iPlotIndex, 1);
					end
				end
			end
		end
	end
end

-- ===========================================================================
local function OnPlayerTurnActivated( player )
	local currentTurn = Game.GetCurrentGameTurn();
	
	if (player == 0 and currentTurn == 1) then
		SetInitialVisibility();

		local individual = GameInfo.GreatPersonIndividuals["GREAT_PERSON_INDIVIDUAL_ALEXANDER"].Hash;
		local class = GameInfo.GreatPersonClasses["GREAT_PERSON_CLASS_GENERAL"].Hash;
		local era = GameInfo.Eras["ERA_CLASSICAL"].Hash;
		local cost = 0;
		Game.GetGreatPeople():GrantPerson(individual, class, era, cost, 0, false);
	end
end

-- ===========================================================================
function OnCityConquered(capturerID,  ownerID, cityID , cityX, cityY)
	if(capturerID == 0) then
		local bIsValid = false;

		if(Map.GetPlot(cityX, cityY):GetUnitCount() > 0) then
			bIsValid = SpecificGreatPerson(cityX, cityY);		
		end
		
		if(bIsValid == false) then
			for direction = 0, DirectionTypes.NUM_DIRECTION_TYPES - 1, 1 do
				local adjacentPlot = Map.GetAdjacentPlot(cityX, cityY, direction);
				if (adjacentPlot ~= nil and bIsValid ~= true) then
					bIsValid = SpecificGreatPerson(adjacentPlot:GetX(), adjacentPlot:GetY());
				end
			end
		end

		if(bIsValid == true) then
			if(cityX == 1 and cityY == 29) then
				UnitManager.InitUnitValidAdjacentHex(capturerID, "UNIT_GREEK_HOPLITE", cityX, cityY, 1);
				UnitManager.InitUnitValidAdjacentHex(capturerID, "UNIT_GREEK_HOPLITE", cityX, cityY, 1);
			elseif(cityX == 38 and cityY == 14) then
				UnitManager.InitUnitValidAdjacentHex(capturerID, "UNIT_PERSIAN_IMMORTAL", cityX, cityY, 1);
				UnitManager.InitUnitValidAdjacentHex(capturerID, "UNIT_PERSIAN_IMMORTAL", cityX, cityY, 1);
			elseif(cityX == 8 and cityY == 15) then
				UnitManager.InitUnitValidAdjacentHex(capturerID, "UNIT_EGYPTIAN_CHARIOT_ARCHER", cityX, cityY, 1);
			elseif(cityX == 61 and cityY == 16) then
				UnitManager.InitUnitValidAdjacentHex(capturerID, "UNIT_INDIAN_VARU", cityX, cityY, 1);
			elseif(cityX == 52 and cityY == 35) then
				UnitManager.InitUnitValidAdjacentHex(capturerID, "UNIT_SCYTHIAN_HORSE_ARCHER", cityX, cityY, 1);
			elseif(cityX == 9 and cityY == 26) then
				UnitManager.InitUnitValidAdjacentHex(capturerID, "UNIT_GALLEY", cityX, cityY, 1);
			elseif(cityX == 27 and cityY == 18) then
				UnitManager.InitUnitValidAdjacentHex(capturerID, "UNIT_ARCHER", cityX, cityY, 1);
			end
		end
	end
end

-- ===========================================================================
function SpecificGreatPerson( iX, iY )
	pPlot = Map.GetPlot(iX, iY)
	if(pPlot:GetUnitCount() > 0) then
		--print("Units2: ", pPlot:GetUnitCount());
		for loop, unit in ipairs(Units.GetUnitsInPlot(pPlot)) do
			--print("Unit");
			if(unit ~= nil) then
				if(unit:IsGreatPerson() == true) then
					local greatperson = unit:GetGreatPerson():GetIndividualHash();
					--print("Individual: ", greatperson);
					if(GameInfo.GreatPersonIndividuals["GREAT_PERSON_INDIVIDUAL_ALEXANDER"].Hash == greatperson) then
						return true;
					end
				end
			end
		end
	end
	
	return false;
end

-- ===========================================================================
local function OnUnitRetreated(UnitOwner, unitID)
	local player = Players[UnitOwner];
	if (player ~= nil) then
		local unit = player:GetUnits():FindID(unitID);
		UnitManager.Kill(unit,false);
	end
end
-- ===========================================================================

Initialize();
GameEvents.OnGameTurnStarted.Add(OnGameTurnStarted);
GameEvents.PlayerTurnStarted.Add(OnPlayerTurnActivated);
GameEvents.CityConquered.Add(OnCityConquered);
GameEvents.OnUnitRetreated.Add(OnUnitRetreated);
LuaEvents.NewGameInitialized.Add(InitializeNewGame);
