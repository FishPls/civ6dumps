-- ===========================================================================
--	Logitech ARX Support for Alexander Scenario
-- ===========================================================================
include("TabSupport");
include("InstanceManager");
include("SupportFunctions");
include("AnimSidePanelSupport");
include("TeamSupport");

-- ===========================================================================
--	CONSTANTS
-- ===========================================================================

-- ===========================================================================
--	VARIABLES
-- ===========================================================================
local m_ScreenMode:number = 0;
local m_LocalPlayer:table;
local m_LocalPlayerID:number;
local fullStr:string = "";
local m_kEras:table	= {};
local m_bIsPortrait:boolean = false;

-- ===========================================================================
-- Draw the Top 5 Civs screen (Overall in Alexander Scenario)
-- ===========================================================================
function DrawTop5()
    -- header with civ name
    local playerConfig:table = PlayerConfigurations[m_LocalPlayerID];
    local name = Locale.Lookup(playerConfig:GetPlayerName());
    local imgname = Locale.Lookup(playerConfig:GetLeaderTypeName());

	fullStr = fullStr.."<p><span class=title><img src='Civ_"..imgname..".png' align=left>"..name.."<br/><br/>";

	local iCities = 0;
	local iOwnerCities = 0;
	local pPlayers:table = PlayerManager.GetAlive();
	for _, player in ipairs(pPlayers) do
		if (player ~= nil) then
			local playerCities:table = player:GetCities();
			for _, city in playerCities:Members() do
				if(city ~= nil) then
					if(player:GetID() > 0) then
						iCities = iCities + 1;
					else
						iOwnerCities = iOwnerCities + 1;
					end
				end
			end
		end
	end
  
    -- todo: fix up the rest of the rules strings the World Ranking shows to be HTML compatible.
	fullStr = fullStr .. "<p><span class=title>";
	fullStr = fullStr .. Locale.Lookup("LOC_ALEXANDER_SCENARIO_WORLD_RANKING_1"); 
	fullStr = fullStr .. "<br/>";
	fullStr = fullStr .. Locale.Lookup("LOC_ALEXANDER_SCENARIO_WORLD_RANKING_2"); 
	fullStr = fullStr .. "<br/><br/>";
	fullStr = fullStr .. Locale.Lookup("LOC_ALEXANDER_SCENARIO_WORLD_RANKING_6", iCities); 
	fullStr = fullStr .. "<br/>";
	fullStr = fullStr .. Locale.Lookup("LOC_ALEXANDER_SCENARIO_WORLD_RANKING_8", iOwnerCities * 5); 
	fullStr = fullStr .. "</span></p>";
                  
    UI.SetARXTagContentByID("Content", fullStr);
end                              

-- ===========================================================================
-- Clear ARX if exiting to main menu
-- ===========================================================================
function OnExitToMain()
    UI.SetARXTagContentByID("top5", " ");
    UI.SetARXTagContentByID("victory", " ");
    UI.SetARXTagContentByID("gossip", " ");
    UI.SetARXTagContentByID("Content", " ");
end

-- ===========================================================================
-- Refresh the ARX screen
-- ===========================================================================
function RefreshARX()
    if UI.HasARX() then
        local strDate = Calendar.MakeYearStr(Game.GetCurrentGameTurn());

        m_bIsPortrait = UI.IsARXDisplayPortrait();
		
        if (Game.GetLocalPlayer() ~= -1) then
            m_LocalPlayer = Players[Game.GetLocalPlayer()];
            m_LocalPlayerID = m_LocalPlayer:GetID();
        end

        -- fill in button texts (generating full button HTML here fails for an unknown reason, may have to use JavaScript to be fully dynamic)
        UI.SetARXTagContentByID("top5", "<span class=content>"..Locale.Lookup("LOC_WORLD_RANKINGS_OVERALL_TAB").."</span>");

        -- make buttons visible
        UI.SetARXTagsPropertyByClass("button", "style.visibility", "visible");

        -- header with civ name
		local playerName;
		if(m_LocalPlayerID and PlayerConfigurations[m_LocalPlayerID]) then
            local bOnTeam = false;
            local team = Teams[m_LocalPlayer:GetTeam()];
            if(team ~= nil and #team ~= 1) then
                bOnTeam = true;
            end

            if bOnTeam then
                playerName = Locale.Lookup(PlayerConfigurations[m_LocalPlayerID]:GetPlayerName())..", "..Locale.Lookup("LOC_WORLD_RANKINGS_TEAM", m_LocalPlayer:GetTeam());
            else
                playerName = Locale.Lookup(PlayerConfigurations[m_LocalPlayerID]:GetPlayerName());
            end
		else
			playerName = Locale.Lookup("LOC_MULTIPLAYER_UNKNOWN");
		end

        fullStr = "<span class=title>".. playerName .."</span>";
        -- and turn and date
		fullStr = fullStr.."<br><span class=content>" .. Locale.Lookup("LOC_TOP_PANEL_CURRENT_TURN").." "..tostring(Game.GetCurrentGameTurn());

        if m_bIsPortrait then
            fullStr = fullStr..", "..strDate;
        else
            fullStr = fullStr..", "..strDate;
        end
                       
		local eraName;
		if(m_LocalPlayer) then
			local eraIndex:number = m_LocalPlayer:GetEra() + 1;
			for _,era in pairs(m_kEras) do
				if era.Index == eraIndex then
					eraName = Locale.Lookup("LOC_GAME_ERA_DESC", era.Description );
					break;
				end		
			end
		else
			eraName = Locale.Lookup("LOC_MULTIPLAYER_UNKNOWN");
		end    

		fullStr = fullStr..", ".. eraName .."</span>";

        DrawTop5();
    end
end

-- ===========================================================================
-- Handle turn change
-- ===========================================================================
function OnTurnBegin()
    RefreshARX();
end

-- ===========================================================================
-- Handle ARX taps
-- ===========================================================================
function OnARXTap(szButtonID:string)
    -- no taps for Alexander scenario
end

-- ===========================================================================
--	Reset the hooks that are visible for hotseat
-- ===========================================================================
function OnLocalPlayerChanged()
	RefreshARX();
end

-- ===========================================================================
--	Update our scores when a city is captured
-- ===========================================================================
function OnCityOccupationChanged(player, cityID)
    RefreshARX();
end

-- ===========================================================================
function Initialize()

	Events.LocalPlayerChanged.Add( OnLocalPlayerChanged );
	Events.TurnBegin.Add( OnTurnBegin );
    Events.ARXTap.Add( OnARXTap );
    Events.ARXOrientationChanged.Add( RefreshARX );
    Events.ExitToMainMenu.Add( OnExitToMain );
	Events.CityOccupationChanged.Add( OnCityOccupationChanged );	

    -- build era table
	m_kEras = {};
	for row:table in GameInfo.Eras() do
		m_kEras[row.EraType] = { 
			Description	= Locale.Lookup(row.Name),
			Index		= row.ChronologyIndex,
		}
	end	

    OnTurnBegin();
end
Initialize();   
