include "SupportFunctions.lua"

-- ===========================================================================
--	Australia Scenario
-- ===========================================================================

-- Scenario constants
local eSydney = 4;
local g_iFirstColonistTurn = 5;
local g_iFederationMinTurn = 35;
local g_iWorldWarITurn = 45;
local g_iWorldWarIITurn = 53;
local g_iFinalTurn = 60;

-- Event global data
local g_aEventTable1:table = {};
local g_aEventTable2:table = {};
local g_aEventTable3:table = {};
local g_iNumPoliticalEventsTriggered = 0;
local g_iNumBenefitEventsTriggered = 0;
local g_iNumDangerEventsTriggered = 0;
local g_iNumDelayEventsTriggered = 0;
local g_iNumPoliticalEventsInDB = 5;
local g_iNumBenefitEventsInDB = 20;
local g_iNumDangerEventsInDB = 32;
local g_iNumDelayEventsInDB = 15;
local g_iRollsPerPlayer = 3;
local g_iNumPlayers = 4;
local g_aRandomRolls:table = {};
local g_aTurnEventType:table = {};
local g_aTurnEventNumber:table = {};
local g_aTurnEventEffect:table = {};
local g_aPossibleEventUnits:table = {};
local g_aPossibleEventIndices:table = {};
local g_aPossibleEventAssignedUnit:table = {};
local g_aDelayedEventUnit:table = {}
local g_aDelayedEventEffect:table = {}

-- AI start area data
local westernAustraliaSWCorner = {0, 0};
local westernAustraliaNECorner = {21, 43};
local southAustraliaSWCorner = {22, 0};
local southAustraliaNECorner = {38, 43};
local victoriaSWCorner = {39, 0};
local victoriaNECorner = {57, 14};
local queenslandSWCorner = {39, 14};
local queenslandNECorner = {57, 43};
local westernAustraliaStartHexes = {
	{6, 14},
	{7, 12},
	{8, 10},
	{8, 8},
	{11, 8},
	{13, 10},
	{15, 10},
	{17, 10},
	{18, 11},
	{20, 12}};
local southAustraliaStartHexes = {
	{22, 12},
	{23, 13},
	{25, 13},
	{27, 13},
	{29, 12},
	{30, 11},
	{32, 10},
	{33, 11},
	{35, 10},
	{36, 8}};
local victoriaStartHexes = {
	{39, 4},
	{41, 4},
	{43, 4},
	{45, 4},
	{45, 5},
	{46, 5},
	{47, 5},
	{48, 6},
	{48, 7},
	{49, 8}};
local queenslandStartHexes = {
	{53, 15},
	{53, 17},
	{53, 19},
	{53, 21},
	{52, 22},
	{51, 23},
	{50, 24},
	{49, 26},
	{47, 28},
	{45, 29}};

-- ===========================================================================
-- Utitity function: find closest coastal tile
-- ===========================================================================
function FindClosestCoast(iStartX, iStartY)

	local iClosest = 10000;
	local pClosestPlot = nil;

	g_iW, g_iH = Map.GetGridSize();

	-- Reveal oceans and coasts
	for iX = 0, g_iW - 1 do
		for iY = 0, g_iH - 1 do
			local iPlotIndex = iY * g_iW + iX;
			local pPlot = Map.GetPlotByIndex(iPlotIndex);
			if (pPlot:IsWater() and not pPlot:IsLake()) then
				local iDistance = Map.GetPlotDistance(iStartX, iStartY, iX, iY);
				if (iDistance < iClosest) then
					pClosestPlot = pPlot;
					iClosest = iDistance;
				end
			end
		end
	end

	return pClosestPlot;
end

-- ===========================================================================
-- Utitity function: find closest city of this player
-- ===========================================================================
function FindClosestCity(player, iStartX, iStartY)

    local pCity = nullptr;
    local iShortestDistance = 10000;
	local pPlayer = Players[player];
   
	local pPlayerCities:table = pPlayer:GetCities();
	for i, pLoopCity in pPlayerCities:Members() do
		local iDistance = Map.GetPlotDistance(iStartX, iStartY, pLoopCity:GetX(), pLoopCity:GetY());
		if (iDistance < iShortestDistance) then
			pCity = pLoopCity;
			iShortestDistance = iDistance;
		end
	end

	if (pCity == nullptr) then
		print ("No closest city found of player " .. tostring(player) .. " from " .. tostring(iStartX) .. ", " .. tostring(iStartX));
	end
   
    return pCity;
end

-- ===========================================================================
-- Utitity function: find closest city of this player
-- ===========================================================================
function FindClosestImprovement(player, iStartX, iStartY)

    local pBestPlot = nullptr;
    local iShortestDistance = 10000;
   
	g_iW, g_iH = Map.GetGridSize();

	for iX = 0, g_iW - 1 do
		for iY = 0, g_iH - 1 do
			local iPlotIndex = iY * g_iW + iX;
			local pPlot = Map.GetPlotByIndex(iPlotIndex);
			if (pPlot:GetOwner() == player and pPlot:GetImprovementType() ~= -1) then
				local iDistance = Map.GetPlotDistance(iStartX, iStartY, pPlot:GetX(), pPlot:GetY());
				if (iDistance < iShortestDistance) then
					pBestPlot = pPlot;
					iShortestDistance = iDistance;
				end
			end
		end
	end

	if (pCity == nullptr) then
		print ("No closest city found of player " .. tostring(player) .. " from " .. tostring(iStartX) .. ", " .. tostring(iStartX));
	end
   
    return pBestPlot;
end

-- ===========================================================================
-- Utitity function: find closest city of this player
-- ===========================================================================
function AnyCityWithinXTiles(iTargetDist, iX, iY)

	local aPlayers = PlayerManager.GetAliveMajors();
	for loop, pPlayer in ipairs(aPlayers) do
		local pPlayerCities:table = pPlayer:GetCities();
		for i, pLoopCity in pPlayerCities:Members() do
			local iDistance = Map.GetPlotDistance(iX, iY, pLoopCity:GetX(), pLoopCity:GetY());
			if (iDistance <= iTargetDist) then
				return true;
			end
		end
	end
	return false;
end

-- ===========================================================================
--	Bit Helper function
--	Converts a number into a table of bits.  Local version pads out arrays with
--  0s so 32 entries guaranteed
-- ===========================================================================
function LocalNumberToBitsTable( value:number )

	local aTable:table = {};
	aTable = numberToBitsTable(value);
	if (#aTable < 32) then
		for iIndex = #aTable + 1, 32 do
			table.insert(aTable, 0);
		end
	end

	return aTable;
end
-- ===========================================================================
-- Event enums

local eEventTypeNone = -1;
local eEventTypePolitical = 0;
local eEventTypeBenefits = 1;
local eEventTypeDangers = 2;
local eEventTypeDelays = 3;

-- ===========================================================================
-- SCENARIO EVENT FUNCTIONS
-- ===========================================================================
--
-- Event Utility Functions to Load/Save event status
--
local function InitializeEventData (playerID)

	print ("InitializeEventData, playerID = " .. tostring(playerID));
	local pPlayer = Players[playerID];
	pPlayer:ChangeScoringScenario1(0);
	pPlayer:ChangeScoringScenario2(0);
	pPlayer:ChangeScoringScenario3(0);
end

-- ===========================================================================
local function RetrieveEventData (playerID)
	local pPlayer = Players[playerID];

	g_aEventTable1 = LocalNumberToBitsTable(pPlayer:GetScoringScenario1());
	g_aEventTable2 = LocalNumberToBitsTable(pPlayer:GetScoringScenario2());
	g_aEventTable3 = LocalNumberToBitsTable(pPlayer:GetScoringScenario3());

	g_iNumPoliticalEventsTriggered = 0;
	g_iNumBenefitEventsTriggered = 0;
	g_iNumDangerEventsTriggered = 0;
	g_iNumDelayEventsTriggered = 0;

	for iIndex = 1, g_iNumPoliticalEventsInDB do
		if (g_aEventTable1[iIndex] ~= nil) then
			g_iNumPoliticalEventsTriggered = g_iNumPoliticalEventsTriggered + g_aEventTable1[iIndex];
		else
			print ("Entry not found, Event Table 1, " .. tostring(iIndex));
		end
	end
	for iIndex = g_iNumPoliticalEventsInDB + 1, g_iNumPoliticalEventsInDB + g_iNumBenefitEventsInDB do
		if (g_aEventTable1[iIndex] ~= nil) then
			g_iNumBenefitEventsTriggered = g_iNumBenefitEventsTriggered + g_aEventTable1[iIndex];
		else
			print ("Entry not found, Event Table 1, " .. tostring(iIndex));
		end
	end
	for iIndex = 1, g_iNumDangerEventsInDB do
		if (g_aEventTable2[iIndex] ~= nil) then
			g_iNumDangerEventsTriggered = g_iNumDangerEventsTriggered + g_aEventTable2[iIndex];
		else
			print ("Entry not found, Event Table 2, " .. tostring(iIndex));
		end
	end
	for iIndex = 1, g_iNumDelayEventsInDB do
		if (g_aEventTable3[iIndex] ~= nil) then
			g_iNumDelayEventsTriggered = g_iNumDelayEventsTriggered + g_aEventTable3[iIndex];
		else
			print ("Entry not found, Event Table 3, " .. tostring(iIndex));
		end
	end

	print ("g_iNumPoliticalEventsTriggered: " .. tostring(g_iNumPoliticalEventsTriggered));
	print ("g_iNumBenefitEventsTriggered:   " .. tostring(g_iNumBenefitEventsTriggered));
	print ("g_iNumDangerEventsTriggered:    " .. tostring(g_iNumDangerEventsTriggered));
	print ("g_iNumDelayEventsTriggered:     " .. tostring(g_iNumDelayEventsTriggered));
end

-- ===========================================================================
local function SaveEventData (playerID)

	local pPlayer = Players[playerID];
	local iTemp;
	iTemp = bitsTableToNumber(g_aEventTable1) - pPlayer:GetScoringScenario1();
	pPlayer:ChangeScoringScenario1(iTemp);
	iTemp = bitsTableToNumber(g_aEventTable2) - pPlayer:GetScoringScenario2();
	pPlayer:ChangeScoringScenario2(iTemp);
	iTemp = bitsTableToNumber(g_aEventTable3) - pPlayer:GetScoringScenario3();
	pPlayer:ChangeScoringScenario3(iTemp);
end

-- ===========================================================================
local function HasEventTriggered ( eEventType, eEventNumber )

	if (eEventType == eEventTypePolitical) then
		return g_aEventTable1[eEventNumber];
	elseif (eEventType == eEventTypeBenefits) then
		return g_aEventTable1[eEventNumber + g_iNumPoliticalEventsInDB];
	elseif (eEventType == eEventTypeDangers) then
		return g_aEventTable2[eEventNumber];
	elseif (eEventType == eEventTypeDelays) then
		return g_aEventTable3[eEventNumber];
	end
	return false;
end

-- ===========================================================================
local function SetEventTriggered ( eEventType, eEventNumber )

	if (eEventType == eEventTypePolitical) then
		g_aEventTable1[eEventNumber] = 1;
	elseif (eEventType == eEventTypeBenefits) then
		g_aEventTable1[eEventNumber + g_iNumPoliticalEventsInDB] = 1;
	elseif (eEventType == eEventTypeDangers) then
		g_aEventTable2[eEventNumber] = 1;
	elseif (eEventType == eEventTypeDelays) then
		g_aEventTable3[eEventNumber] = 1;
	end
end

-- ===========================================================================
-- Event Function to convert an index in the GameInfo EventPopupData table
-- to our event type and number used in the event status data above
-- ===========================================================================
local function RowToTypeNumber ( iEventIndex )
	
	local eEventType = eEventTypeNone;
	local eEventNumber = -1;

	-- Benefit events are first
	if ( iEventIndex < g_iNumBenefitEventsInDB) then
		eEventType = eEventTypeBenefits;
		eEventNumber = iEventIndex + 1;

	-- Then dangers
	elseif ( iEventIndex < g_iNumBenefitEventsInDB + g_iNumDangerEventsInDB) then
		eEventType = eEventTypeDangers;
		eEventNumber = iEventIndex + 1 - g_iNumBenefitEventsInDB;

	-- Then delays
	elseif ( iEventIndex < g_iNumBenefitEventsInDB + g_iNumDangerEventsInDB + g_iNumDelayEventsInDB) then
		eEventType = eEventTypeDelays;
		eEventNumber = iEventIndex + 1 - (g_iNumBenefitEventsInDB + g_iNumDangerEventsInDB);

	-- Finally political
	else
		eEventType = eEventTypePolitical;
		eEventNumber = iEventIndex + 1 - (g_iNumBenefitEventsInDB + g_iNumDangerEventsInDB + g_iNumDelayEventsInDB);
	end

	return eEventType, eEventNumber;
end

-- ===========================================================================
local function IsLocationValid (iEventIndex, pPlot)

	local filterCondition = GameInfo.EventPopupData[iEventIndex].FilterCondition;
	if (filterCondition == "ANY") then
		return true;

	elseif (filterCondition == "COASTAL_LAND") then
		if (pPlot:IsCoastalLand()) then
			return true;
		end
	end

	local eTerrainType = pPlot:GetTerrainType();
	local terrainName = GameInfo.Terrains[eTerrainType].TerrainType;

	if (filterCondition == "DESERT") then
		if (terrainName == "TERRAIN_DESERT" or terrainName == "TERRAIN_DESERT_HILLS") then
			return true;
		end
	elseif (filterCondition == "GRASSLAND") then
		if (terrainName == "TERRAIN_GRASS" or terrainName == "TERRAIN_GRASS_HILLS") then
			return true;
		end
	elseif (filterCondition == "PLAINS") then
		if (terrainName == "TERRAIN_PLAINS" or terrainName == "TERRAIN_PLAINS_HILLS") then
			return true;
		end
	end

	local eFeatureType = pPlot:GetFeatureType();
	if (eFeatureType ~= -1) then
		local featureName = GameInfo.Features[eFeatureType].FeatureType;

		if (filterCondition == "FLOODPLAINS") then
			if (featureName == "FEATURE_FLOODPLAINS") then
				return true;
			end
		elseif (filterCondition == "MARSH") then
			if (featureName == "FEATURE_MARSH") then
				return true;
			end
		elseif (filterCondition == "OASIS") then
			if (featureName == "FEATURE_OASIS") then
				return true;
			end
		elseif (filterCondition == "RAINFOREST") then
			if (featureName == "FEATURE_RAINFOREST") then
				return true;
			end
		elseif (filterCondition == "WOODS") then
			if (featureName == "FEATURE_WOODS") then
				return true;
			end
		end
	end

	if (filterCondition == "MOUNTAIN_ADJACENT") then
		for direction = 0, DirectionTypes.NUM_DIRECTION_TYPES - 1, 1 do
			local adjacentPlot = Map.GetAdjacentPlot(pPlot:GetX(), pPlot:GetY(), direction);
			if (adjacentPlot) then
				eTerrainType = pPlot:GetTerrainType();
				terrainName = GameInfo.Terrains[eTerrainType].TerrainType;
				if (terrainName == "TERRAIN_DESERT_MOUNTAIN" or
					terrainName == "TERRAIN_PLAINS_MOUNTAIN" or
					terrainName == "TERRAIN_GRASS_MOUNTAIN") then
					return true;
				end
			end
		end
	end

	return false;
end

-- ===========================================================================
local function IsEffectValid (iEventIndex, pPlot, iPlayer, pUnit)

	local effectType = GameInfo.EventPopupData[iEventIndex].EffectType;

	-- Only a few effects are situational
	if (effectType == "GAIN_MAP") then

		-- Make sure there are at least five unrevealed tiles within +/- 5 x, y coords from here
		local iUnrevealedCount = 0;
		local localPlayerVis:table = PlayersVisibility[iPlayer];
		for iX = pPlot:GetX() - 5, pPlot:GetX() + 5 do
			for iY = pPlot:GetY() - 5, pPlot:GetY() + 5 do
				local testPlot = Map.GetPlot(iX, iY);
				if (testPlot ~= nil) then
					if not localPlayerVis:IsRevealed(testPlot:GetX(), testPlot:GetY()) then
						iUnrevealedCount = iUnrevealedCount + 1;
					end

					if (iUnrevealedCount >= 5) then
						return true;
					end
			end
			end
		end
	elseif (effectType == "OASIS_MAP") then

		-- Make sure there is at least one unrevealed oasis within +/- 5 x, y coords from here
		local localPlayerVis:table = PlayersVisibility[iPlayer];
		for iX = pPlot:GetX() - 5, pPlot:GetX() + 5 do
			for iY = pPlot:GetY() - 5, pPlot:GetY() + 5 do
				local testPlot = Map.GetPlot(iX, iY);
				if (testPlot ~= nil) then
					if not localPlayerVis:IsRevealed(testPlot:GetX(), testPlot:GetY()) then
						local eFeatureType = testPlot:GetFeatureType();
						if (eFeatureType ~= -1) then
							local featureName = GameInfo.Features[eFeatureType].FeatureType;
							if (featureName == "FEATURE_OASIS") then
								return true;
							end
						end
					end
				end
			end
		end
	
	elseif (effectType == "PILLAGE") then
		local pPlot = FindClosestImprovement(iPlayer, pPlot:GetX(), pPlot:GetY());
		if (pPlot ~= nil) then
			return true;
		end

	elseif (effectType == "LOSE_MOVEMENT") then
		if (pUnit:GetMaxMoves() > 1) then
			return true;
		end

	elseif (effectType == "RETURN_CITY") then
		local pCity = FindClosestCity(iPlayer, pPlot:GetX(), pPlot:GetY());
		if (pCity ~= nil) then
			return true;
		end

	-- All other effect types are fine
	else
		return true;
	end

	return false;
end

-- ===========================================================================
-- Event Functions to cache an event generated at GAME turn start to be
-- displayed at PLAYER turn start
-- ===========================================================================
local function CachePlayerEvent (player, eventType, eventNumber, effectString)

	g_aTurnEventType[player + 1] = eventType;
	g_aTurnEventNumber[player + 1] = eventNumber;
	g_aTurnEventEffect[player + 1] = effectString;

	if (eventType ~= eEventTypeNone) then
		SetEventTriggered(eventType, eventNumber);
	end
end

-- ===========================================================================
-- Event Functions to check and see what is going to occur this turn
-- ===========================================================================
local function GenerateDieRolls ()
	
	for iIndex = 1, g_iRollsPerPlayer * g_iNumPlayers do
		g_aRandomRolls[iIndex] = Game.GetRandNum(100, "Australia Scenario Event Roll");
	end
end

-- ===========================================================================
local function CheckForPoliticalEvent( player, currentTurn )

	-- Most political events come at a specific historical time
	if (currentTurn == 1) then
		CachePlayerEvent(player, eEventTypePolitical, 1, "");
		return true;

	elseif (currentTurn == g_iFirstColonistTurn - 1) then
		CachePlayerEvent(player, eEventTypePolitical, 2, "");
		return true;

	elseif (currentTurn == g_iWorldWarITurn) then
		CachePlayerEvent(player, eEventTypePolitical, 4, "");
		return true;

	elseif (currentTurn == g_iWorldWarIITurn) then
		CachePlayerEvent(player, eEventTypePolitical, 5, "");
		return true;

	end

	-- Federation event has more complex requirements
	local pCulture:table = Players[player]:GetCulture();
	local eFederationCivicType = GameInfo.Civics["CIVIC_FEDERATION"].Index;
	if (not pCulture:HasCivic(eFederationCivicType) and HasEventTriggered(eEventTypePolitical, 3) == 0) then
		if (currentTurn == g_iFederationMinTurn or pCulture:HasBoostBeenTriggered(eFederationCivicType)) then
			CachePlayerEvent(player, eEventTypePolitical, 3, "");
			return true;
		end
	end

	return false;
end

-- ===========================================================================
local function FindPossibleEventUnits( player, currentTurn )

	g_aPossibleEventUnits = {};
		
	local pPlayer = Players[player];
	local playerUnits = pPlayer:GetUnits();
	for i, unit in playerUnits:Members() do
		local unitInfo = GameInfo.Units[unit:GetType()];
		if (unitInfo) then
			local unitTypeName = unitInfo.UnitType;
			-- Never affect a Trader
			if unitTypeName ~= "UNIT_TRADER" then
				-- Don't affect a Settler until the player has 1 city
				if unitTypeName ~= "UNIT_SETTLER" or pPlayer:GetCities():GetCount() > 0 then
					local pPlot = Map.GetPlot(unit:GetX(), unit:GetY());
					local eOwner = pPlot:GetOwner();
					if (not pPlot:IsWater() and eOwner == -1) then
						table.insert(g_aPossibleEventUnits, unit);
					end
				end
			end
		end
	end
end

-- ===========================================================================
local function FindPossibleEvents( player, currentTurn )

	g_aPossibleEventIndices = {};
	g_aPossibleEventAssignedUnit = {};
	local evtType;
	local evtNum;
		
	local pPlayer = Players[player];
	for iIndex = 1, #g_aPossibleEventUnits do 
		local unit = g_aPossibleEventUnits[iIndex];
		local pPlot = Map.GetPlot(unit:GetX(), unit:GetY());
		local unitTypeName = UnitManager.GetTypeName(unit);
		print (unitTypeName);
		local iNumPossibleEvents = 0;

		for row in GameInfo.EventPopupData() do
		   evtIndex = row.Index;
		   evtType, evtNum = RowToTypeNumber(evtIndex);

		   -- This event already used?
		   if HasEventTriggered(evtType, evtNum) == 0 and evtType ~= eEventTypePolitical then
				
				if IsLocationValid(evtIndex, pPlot) and IsEffectValid(evtIndex, pPlot, player, unit) then
					table.insert (g_aPossibleEventIndices, evtIndex);
					table.insert (g_aPossibleEventAssignedUnit, unit);
					iNumPossibleEvents = iNumPossibleEvents + 1;
				end
		   end
		end

		print ("iNumPossibleEvents: " .. tostring(iNumPossibleEvents));
	end
end

-- ===========================================================================
local function SelectEvent(eEventType, player)

	-- Returns -1 if none of this type available, otherwise returns index in possible events table
	local iCountPossible = 0;
	for iPossibleEvent = 1, #g_aPossibleEventIndices do
		local iEventIndex = g_aPossibleEventIndices[iPossibleEvent];
		evtType, evtNum = RowToTypeNumber(iEventIndex);
		if (eEventType == evtType) then
			iCountPossible = iCountPossible + 1;
		end
	end

	if (iCountPossible > 0) then
		local iRoll = g_aRandomRolls[2 + (player * g_iRollsPerPlayer)];
		local iSelection = math.floor(iCountPossible * iRoll / 100) + 1;

		local iCountFound = 0;
		for iPossibleEvent = 1, #g_aPossibleEventIndices do
			local iEventIndex = g_aPossibleEventIndices[iPossibleEvent];
			local evtType, evtNum = RowToTypeNumber(iEventIndex);
			if (eEventType == evtType) then
				iCountFound = iCountFound + 1;
				if (iCountFound == iSelection) then
					return iPossibleEvent;
				end
			end
		end
	end

	return -1;
end

-- ===========================================================================
local function TriggerGameplayEffect(iPossibleEvent, player)

	local iEventIndex = g_aPossibleEventIndices[iPossibleEvent];
	local effectType = GameInfo.EventPopupData[iEventIndex].EffectType;
	local pUnit = g_aPossibleEventAssignedUnit[iPossibleEvent];
	local iRoll = g_aRandomRolls[3 + (player * g_iRollsPerPlayer)];
	local iCurrentTurn = Game.GetCurrentGameTurn();
	local szEffectText = "";
	local pPlayer = Players[player];
	local iMaxDamage = pUnit:GetMaxDamage();
	local bHasCartography = pPlayer:GetTechs():HasTech(GameInfo.Technologies["TECH_CARTOGRAPHY"].Index);

	g_aDelayedEventUnit[player + 1] = -1
	g_aDelayedEventEffect[player + 1] = "";

    ReportingEvents.Send("EVENT_SOUND_REQUEST", "UI_Outback_Notification", player);

	-- Returns effect string to display
	if (effectType == "GAIN_MAP") then
		local iRange = 4;
		if (bHasCartography == true) then
			iRange = iRange + 1;
		end
		local localPlayerVis:table = PlayersVisibility[player];
		for iX = pUnit:GetX() - iRange, pUnit:GetX() + iRange do
			for iY = pUnit:GetY() - iRange, pUnit:GetY() + iRange do
				local revealPlot = Map.GetPlot(iX, iY);
				if (revealPlot ~= nil) then
					localPlayerVis:ChangeVisibilityCount(revealPlot:GetIndex(), 1);
				end
			end
		end		
		szEffectText = Locale.Lookup("LOC_SCENARIO_AUSTRALIA_EVENT_BENEFIT_EFFECT_3", pUnit:GetName());

	elseif (effectType == "OASIS_MAP") then
		local iRange = 4;
		if (bHasCartography == true) then
			iRange = iRange + 1;
		end
		local localPlayerVis:table = PlayersVisibility[player];
		for iX = pUnit:GetX() - iRange, pUnit:GetX() + iRange do
			for iY = pUnit:GetY() - iRange, pUnit:GetY() + iRange do
				local testPlot = Map.GetPlot(iX, iY);
				if (testPlot ~= nil) then
					if not localPlayerVis:IsRevealed(testPlot:GetX(), testPlot:GetY()) then
						local eFeatureType = testPlot:GetFeatureType();
						if (eFeatureType ~= -1) then
							local featureName = GameInfo.Features[eFeatureType].FeatureType;
							if (featureName == "FEATURE_OASIS") then
								localPlayerVis:ChangeVisibilityCount(testPlot:GetIndex(), 1);
								-- Also reveal the 6 plots around the oasis
								for direction = 0, DirectionTypes.NUM_DIRECTION_TYPES - 1, 1 do
									local adjacentPlot = Map.GetAdjacentPlot(testPlot:GetX(), testPlot:GetY(), direction);
									if (adjacentPlot) then
										localPlayerVis:ChangeVisibilityCount(adjacentPlot:GetIndex(), 1);
									end
								end
								-- TESTED?
								return Locale.Lookup("LOC_SCENARIO_AUSTRALIA_EVENT_BENEFIT_EFFECT_3", pUnit:GetName());
							end
						end
					end
				end
			end
		end

	elseif (effectType == "UNIT_FISHER" or effectType == "UNIT_FARMER" or effectType == "UNIT_PROSPECTOR" or effectType == "UNIT_GRAZIER" or effectType == "UNIT_EXPLORER") then
		local pCity = FindClosestCity(player, pUnit:GetX(), pUnit:GetY());
		if (pCity ~= nil) then
			UnitManager.InitUnit(player, effectType, pCity:GetX(), pCity:GetY());
			-- TESTED?
			szEffectText = Locale.Lookup("LOC_SCENARIO_AUSTRALIA_EVENT_BENEFIT_EFFECT_1", GameInfo.Units[effectType].Name, pCity:GetName());
		end

		-- FUTURE, add way to trigger for some events this effect:
		-- <Row Tag="LOC_SCENARIO_AUSTRALIA_EVENT_BENEFIT_EFFECT_7">
		--  <Text>Your nearby city of {1_CityName} has gained 1 Population.</Text>
		-- </Row>
		-- <Row Tag="LOC_SCENARIO_AUSTRALIA_EVENT_BENEFIT_EFFECT_2">
		-- 	<Text>A {1_UnitName} has arrived next to your {2_UnitName}.</Text>
		-- </Row>

	elseif (effectType == "HEAL") then
		local iDamageHealed = math.floor((iRoll + 50) / 2);
		local iCurrentDamage = pUnit:GetDamage();
		if (iCurrentDamage == 0) then
			pUnit:ChangeExtraMoves(1);
			szEffectText = Locale.Lookup("LOC_SCENARIO_AUSTRALIA_EVENT_BENEFIT_EFFECT_4", pUnit:GetName());

		elseif (iDamageHealed > iCurrentDamage) then
			pUnit:ChangeDamage(-iCurrentDamage);
			-- TESTED?
			szEffectText = Locale.Lookup("LOC_SCENARIO_AUSTRALIA_EVENT_BENEFIT_EFFECT_5", pUnit:GetName());

		else
			-- TESTED?
			pUnit:ChangeDamage(-iDamageHealed);
			szEffectText = Locale.Lookup("LOC_SCENARIO_AUSTRALIA_EVENT_BENEFIT_EFFECT_6", pUnit:GetName(), iDamageHealed);
		end

	elseif (effectType == "GOLD") then
		local iGoldGained = (5 * iCurrentTurn) + iRoll + 50;
		iGoldGained = math.floor(iGoldGained / 10);
		iGoldGained = iGoldGained * 10;
		pPlayer:GetTreasury():ChangeGoldBalance(iGoldGained);
		szEffectText = Locale.Lookup("LOC_SCENARIO_AUSTRALIA_EVENT_BENEFIT_EFFECT_8", iGoldGained);

	elseif (effectType == "TECH_BOOST") then
		-- Find first 4 that could be boosted
		local iNumFound = 0;
		local aPossibleTechBoosts = {};
		for row in GameInfo.Technologies() do
			if (not player:GetTechs():HasTech(row.Index)) then
				if (not player:GetTechs():HasBeenBoosted(row.Index)) then
					iNumFound = iNumFound + 1;
					table.insert(aPossibleTechBoosts, row.Index);
					if iNumFound >= 4 then
						break;
					end
				end
			end
		end
		if (iNumFound > 0) then
			local iChosen = (iNumFound * iRoll) / 100 + 1;
			player:GetTechs():TriggerBoost(aPossibleTechBoosts[iChosen], 1); -- Trigger as if from goody hut
			-- TESTED?
			szEffectText = Locale.Lookup("LOC_SCENARIO_AUSTRALIA_EVENT_BENEFIT_EFFECT_9", GameInfo.Technologies[aPossibleTechBoosts[iChosen]].Name);
		end

	elseif (effectType == "GAIN_POP") then
		local pCity = FindClosestCity(player, pUnit:GetX(), pUnit:GetY())
		if (pCity ~= nil) then
			pCity:ChangePopulation(1);
			-- TESTED?
			szEffectText = Locale.Lookup("LOC_SCENARIO_AUSTRALIA_EVENT_BENEFIT_EFFECT_7", pCity:GetName());
		end

	elseif (effectType == "DAMAGE") then
		local iDamageInflicted = math.floor((iRoll + 50) / 2);
		if (bHasCartography == true) then
			iDamageInflicted = iDamageInflicted - 10;
		end
		local iCurrentDamage = pUnit:GetDamage();
		if ((iCurrentDamage + iDamageInflicted) >= iMaxDamage) then
			UnitManager.Kill(pUnit);
			Game.UnlockAchievement(pUnit:GetOwner(), "DLC3_ACHIEVEMENT_10");
			-- TESTED?
			szEffectText = Locale.Lookup("LOC_SCENARIO_AUSTRALIA_EVENT_DANGER_EFFECT_2", pUnit:GetName());

		else
			pUnit:ChangeDamage(iDamageInflicted);
			szEffectText = Locale.Lookup("LOC_SCENARIO_AUSTRALIA_EVENT_DANGER_EFFECT_1", pUnit:GetName(), iDamageInflicted);
		end

		-- FUTURE, add way to trigger for some events this effect:
		-- 		<Row Tag="LOC_SCENARIO_AUSTRALIA_EVENT_DANGER_EFFECT_6">
		-- 			<Text>Your nearby {1_Improvement} has been pillaged.</Text>
		-- 		</Row>

	elseif (effectType == "LOSE_MOVEMENT") then
		pUnit:ChangeExtraMoves(-1);
		szEffectText = Locale.Lookup("LOC_SCENARIO_AUSTRALIA_EVENT_DANGER_EFFECT_3", pUnit:GetName());

	elseif (effectType == "KILLED") then

		local bGoAheadWithKill = true;
		local iCurrentDamage = pUnit:GetDamage();		
		-- Dies automatically if doesn't have Cartography or if already damaged
		if (bHasCartography == true and iCurrentDamage == 0) then
			local iDamageInflicted = math.floor((iRoll + 50) / 2);
			if (iDamageInflicted < 50) then
				iDamageInflicted = iDamageInflicted + 20;
				pUnit:ChangeDamage(iDamageInflicted);
				szEffectText = Locale.Lookup("LOC_SCENARIO_AUSTRALIA_EVENT_DANGER_EFFECT_1", pUnit:GetName(), iDamageInflicted);
				bGoAheadWithKill = false;
			end	
		end
		if (bGoAheadWithKill == true) then
			UnitManager.Kill(pUnit);
			Game.UnlockAchievement(pUnit:GetOwner(), "DLC3_ACHIEVEMENT_10");
			szEffectText = Locale.Lookup("LOC_SCENARIO_AUSTRALIA_EVENT_DANGER_EFFECT_2", pUnit:GetName());
		end

	elseif (effectType == "LOSE_GOLD") then
		local iGoldLost = (5 * iCurrentTurn) + iRoll + 50;
		iGoldLost = math.floor(iGoldLost / 20);  -- Half of the gold gain event 
		iGoldLost = iGoldLost * 10;
		pPlayer:GetTreasury():ChangeGoldBalance(-iGoldLost);
		-- TESTED?
		szEffectText = Locale.Lookup("LOC_SCENARIO_AUSTRALIA_EVENT_DANGER_EFFECT_4", iGoldLost);

	elseif (effectType == "RETURN_CITY") then
		local pCity = FindClosestCity(player, pUnit:GetX(), pUnit:GetY())
		if (pCity ~= nil) then
			UnitManager.PlaceUnit(pUnit, pCity:GetX(), pCity:GetY());
			-- TESTED?
			szEffectText = Locale.Lookup("LOC_SCENARIO_AUSTRALIA_EVENT_DANGER_EFFECT_5", pUnit:GetName(), pCity:GetName());
		end

	elseif (effectType == "PILLAGE") then
		local pPlot = FindClosestImprovement(player, pUnit:GetX(), pUnit:GetY());
		Map.GetImprovementBuilder().SetImprovementPillaged(pPlot, true);
		local szImprovementName = GameInfo.Improvements[pPlot:GetImprovementType()].Name;
		-- TESTED?
		szEffectText = Locale.Lookup("LOC_SCENARIO_AUSTRALIA_EVENT_DANGER_EFFECT_6", szImprovementName);

	elseif (effectType == "LOSE_MAP") then
		local localPlayerVis:table = PlayersVisibility[player];
		local iPlotsLost = 0;
		for iX = pUnit:GetX() - 4, pUnit:GetX() + 3 do
			for iY = pUnit:GetY() - 4, pUnit:GetY() + 3 do
				local revealPlot = Map.GetPlot(iX, iY);
				if (revealPlot ~= nil and Map.GetPlotDistance(iX, iY, pUnit:GetX(), pUnit:GetY()) > 1) then
					if (localPlayerVis:IsRevealed(iX, iY) and revealPlot:GetOwner() ~= player) then
						iPlotsLost = iPlotsLost + 1;
					end
					localPlayerVis:ChangeVisibilityCount(revealPlot:GetIndex(), -1 * localPlayerVis:GetVisibilityCount(revealPlot:GetIndex()));
				end
			end
		end	
		if (iPlotsLost > 0) then	
			-- TESTED?
			szEffectText = Locale.Lookup("LOC_SCENARIO_AUSTRALIA_EVENT_DELAY_EFFECT_3", pUnit:GetName());
		else
			local pCity = FindClosestCity(player, pUnit:GetX(), pUnit:GetY());
			if (pCity ~= nil) then
				UnitManager.PlaceUnit(pUnit, pCity:GetX(), pCity:GetY());
				-- TESTED?
				szEffectText = Locale.Lookup("LOC_SCENARIO_AUSTRALIA_EVENT_DANGER_EFFECT_5", pUnit:GetName(), pCity:GetName());
			end
		end

	elseif (effectType == "LOSE_MOVES") then
		local iMovesLost = 2;
		szEffectText = Locale.Lookup("LOC_SCENARIO_AUSTRALIA_EVENT_DELAY_EFFECT_1", pUnit:GetName(), iMovesLost);
		-- DELAYED EFFECT
		g_aDelayedEventUnit[player + 1] = pUnit
		g_aDelayedEventEffect[player + 1] = effectType;

	elseif (effectType == "DELAY_MOVE") then
		szEffectText = Locale.Lookup("LOC_SCENARIO_AUSTRALIA_EVENT_DELAY_EFFECT_2", pUnit:GetName());
		-- DELAYED EFFECT
		g_aDelayedEventUnit[player + 1] = pUnit
		g_aDelayedEventEffect[player + 1] = effectType;

	end

	return szEffectText;
end

-- ===========================================================================
local function TriggerDelayedEffect (player, turn)

	local effectType = g_aDelayedEventEffect[player + 1];
	local pUnit = g_aDelayedEventUnit[player + 1];
	
	if (effectType == "LOSE_MOVES") then
		local iMovesLost = 2;
		UnitManager.ChangeMovesRemaining(pUnit, -iMovesLost);
		print ("Delayed effect: " .. effectType);

	elseif (effectType == "DELAY_MOVE") then
		UnitManager.FinishMoves(pUnit);
		print ("Delayed effect: " .. effectType);

	end
end

-- ===========================================================================
local function CheckForExplorationEvent( player, currentTurn )

	-- No exploration events until Settler can arrive
	if (currentTurn > g_iFirstColonistTurn and currentTurn < g_iFinalTurn) then
		
		-- Do we want an exploration event at all this turn?
		local iPossibleExplorationEventTurns = currentTurn - g_iFirstColonistTurn;
		local iTotalEventsSoFar = g_iNumBenefitEventsTriggered + g_iNumDangerEventsTriggered + g_iNumDelayEventsTriggered;
		local iPercentTurnsWithEvent = 100 * iTotalEventsSoFar / iPossibleExplorationEventTurns;
		local iChanceEventThisTurn  = 100 - iPercentTurnsWithEvent;
		if g_aRandomRolls[1 + (player * g_iRollsPerPlayer)] > iChanceEventThisTurn then
			print ("Failed roll for event this turn");
			return false;
		end

		FindPossibleEventUnits(player, currentTurn);
		if (#g_aPossibleEventUnits == 0) then
			print ("No unit outside our territory this turn");
			return false;
		end

		FindPossibleEvents(player, currentTurn);
		if (#g_aPossibleEventIndices == 0) then
			print ("No events possible this turn");
			return false;
		end

		local iTotalEventsTriggered = g_iNumBenefitEventsTriggered + g_iNumDangerEventsTriggered + g_iNumDelayEventsTriggered;

		-- 50% Danger
		local eEventType = eEventTypeNone;
		if (g_iNumDangerEventsTriggered * 2 <= iTotalEventsTriggered) then
			eEventType = eEventTypeDangers;
			print "Danger event selected";

		-- 25% Delay
		elseif (g_iNumDelayEventsTriggered * 4 <= iTotalEventsTriggered) then
			eEventType = eEventTypeDelays;
			print "Delay event selected";

		-- Otherwise Benefit
		else
			eEventType = eEventTypeBenefits;
			print "Benefit event selected";
		end

		local iPossibleEvent = SelectEvent(eEventType, player);
		if (iPossibleEvent ~= -1) then
			local effectString = TriggerGameplayEffect(iPossibleEvent, player);
			local iEventIndex = g_aPossibleEventIndices[iPossibleEvent];
			local evtType, evtNum = RowToTypeNumber(iEventIndex);
			
			if (evtType ~= eEventType) then
				print ("Internal scripting error!");
			end

			print ("Selected event number (within this type): " .. tostring(evtNum));

			CachePlayerEvent(player, eEventType, evtNum, effectString);
			return true;
		end
	end

	return false;
end

-- ===========================================================================
local function CheckForScenarioEvent( player, currentTurn )

	local bEventCached;

	RetrieveEventData(player);

	-- Cache empty event in case we don't trigger one
	CachePlayerEvent(player, eEventTypeNone, 0, "");

	bEventCached = CheckForPoliticalEvent(player, currentTurn);

	if (not bEventCached) then
		bEventCached = CheckForExplorationEvent(player, currentTurn);
	end

	if (bEventCached) then
		SaveEventData(player);
	end
end

-- ===========================================================================
local function TriggerCachedEvent( player, currentTurn )

	local eventType = g_aTurnEventType[player + 1];
	if (eventType ~= eEventTypeNone) then
		
		local eventNum = g_aTurnEventNumber[player + 1];
		local eventEffectString = g_aTurnEventEffect[player + 1];

		if (eventType == eEventTypePolitical) then
			if (eventNum == 1) then
				   ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = player, EventKey = "SCENARIO_AUSTRALIA_EVENT_POLITICAL_1" });
			elseif (eventNum == 2) then
				   ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = player, EventKey = "SCENARIO_AUSTRALIA_EVENT_POLITICAL_2" });
			elseif (eventNum == 3) then
				   ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = player, EventKey = "SCENARIO_AUSTRALIA_EVENT_POLITICAL_3" });
			elseif (eventNum == 4) then
				   ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = player, EventKey = "SCENARIO_AUSTRALIA_EVENT_POLITICAL_4" });
			elseif (eventNum == 5) then
				   ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = player, EventKey = "SCENARIO_AUSTRALIA_EVENT_POLITICAL_5" });
			end
		elseif (eventType == eEventTypeDangers) then
			local iIndex = eventNum - 1 + g_iNumBenefitEventsInDB;
			local eventKey = GameInfo.EventPopupData[iIndex].Type;
			ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = player, EventKey = eventKey, EventEffect = eventEffectString });
		elseif (eventType == eEventTypeDelays) then
			local iIndex = eventNum - 1 + g_iNumBenefitEventsInDB + g_iNumDangerEventsInDB;
			local eventKey = GameInfo.EventPopupData[iIndex].Type;
			ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = player, EventKey = eventKey, EventEffect = eventEffectString });
			 
		elseif (eventType == eEventTypeBenefits) then
			local iIndex = eventNum - 1;
			local eventKey = GameInfo.EventPopupData[iIndex].Type;
			ReportingEvents.Send("EVENT_POPUP_REQUEST", { ForPlayer = player, EventKey = eventKey, EventEffect = eventEffectString });
		end
	end
end

--- ===========================================================================
local function ScoreStartPosition(coord)

	g_iW, g_iH = Map.GetGridSize();
	local iPlotIndex = coord[2] * g_iW + coord[1];
	local pPlot = Map.GetPlotByIndex(iPlotIndex);
	local iScore = 0;

	if (pPlot:IsImpassable()) then
		return -1;
	elseif (pPlot:IsWater()) then
		return -1;
	elseif (pPlot:IsNaturalWonder()) then
		return -1;
	end

	if (AnyCityWithinXTiles(10, pPlot:GetX(), pPlot:GetY())) then
		return -1;
	end

	-- Resource under city
	if (pPlot:GetResourceType() ~= -1) then
		iScore = iScore + 5;
	end

	-- City on river
	if (pPlot:IsRiver()) then
		iScore = iScore + 10;
	end

	-- Score all tiles around possible city
	for direction = 0, DirectionTypes.NUM_DIRECTION_TYPES - 1, 1 do
		local adjacentPlot = Map.GetAdjacentPlot(pPlot:GetX(), pPlot:GetY(), direction);
		if (adjacentPlot ~= nil and not adjacentPlot:IsImpassable()) then
			if (adjacentPlot:IsMountain()) then
				iScore = iScore + 2;
			else
				eTerrainType = adjacentPlot:GetTerrainType();
				terrainName = GameInfo.Terrains[eTerrainType].TerrainType;
				if (terrainName == "TERRAIN_GRASS" or
					terrainName == "TERRAIN_GRASS_HILLS") then
					iScore = iScore + 5;
				elseif (terrainName == "TERRAIN_PLAINS" or
					terrainName == "TERRAIN_PLAINS_HILLS") then
					iScore = iScore + 4;
				end

				eFeatureType = adjacentPlot:GetFeatureType();
				if (eFeatureType ~= -1) then
					featureName = GameInfo.Features[eFeatureType].FeatureType;
					if (featureName == "FEATURE_WOODS" or
						featureName == "FEATURE_JUNGLE") then
						iScore = iScore + 2;
					elseif (featureName == "FEATURE_OASIS" or
							featureName == "FEATURE_MARSH" or
							featureName == "FEATURE_FLOODPLAINS" ) then
						iScore = iScore + 3;
					elseif (adjacentPlot:IsNaturalWonder()) then
						iScore = iScore + 10;
					end
				end

				if (adjacentPlot:GetResourceType() ~= -1) then
					iScore = iScore + 5;	
				end
			end
		end
	end

	return iScore;
end

--- ===========================================================================
local function FindBestStartPosition(startHexList)

	local iBestScore = 0;
	local kBestPosition = nil;

	for iCityIndex = 1, 10 do
		local iScore = ScoreStartPosition(startHexList[iCityIndex]);
		if (iScore > iBestScore) then
			iBestScore = iScore;
			kBestPosition = startHexList[iCityIndex];
		end
	end

	return kBestPosition;
end

--- ===========================================================================
local function AddCityIn(civTypeName, playerID)

	print ("Placing player ID: " .. tostring(playerID) .. " in " .. civTypeName);

	local startHexList = {};
	if (civTypeName == "CIVILIZATION_VICTORIA") then
		startHexList = victoriaStartHexes;
	elseif (civTypeName == "CIVILIZATION_QUEENSLAND") then
		startHexList = queenslandStartHexes;
	elseif (civTypeName == "CIVILIZATION_SOUTH_AUSTRALIA") then
		startHexList = southAustraliaStartHexes;
	else
		startHexList = westernAustraliaStartHexes;
	end
	 
	local startHex = FindBestStartPosition(startHexList);

	local pAIPlayer = Players[playerID];
	if (pAIPlayer ~= nil and startHex ~= nil) then
		pAIPlayer:GetCities():Create(startHex[1], startHex[2]);
	end
end

--- ===========================================================================
local function AddAICities(cityX, cityY)

	local bCivInWesternAustralia = false;
	local bCivInSouthAustralia = false;
	local bCivInVictoria = false;
	local bCivInQueensland = false;
	if (cityX >= westernAustraliaSWCorner[1] and cityX <= westernAustraliaNECorner[1] and
		cityY >= westernAustraliaSWCorner[2] and cityY <= westernAustraliaNECorner[2]) then
		bCivInWesternAustralia = true;
		print ("Western Australia");
	end
	if (cityX >= southAustraliaSWCorner[1] and cityX <= southAustraliaNECorner[1] and
		cityY >= southAustraliaSWCorner[2] and cityY <= southAustraliaNECorner[2]) then
		bCivInSouthAustralia = true;
		print ("South Australia");
	end
	if (cityX >= victoriaSWCorner[1] and cityX <= victoriaNECorner[1] and
		cityY >= victoriaSWCorner[2] and cityY <= victoriaNECorner[2]) then
		bCivInVictoria = true;
		print ("Victoria");
	end
	if (cityX >= queenslandSWCorner[1] and cityX <= queenslandNECorner[1] and
		cityY >= queenslandSWCorner[2] and cityY <= queenslandNECorner[2]) then
		bCivInQueensland = true;
		print ("Queensland");
	end

	-- Place AI civs in their historical spot if possible
	local aPlayers = PlayerManager.GetAliveMajors();
	for loop, pPlayer in ipairs(aPlayers) do
		if (not pPlayer:IsHuman() and pPlayer:GetCities():GetCount() == 0) then
			local pPlayerConfig :table = PlayerConfigurations[pPlayer:GetID()];
			local civ = pPlayerConfig:GetCivilizationTypeName();
			if (civ == "CIVILIZATION_VICTORIA" and not bCivInVictoria) then
				AddCityIn (civ, pPlayer:GetID());
				bCivInVictoria = true;
			end
			if (civ == "CIVILIZATION_QUEENSLAND" and not bCivInQueensland) then
				AddCityIn (civ, pPlayer:GetID());
				bCivInQueensland = true;
			end
			if (civ == "CIVILIZATION_SOUTH_AUSTRALIA" and not bCivInSouthAustralia) then
				AddCityIn (civ, pPlayer:GetID());
				bCivInSouthAustralia = true;
			end
			if (civ == "CIVILIZATION_WESTERN_AUSTRALIA" and not bCivInWesternAustralia) then
				AddCityIn (civ, pPlayer:GetID());
				bCivInWesternAustralia = true;
			end
		end
	end

	-- Historic spot taken, place AI civs in first available
	local aPlayers = PlayerManager.GetAliveMajors();
	for loop, pPlayer in ipairs(aPlayers) do
		if (not pPlayer:IsHuman() and pPlayer:GetCities():GetCount() == 0) then
			local pPlayerConfig :table = PlayerConfigurations[pPlayer:GetID()];
			if (not bCivInVictoria) then
				AddCityIn ("CIVILIZATION_VICTORIA", pPlayer:GetID());
				bCivInVictoria = true;
			end
			if (not bCivInQueensland) then
				AddCityIn ("CIVILIZATION_QUEENSLAND", pPlayer:GetID());
				bCivInQueensland = true;
			end
			if (not bCivInSouthAustralia) then
				AddCityIn ("CIVILIZATION_SOUTH_AUSTRALIA", pPlayer:GetID());
				bCivInSouthAustralia = true;
			end
			if (not bCivInWesternAustralia) then
				AddCityIn ("CIVILIZATION_WESTERN_AUSTRALIA", pPlayer:GetID());
				bCivInWesternAustralia = true;
			end
		end
	end
end

-- ===========================================================================
-- Initialization functions
-- ===========================================================================
function Initialize()
		
end

-- ===========================================================================
local function SetInitialVisibility()

	print ("SetInitialVisibility");

	g_iW, g_iH = Map.GetGridSize();

	-- Apply updates to all majors
	local aPlayers = PlayerManager.GetAliveMajors();
	for loop, pPlayer in ipairs(aPlayers) do
		local iPlayer = pPlayer:GetID();
	
		local pCurPlayerVisibility = PlayersVisibility[pPlayer:GetID()];
		if(pCurPlayerVisibility ~= nil) then

			-- Reveal oceans and coasts
			for iX = 0, g_iW - 1 do
				for iY = 0, g_iH - 1 do
					local iPlotIndex = iY * g_iW + iX;
					local pPlot = Map.GetPlotByIndex(iPlotIndex);
					if (pPlot:IsWater() and not pPlot:IsLake()) then
						pCurPlayerVisibility:ChangeVisibilityCount(iPlotIndex, 1);
					end
				end
			end

			-- Reveal New South Wales
			local iPlotIndex;
			iX = 50; iY = 9;
			pCurPlayerVisibility:ChangeVisibilityCount(iY * g_iW + iX, 1);
			iX = 50; iY = 10;
			pCurPlayerVisibility:ChangeVisibilityCount(iY * g_iW + iX, 1);
			iX = 51; iY = 10;
			pCurPlayerVisibility:ChangeVisibilityCount(iY * g_iW + iX, 1);
			iX = 50; iY = 11;
			pCurPlayerVisibility:ChangeVisibilityCount(iY * g_iW + iX, 1);
			iX = 51; iY = 11;
			pCurPlayerVisibility:ChangeVisibilityCount(iY * g_iW + iX, 1);
			iX = 51; iY = 12;
			pCurPlayerVisibility:ChangeVisibilityCount(iY * g_iW + iX, 1);
			iX = 52; iY = 12;
			pCurPlayerVisibility:ChangeVisibilityCount(iY * g_iW + iX, 1);
			iX = 51; iY = 13;
			pCurPlayerVisibility:ChangeVisibilityCount(iY * g_iW + iX, 1);
			iX = 52; iY = 13;
			pCurPlayerVisibility:ChangeVisibilityCount(iY * g_iW + iX, 1);
		end
	end
end

-- ===========================================================================
function InitializeNewGame()

	print ("InitializeNewGame");

	-- Majors have met
	Players[0]:GetDiplomacy():SetHasMet(1);
	Players[0]:GetDiplomacy():SetHasMet(2);
	Players[0]:GetDiplomacy():SetHasMet(3);
	Players[1]:GetDiplomacy():SetHasMet(2);
	Players[1]:GetDiplomacy():SetHasMet(3);
	Players[2]:GetDiplomacy():SetHasMet(3);
	
	-- Majors are allied
	Players[0]:GetDiplomacy():SetPermanentAlliance(1);
	Players[0]:GetDiplomacy():SetPermanentAlliance(2);
	Players[0]:GetDiplomacy():SetPermanentAlliance(3);
	Players[1]:GetDiplomacy():SetPermanentAlliance(2);
	Players[1]:GetDiplomacy():SetPermanentAlliance(3);
	Players[2]:GetDiplomacy():SetPermanentAlliance(3);

	-- Majors also have delegations
	Players[0]:GetDiplomacy():SetHasDelegationAt(1, true);
	Players[0]:GetDiplomacy():SetHasDelegationAt(2, true);
	Players[0]:GetDiplomacy():SetHasDelegationAt(3, true);
	Players[1]:GetDiplomacy():SetHasDelegationAt(0, true);
	Players[1]:GetDiplomacy():SetHasDelegationAt(2, true);
	Players[1]:GetDiplomacy():SetHasDelegationAt(3, true);
	Players[2]:GetDiplomacy():SetHasDelegationAt(0, true);
	Players[2]:GetDiplomacy():SetHasDelegationAt(1, true);
	Players[2]:GetDiplomacy():SetHasDelegationAt(3, true);
	Players[3]:GetDiplomacy():SetHasDelegationAt(0, true);
	Players[3]:GetDiplomacy():SetHasDelegationAt(1, true);
	Players[3]:GetDiplomacy():SetHasDelegationAt(2, true);
	Players[0]:GetDiplomacy():RecheckVisibilityOnAll();
	Players[1]:GetDiplomacy():RecheckVisibilityOnAll();
	Players[2]:GetDiplomacy():RecheckVisibilityOnAll();
	Players[3]:GetDiplomacy():RecheckVisibilityOnAll();

	SetInitialVisibility();

	-- Set up Sydney
	local pSydneyPlayer = Players[eSydney];
	if (pSydneyPlayer ~= nil) then
		pSydneyPlayer:GetCities():Create(51, 11);
		local pPlot = Map.GetPlot(52, 11)
		pPlot:SetOwner(-1);
		pPlot = Map.GetPlot(52, 10);
		pPlot:SetOwner(-1);
		
		-- Don't need their Settler
		local pSettler = UnitManager.GetUnit(eSydney, 65536);
		UnitManager.Kill(pSettler);
    end

	-- Apply updates to all majors
	local aPlayers = PlayerManager.GetAliveMajors();
	for loop, pPlayer in ipairs(aPlayers) do
		local iPlayer = pPlayer:GetID();
	
		InitializeEventData(iPlayer);

		-- Almost finished with Medieval era
		local pCulture:table = pPlayer:GetCulture();
		local pScience:table = pPlayer:GetTechs();
		-- pCulture:SetCivic(GameInfo.Civics["CIVIC_MERCENARIES"].Index, false);
		pScience:SetTech(GameInfo.Technologies["TECH_SHIPBUILDING"].Index, true);

		if pPlayer:IsHuman() then
			local iStartX = -1;
			local iStartY = -1;
		    if (loop == 1) then
				iStartX=52;
				iStartY=10;
		    elseif (loop == 2) then
				iStartX=52;
				iStartY=11;
		    elseif (loop == 3) then
				iStartX=53;
				iStartY=12;
		    elseif (loop == 4) then
				iStartX=53;
				iStartY=13;
			end
			print ("InitUnit (Explorer), playerID = " .. tostring(iPlayer) .. " at " .. tostring(iStartX) .. ", " .. tostring(iStartY));
			UnitManager.InitUnit(iPlayer, "UNIT_EXPLORER", iStartX, iStartY);
			local curPlayerConfig = PlayerConfigurations[iPlayer];
			if curPlayerConfig:GetLeaderTypeName() == "LEADER_SCENARIO_VICTORIA" then
				UnitManager.InitUnit(iPlayer, "UNIT_PROSPECTOR", iStartX, iStartY);
			elseif curPlayerConfig:GetLeaderTypeName() == "LEADER_SOUTH_AUSTRALIA" then
				UnitManager.InitUnit(iPlayer, "UNIT_GRAZIER", iStartX, iStartY);
			end
		end
	end
end
LuaEvents.NewGameInitialized.Add(InitializeNewGame);

-- ===========================================================================
-- Event Handlers
-- ===========================================================================
local function OnPlayerTurnActivated( player )
	local currentTurn = Game.GetCurrentGameTurn();
	
	print ("OnPlayerTurnActivated: Player " .. tostring(player) .. ", Turn " .. tostring(currentTurn));

	local pPlayer = Players[player];
	if (pPlayer:IsHuman() == true) then

		-- Reverse civic boost for Conservation for finding GBR at start
		if (currentTurn == 1) then
			pPlayer:GetCulture():ReverseBoost(GameInfo.Civics["CIVIC_CONSERVATION"].Index);
		end
		
		-- First settler arrives in closest coastal tile to settler on Turn 5 (g_iFirstColonistTurn)
		if (currentTurn == g_iFirstColonistTurn) then
			local pExplorer = UnitManager.GetUnit(player, 65536);
			local pSettlerPlot = nil;
			if (pExplorer ~= nil) then
				pSettlerPlot = FindClosestCoast(pExplorer:GetX(), pExplorer:GetY());
			else
				pSettlerPlot = Map.GetPlot(52, 10);
			end
			if (pSettlerPlot ~= nil) then

				print ("Creating settler at " .. tostring(pSettlerPlot:GetX()) .. ", " .. tostring(pSettlerPlot:GetY()));

				local pSettler = UnitManager.InitUnit(player, "UNIT_SETTLER", pSettlerPlot:GetX(), pSettlerPlot:GetY());			
				UnitManager.ChangeMovesRemaining(pSettler, 18); -- give him full embarked movement		
			end
		end
				
		TriggerDelayedEffect(player, currentTurn);
		TriggerCachedEvent(player, currentTurn);
	end
end
GameEvents.PlayerTurnStartComplete.Add(OnPlayerTurnActivated);

-- ===========================================================================
local function OnGameTurnStarted( player )
	local currentTurn = Game.GetCurrentGameTurn();
	
	print ("OnGameTurnStarted: Turn " .. tostring(currentTurn));

	GenerateDieRolls();

	local aPlayers = PlayerManager.GetAliveMajors();
	for loop, pPlayer in ipairs(aPlayers) do
		local iPlayer = pPlayer:GetID();
		if (pPlayer:IsHuman() == true) then
		    CheckForScenarioEvent(iPlayer, currentTurn);
		end
	end
end
GameEvents.OnGameTurnStarted.Add(OnGameTurnStarted);

-- ===========================================================================
function OnCityAddedToMap( playerID: number, cityID : number, cityX : number, cityY : number )
	
	print("CityAddedToMap - " .. tostring(playerID) .. ":" .. tostring(cityID) .. " " .. tostring(cityX) .. "x" .. tostring(cityY));
	if (playerID == 0 and Players[playerID]:GetCities():GetCount() == 1) then
		AddAICities(cityX, cityY);
	end
end
GameEvents.CityBuilt.Add(OnCityAddedToMap );

-- ===========================================================================

Initialize();