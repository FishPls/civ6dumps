include("InstanceManager");
include("CivicUnlockIcon");

g_ExtraIconData["MODIFIER_PLAYER_GRANT_UNIT_IN_CAPITAL"] = {
	IM = InstanceManager:new( "CivicUnlockNumberedInstance", "Top"),
	HideDescriptionIcon = true,
	
	Initialize = function(self:table, rootControl:table, itemData:table)
			if itemData == nil then 
				UI.DataError("Failed to find civics tree icon data.");
				return;
			end

			local instance = CivicUnlockIcon.GetInstance(self.IM, rootControl);

			local tooltip = "";
			local numUnitsAwarded = -1;
			if itemData.ModifierId == "CIVIC_AWARD_ONE_SETTLER" then
				numUnitsAwarded = 1;
				tooltip = Locale.Lookup("LOC_CIVIC_SCENARIO_CROWN_COLONY_DESCRIPTION");
			elseif itemData.ModifierId == "CIVIC_AWARD_TWO_SETTLERS" then
				numUnitsAwarded = 2;
				tooltip = Locale.Lookup("LOC_CIVIC_SCENARIO_GOLD_RUSH_DESCRIPTION");
			end

			instance:UpdateNumberedIcon(numUnitsAwarded, "[Icon_Unit]", tooltip, itemData.Callback);
		end,

	Reset = function(self)
			self.IM:ResetInstances();
		end
};
