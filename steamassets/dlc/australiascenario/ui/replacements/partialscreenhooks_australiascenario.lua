-- ===========================================================================
-- INCLUDE BASE FILE
-- ===========================================================================
include("PartialScreenHooks");

-- ===========================================================================
function CheckSpyCapacity(localPlayer)
	-- Ensure m_isEspionageUnlocked is never set to true
end

-- ===========================================================================
function OnDiplomacyMeet(player1ID:number, player2ID:number)
	-- Ensure m_isCityStatesUnlocked is never set to true
end

-- ===========================================================================
function CheckCityStatesUnlocked(localPlayer:table)
	-- Ensure m_isCityStatesUnlocked is never set to true 
end