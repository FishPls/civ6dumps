-- ===========================================================================
--
--	Indonesia Khmer Scenario
-- ===========================================================================


-- ===========================================================================

function Initialize()
end
-- ===========================================================================
function InitializeNewGame()
	local aPlayers = PlayerManager.GetAlive();


	for loop, pPlayer in ipairs(aPlayers) do
		local iPlayer = pPlayer:GetID();
		local pDiplomacy:table = pPlayer:GetDiplomacy();
		local pCulture:table = pPlayer:GetCulture();
		local config = PlayerConfigurations[iPlayer];

		if(GameInfo.Civilizations["CIVILIZATION_CHINA"].Hash == config:GetCivilizationTypeID() or 
			GameInfo.Civilizations["CIVILIZATION_KHMER"].Hash == config:GetCivilizationTypeID() or 
			GameInfo.Civilizations["CIVILIZATION_KUCHA"].Hash == config:GetCivilizationTypeID() or
			GameInfo.Civilizations["CIVILIZATION_LAMPHUN"].Hash == config:GetCivilizationTypeID() or 
			GameInfo.Civilizations["CIVILIZATION_TEMASEK"].Hash == config:GetCivilizationTypeID() or 
			GameInfo.Civilizations["CIVILIZATION_UJJAIN"].Hash == config:GetCivilizationTypeID() or 
			GameInfo.Civilizations["CIVILIZATION_XINGQING"].Hash == config:GetCivilizationTypeID() or
			GameInfo.Civilizations["CIVILIZATION_ZHONGJING"].Hash == config:GetCivilizationTypeID() ) then

			pCulture:SetCivic(GameInfo.Civics["CIVIC_EARLY_EMPIRE"].Index, true);
		end
	
		if(GameInfo.Civilizations["CIVILIZATION_CHOLA"].Hash == config:GetCivilizationTypeID() or 
			GameInfo.Civilizations["CIVILIZATION_GHAZNAVID"].Hash == config:GetCivilizationTypeID() or
			GameInfo.Civilizations["CIVILIZATION_INDONESIA"].Hash == config:GetCivilizationTypeID() or 
			GameInfo.Civilizations["CIVILIZATION_JAKARTA"].Hash == config:GetCivilizationTypeID() or 
			GameInfo.Civilizations["CIVILIZATION_GOA"].Hash == config:GetCivilizationTypeID() or 
			GameInfo.Civilizations["CIVILIZATION_HEIAN"].Hash == config:GetCivilizationTypeID() or 
			GameInfo.Civilizations["CIVILIZATION_LAMURI"].Hash == config:GetCivilizationTypeID() or 
			GameInfo.Civilizations["CIVILIZATION_THANG_LONG"].Hash == config:GetCivilizationTypeID() or 
			GameInfo.Civilizations["CIVILIZATION_TONDO"].Hash == config:GetCivilizationTypeID() ) then

			pCulture:SetCivic(GameInfo.Civics["CIVIC_CELESTIAL_NAVIGATION"].Index, true);
		end
	
		if(GameInfo.Civilizations["CIVILIZATION_GHAZNAVID"].Hash == config:GetCivilizationTypeID() or 
			GameInfo.Civilizations["CIVILIZATION_DALI"].Hash == config:GetCivilizationTypeID() or 
			GameInfo.Civilizations["CIVILIZATION_GAOCHANG"].Hash == config:GetCivilizationTypeID() or 
			GameInfo.Civilizations["CIVILIZATION_KHOTAN"].Hash == config:GetCivilizationTypeID() or 
			GameInfo.Civilizations["CIVILIZATION_PALEMBANG"].Hash == config:GetCivilizationTypeID() or 
			GameInfo.Civilizations["CIVILIZATION_SAMARKAND"].Hash == config:GetCivilizationTypeID() ) then

			pCulture:SetCivic(GameInfo.Civics["CIVIC_CURRENCY"].Index, true);
		end


		if(GameInfo.Civilizations["CIVILIZATION_PALA"].Hash == config:GetCivilizationTypeID() or 
			GameInfo.Civilizations["CIVILIZATION_TUFAN"].Hash == config:GetCivilizationTypeID() or 
			GameInfo.Civilizations["CIVILIZATION_KHMER"].Hash == config:GetCivilizationTypeID() or 
			GameInfo.Civilizations["CIVILIZATION_DELHI"].Hash == config:GetCivilizationTypeID() or 
			GameInfo.Civilizations["CIVILIZATION_KAESONG"].Hash == config:GetCivilizationTypeID() or 
			GameInfo.Civilizations["CIVILIZATION_KASHGAR"].Hash == config:GetCivilizationTypeID() or 
			GameInfo.Civilizations["CIVILIZATION_MALABANG"].Hash == config:GetCivilizationTypeID() or 
			GameInfo.Civilizations["CIVILIZATION_SOMNATH"].Hash == config:GetCivilizationTypeID() or 
			GameInfo.Civilizations["CIVILIZATION_PAGAN"].Hash == config:GetCivilizationTypeID() or 
			GameInfo.Civilizations["CIVILIZATION_TURPAN"].Hash == config:GetCivilizationTypeID() or 
			GameInfo.Civilizations["CIVILIZATION_VIJAYA"].Hash == config:GetCivilizationTypeID() ) then

			pCulture:SetCivic(GameInfo.Civics["CIVIC_MYSTICISM"].Index, true);
		end

		
		if(GameInfo.Civilizations["CIVILIZATION_KHMER"].Hash == config:GetCivilizationTypeID()) then
			pCulture:SetCivic(GameInfo.Civics["CIVIC_ENGINEERING"].Index, true);
		end

		for loop2, pPlayer2 in ipairs(aPlayers) do
			local iPlayer2 = pPlayer2:GetID();
			if(iPlayer2 ~= iPlayer and pPlayer2:IsMajor() == true) then
				if(pDiplomacy:HasMet(iPlayer2) == false) then
					pDiplomacy:SetHasMet(iPlayer2);
					pDiplomacy:SetHasDelegationAt(iPlayer2, true);
					pDiplomacy:RecheckVisibilityOnAll();
				end
			end
		end
	end

	aPlayers = PlayerManager.GetAliveMajors();
	local pGameReligion:table = Game.GetReligion();
		for loop, pPlayer in ipairs(aPlayers) do
			local pReligion:table = pPlayer:GetReligion();
			local iPlayer = pPlayer:GetID();
			local religion = -1;

			if(pPlayer ~= nil) then
				local config = PlayerConfigurations[iPlayer];
				if(GameInfo.Civilizations["CIVILIZATION_CHINA"].Hash == config:GetCivilizationTypeID()) then 
					--print("Player Id: ", iPlayer);
					pGameReligion:FoundPantheonHash(iPlayer,  GameInfo.Beliefs["BELIEF_RIVER_GODDESS"].Hash);
					pReligion:SetHolyCity(pPlayer:GetCities():GetCapitalCity());
					pGameReligion:FoundReligion(iPlayer,  GameInfo.Religions["RELIGION_TAOISM"].Index);
					pGameReligion:AddBeliefHash(iPlayer,  GameInfo.Beliefs["BELIEF_FEED_THE_WORLD"].Hash);
					pGameReligion:AddBeliefHash(iPlayer,  GameInfo.Beliefs["BELIEF_PAGODA"].Hash);
					pGameReligion:AddBeliefHash(iPlayer,  GameInfo.Beliefs["BELIEF_MONASTIC_ISOLATION"].Hash);
					pReligion:ChangeNumBeliefsEarned(1);
					religion = GameInfo.Religions["RELIGION_TAOISM"].Index;
					pPlayer:GetAi_Religion():SetFavoredReligion(religion);
					SetPlayerCitiesReligion(pPlayer, religion, false);
				elseif(GameInfo.Civilizations["CIVILIZATION_CHOLA"].Hash == config:GetCivilizationTypeID()) then 
					--print("Player Id: ", iPlayer);
					pGameReligion:FoundPantheonHash(iPlayer,  GameInfo.Beliefs["BELIEF_SACRED_PATH"].Hash);
					pReligion:SetHolyCity(pPlayer:GetCities():GetCapitalCity());
					pGameReligion:FoundReligion(iPlayer,  GameInfo.Religions["RELIGION_CUSTOM_1"].Index);
					pGameReligion:AddBeliefHash(iPlayer,  GameInfo.Beliefs["BELIEF_LINGA"].Hash);
					pGameReligion:AddBeliefHash(iPlayer,  GameInfo.Beliefs["BELIEF_STEWARDSHIP"].Hash);
					pGameReligion:AddBeliefHash(iPlayer,  GameInfo.Beliefs["BELIEF_YOGA"].Hash);
					pReligion:ChangeNumBeliefsEarned(1);
					religion = GameInfo.Religions["RELIGION_CUSTOM_1"].Index;
					pPlayer:GetAi_Religion():SetFavoredReligion(religion);
					SetPlayerCitiesReligion(pPlayer, religion, false);
				elseif(GameInfo.Civilizations["CIVILIZATION_GHAZNAVID"].Hash == config:GetCivilizationTypeID()) then 
					--print("Player Id: ", iPlayer);
					pGameReligion:FoundPantheonHash(iPlayer,  GameInfo.Beliefs["BELIEF_DESERT_FOLKLORE"].Hash);
					pReligion:SetHolyCity(pPlayer:GetCities():GetCapitalCity());
					pGameReligion:FoundReligion(iPlayer,  GameInfo.Religions["RELIGION_ISLAM"].Index);
					pGameReligion:AddBeliefHash(iPlayer,  GameInfo.Beliefs["BELIEF_RELIGIOUS_COMMUNITY"].Hash);
					pGameReligion:AddBeliefHash(iPlayer,  GameInfo.Beliefs["BELIEF_MOSQUE"].Hash);
					pGameReligion:AddBeliefHash(iPlayer,  GameInfo.Beliefs["BELIEF_PILGRIMAGE"].Hash);
					pReligion:ChangeNumBeliefsEarned(1);
					religion = GameInfo.Religions["RELIGION_ISLAM"].Index;
					SetPlayerCitiesReligion(pPlayer, religion, false);
				elseif(GameInfo.Civilizations["CIVILIZATION_INDONESIA"].Hash == config:GetCivilizationTypeID()) then 
					--print("Player Id: ", iPlayer);
					pGameReligion:FoundPantheonHash(iPlayer,  GameInfo.Beliefs["BELIEF_GOD_OF_THE_SEA"].Hash);
					pReligion:SetHolyCity(pPlayer:GetCities():GetCapitalCity());
					pGameReligion:FoundReligion(iPlayer,  GameInfo.Religions["RELIGION_CUSTOM_2"].Index);
					pGameReligion:AddBeliefHash(iPlayer,  GameInfo.Beliefs["BELIEF_KAMPUNG"].Hash);
					pGameReligion:AddBeliefHash(iPlayer,  GameInfo.Beliefs["BELIEF_NUSANTARA"].Hash);
					pGameReligion:AddBeliefHash(iPlayer,  GameInfo.Beliefs["BELIEF_ITINERANT_PREACHERS"].Hash);
					pReligion:ChangeNumBeliefsEarned(1);
					religion = GameInfo.Religions["RELIGION_CUSTOM_2"].Index;
					pPlayer:GetAi_Religion():SetFavoredReligion(religion);
					SetPlayerCitiesReligion(pPlayer, religion, false);
				elseif(GameInfo.Civilizations["CIVILIZATION_KHMER"].Hash == config:GetCivilizationTypeID()) then 
					--print("Player Id: ", iPlayer);
					pGameReligion:FoundPantheonHash(iPlayer,  GameInfo.Beliefs["BELIEF_STONE_CIRCLES"].Hash);
					pReligion:SetHolyCity(pPlayer:GetCities():GetCapitalCity());
					pGameReligion:FoundReligion(iPlayer,  GameInfo.Religions["RELIGION_CUSTOM_3"].Index);
					pGameReligion:AddBeliefHash(iPlayer,  GameInfo.Beliefs["BELIEF_WAT"].Hash);
					pGameReligion:AddBeliefHash(iPlayer,  GameInfo.Beliefs["BELIEF_DANA"].Hash);
					pGameReligion:AddBeliefHash(iPlayer,  GameInfo.Beliefs["BELIEF_WORLD_CHURCH"].Hash);
					pReligion:ChangeNumBeliefsEarned(1);
					religion = GameInfo.Religions["RELIGION_CUSTOM_3"].Index;
					SetPlayerCitiesReligion(pPlayer, religion, false);
				elseif(GameInfo.Civilizations["CIVILIZATION_PALA"].Hash == config:GetCivilizationTypeID()) then 
					--print("Player Id: ", iPlayer);
					pGameReligion:FoundPantheonHash(iPlayer,  GameInfo.Beliefs["BELIEF_EARTH_GODDESS"].Hash);
					pReligion:SetHolyCity(pPlayer:GetCities():GetCapitalCity());
					pGameReligion:FoundReligion(iPlayer,  GameInfo.Religions["RELIGION_CUSTOM_4"].Index);
					pGameReligion:AddBeliefHash(iPlayer,  GameInfo.Beliefs["BELIEF_WORK_ETHIC"].Hash);
					pGameReligion:AddBeliefHash(iPlayer,  GameInfo.Beliefs["BELIEF_MAHAYANA_STUPA"].Hash);
					pGameReligion:AddBeliefHash(iPlayer,  GameInfo.Beliefs["BELIEF_HOLY_ORDER"].Hash);
					pReligion:ChangeNumBeliefsEarned(1);
					religion = GameInfo.Religions["RELIGION_CUSTOM_4"].Index;
					pPlayer:GetAi_Religion():SetFavoredReligion(religion);
					SetPlayerCitiesReligion(pPlayer, religion, false);
				elseif(GameInfo.Civilizations["CIVILIZATION_TUFAN"].Hash == config:GetCivilizationTypeID()) then 
					--print("Player Id: ", iPlayer);
					pGameReligion:FoundPantheonHash(iPlayer,  GameInfo.Beliefs["BELIEF_GOD_OF_THE_OPEN_SKY"].Hash);
					pReligion:SetHolyCity(pPlayer:GetCities():GetCapitalCity());
					pGameReligion:FoundReligion(iPlayer,  GameInfo.Religions["RELIGION_CUSTOM_5"].Index);
					pGameReligion:AddBeliefHash(iPlayer,  GameInfo.Beliefs["BELIEF_ZEN_MEDITATION"].Hash);
					pGameReligion:AddBeliefHash(iPlayer,  GameInfo.Beliefs["BELIEF_MISSIONARY_ZEAL"].Hash);
					pGameReligion:AddBeliefHash(iPlayer,  GameInfo.Beliefs["BELIEF_VAJRAYANA_STUPA"].Hash);
					pReligion:ChangeNumBeliefsEarned(1);
					religion = GameInfo.Religions["RELIGION_CUSTOM_5"].Index;
					pPlayer:GetAi_Religion():SetFavoredReligion(religion);
					SetPlayerCitiesReligion(pPlayer, religion, false);
				end
			end
		end
		
		local aPlayers2 = PlayerManager.GetAliveMinors();
		for loop2, pPlayer2 in ipairs(aPlayers2) do		
			local iPlayer = pPlayer2:GetID();
			local religion = -1;

			if(pPlayer2 ~= nil) then
				local config = PlayerConfigurations[iPlayer];
				
				if(GameInfo.Civilizations["CIVILIZATION_SAMARKAND"].Hash == config:GetCivilizationTypeID()) then
					religion = GameInfo.Religions["RELIGION_ISLAM"].Index;
					SetPlayerCitiesReligion(pPlayer2, religion, true);
				elseif(GameInfo.Civilizations["CIVILIZATION_DELHI"].Hash == config:GetCivilizationTypeID() or
					GameInfo.Civilizations["CIVILIZATION_GOA"].Hash == config:GetCivilizationTypeID() or
					GameInfo.Civilizations["CIVILIZATION_SOMNATH"].Hash == config:GetCivilizationTypeID() or
					GameInfo.Civilizations["CIVILIZATION_UJJAIN"].Hash == config:GetCivilizationTypeID() or
					GameInfo.Civilizations["CIVILIZATION_VIJAYA"].Hash == config:GetCivilizationTypeID()) then
					religion = GameInfo.Religions["RELIGION_CUSTOM_1"].Index;
					SetPlayerCitiesReligion(pPlayer2, religion, true);
				elseif(GameInfo.Civilizations["CIVILIZATION_JAKARTA"].Hash == config:GetCivilizationTypeID() or
					GameInfo.Civilizations["CIVILIZATION_LAMURI"].Hash == config:GetCivilizationTypeID() or
					GameInfo.Civilizations["CIVILIZATION_TONDO"].Hash == config:GetCivilizationTypeID()) then
					religion = GameInfo.Religions["RELIGION_CUSTOM_2"].Index;
					SetPlayerCitiesReligion(pPlayer2, religion, true);
				elseif(GameInfo.Civilizations["CIVILIZATION_LAMPHUN"].Hash == config:GetCivilizationTypeID() or
					GameInfo.Civilizations["CIVILIZATION_MALABANG"].Hash == config:GetCivilizationTypeID() or
					GameInfo.Civilizations["CIVILIZATION_PAGAN"].Hash == config:GetCivilizationTypeID()) then
					religion = GameInfo.Religions["RELIGION_CUSTOM_3"].Index;
					SetPlayerCitiesReligion(pPlayer2, religion, true);
				elseif(GameInfo.Civilizations["CIVILIZATION_DALI"].Hash == config:GetCivilizationTypeID() or
					GameInfo.Civilizations["CIVILIZATION_GAOCHANG"].Hash == config:GetCivilizationTypeID() or
					GameInfo.Civilizations["CIVILIZATION_HEIAN"].Hash == config:GetCivilizationTypeID() or
					GameInfo.Civilizations["CIVILIZATION_KAESONG"].Hash == config:GetCivilizationTypeID() or
					GameInfo.Civilizations["CIVILIZATION_KASHGAR"].Hash == config:GetCivilizationTypeID() or
					GameInfo.Civilizations["CIVILIZATION_KHOTAN"].Hash == config:GetCivilizationTypeID() or
					GameInfo.Civilizations["CIVILIZATION_PALEMBANG"].Hash == config:GetCivilizationTypeID() or
					GameInfo.Civilizations["CIVILIZATION_THANG_LONG"].Hash == config:GetCivilizationTypeID() or
					GameInfo.Civilizations["CIVILIZATION_TEMASEK"].Hash == config:GetCivilizationTypeID() or
					GameInfo.Civilizations["CIVILIZATION_TURPAN"].Hash == config:GetCivilizationTypeID()) then
					religion = GameInfo.Religions["RELIGION_CUSTOM_4"].Index;
					SetPlayerCitiesReligion(pPlayer2, religion, true);
				elseif(GameInfo.Civilizations["CIVILIZATION_KUCHA"].Hash == config:GetCivilizationTypeID() or
					GameInfo.Civilizations["CIVILIZATION_XINGQING"].Hash == config:GetCivilizationTypeID() or
					GameInfo.Civilizations["CIVILIZATION_ZHONGJING"].Hash == config:GetCivilizationTypeID()) then
					religion = GameInfo.Religions["RELIGION_CUSTOM_5"].Index;
					SetPlayerCitiesReligion(pPlayer2, religion, true);
				end
			end
		end
end

-- ===========================================================================
local function OnPlayerTurnActivated(player)
	local currentTurn = Game.GetCurrentGameTurn();
	local pPlayerTurn:table  = Players[player];

	if(pPlayerTurn == nil) then
		return
	end

	if (currentTurn > 1 and pPlayerTurn:IsMajor()) then
		local pReligion:table = pPlayerTurn:GetReligion();
		if(pReligion ~= nil) then
			local iFaith = pReligion:GetFaithYield();
			pPlayerTurn:SetScoringScenario3(iFaith);
		end
	end

	-- Score at the barb turn.
	if (player == 63) then
		local aPlayers = PlayerManager.GetAliveMajors();
		for loop, pPlayer in ipairs(aPlayers) do
			if(pPlayer ~= nil) then
				local pStats:table = pPlayer:GetStats();
				if (pStats ~= nil) then
					local pReligion:table = pPlayer:GetReligion();
					if(pReligion ~= nil) then
						local iFollowers = pStats:GetNumFollowers();
						pPlayer:SetScoringScenario1(iFollowers);

						local iCities = pStats:GetNumForeignCitiesFollowingReligion();
						pPlayer:SetScoringScenario2(iCities);

						local iFaith = pReligion:GetFaithYield();
						pPlayer:SetScoringScenario3(iFaith);
					end
				end
			end
		end
	end
end

-- ===========================================================================
function SetPlayerCitiesReligion(pPlayer, religion, cityState)
	if(religion == -1) then
		return;
	end
		
	if(cityState == true) then
		local pCity:table = pPlayer:GetCities():GetCapitalCity();
		if(pCity ~= nil) then
		--print("Player Id: ", iPlayer);
			pCity:GetReligion():SetAllCityToReligion(religion);
		end
	else
		local pPlayerCities:table = pPlayer:GetCities();
		for i, pCity in pPlayerCities:Members() do
			if(pCity ~= nil) then
				local pReligion = pCity:GetReligion();
				if(pReligion ~= nil) then
					pReligion:SetAllCityToReligion(religion);
				else
					--print("City Religion is nil");
				end
			else
				--print("City is nil and ID is ", iCity);
			end
		end
	end
end

-- ===========================================================================
GameEvents.OnPlayerGaveInfluenceToken.Add(function (majorId, minorId, iAmount)
	local pPlayer1 = Players[majorId];
	local pPlayer2= Players[minorId];
	if (pPlayer1 ~= nil and pPlayer2 ~= nil) then
		local religion = -1;
		local iPressure = 300 * iAmount;
		local config = PlayerConfigurations[majorId];
		local pCity:table = pPlayer2:GetCities():GetCapitalCity();

		if(pCity ~= nil) then
			if(GameInfo.Civilizations["CIVILIZATION_CHINA"].Hash == config:GetCivilizationTypeID()) then 
				religion = GameInfo.Religions["RELIGION_TAOISM"].Index;
			elseif(GameInfo.Civilizations["CIVILIZATION_CHOLA"].Hash == config:GetCivilizationTypeID()) then
				religion = GameInfo.Religions["RELIGION_CUSTOM_1"].Index;
			elseif(GameInfo.Civilizations["CIVILIZATION_GHAZNAVID"].Hash == config:GetCivilizationTypeID()) then 
				religion = GameInfo.Religions["RELIGION_ISLAM"].Index;
			elseif(GameInfo.Civilizations["CIVILIZATION_INDONESIA"].Hash == config:GetCivilizationTypeID()) then 
				religion = GameInfo.Religions["RELIGION_CUSTOM_2"].Index;
			elseif(GameInfo.Civilizations["CIVILIZATION_KHMER"].Hash == config:GetCivilizationTypeID()) then 
				religion = GameInfo.Religions["RELIGION_CUSTOM_3"].Index;
			elseif(GameInfo.Civilizations["CIVILIZATION_PALA"].Hash == config:GetCivilizationTypeID()) then 
				religion = GameInfo.Religions["RELIGION_CUSTOM_4"].Index;
			elseif(GameInfo.Civilizations["CIVILIZATION_TUFAN"].Hash == config:GetCivilizationTypeID()) then 
				religion = GameInfo.Religions["RELIGION_CUSTOM_5"].Index;
			end

			pCity:GetReligion():AddReligiousPressure(8, religion, iPressure, majorId);
		end
	end
end);

GameEvents.PlayerTurnStarted.Add(OnPlayerTurnActivated);
LuaEvents.NewGameInitialized.Add(InitializeNewGame);
Initialize();



		