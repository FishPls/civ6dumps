--[[
-- Created by Keaton VanAauken on Aug 23 2017
-- Copyright (c) Firaxis Games
--]]
-- ===========================================================================
-- INCLUDE BASE FILE
-- ===========================================================================
include("CityStates");

-- ===========================================================================
--	CONSTANTS
-- ===========================================================================
local MIN_ENVOY_TOKENS_SUZERAIN			:number = 3;	--TODO: expose via game core
local NUM_ENVOY_TOKENS_FOR_FIRST_BONUS	:number = 1;
local NUM_ENVOY_TOKENS_FOR_SECOND_BONUS	:number = 3;
local NUM_ENVOY_TOKENS_FOR_THIRD_BONUS	:number = 6;

-- ===========================================================================
--	CACHE BASE FUNCTIONS
-- ===========================================================================
BASE_GetBonusText = GetBonusText;
BASE_GetCityStateType = GetCityStateType;
BASE_GetTypeName = GetTypeName;
BASE_GetBonusIconAtlasPieces = GetBonusIconAtlasPieces;
BASE_GetTypeTooltip = GetTypeTooltip;

-- ===========================================================================
function GetBonusIconAtlasPieces( kCityState:table, size:number )			
	if kCityState.Type == "MARITIME" then
		return IconManager:FindIconAtlas("ICON_ENVOY_BONUS_MARITIME", size); 
	end

	return BASE_GetBonusIconAtlasPieces( kCityState, size );
end

-- ===========================================================================
function GetTypeName( kCityState:table )
	if kCityState.Type == "MARITIME" then
		return Locale.Lookup("LOC_CITY_STATES_SCENARIO_TYPE_MARITIME");
	end

	return BASE_GetTypeName( kCityState );
end

-- ===========================================================================
function GetTypeTooltip( kCityState:table )
	if kCityState.Type == "MARITIME" then
		return "LOC_CITY_STATES_SCENARIO_TYPE_MARITIME";
	end
	return BASE_GetTypeTooltip( kCityState );
end

-- ===========================================================================
function GetCityStateType( playerID:number )

	local cityStateType	:string = "";
	local leader		:string = PlayerConfigurations[ playerID ]:GetLeaderTypeName();
	local leaderInfo	:table	= GameInfo.Leaders[leader];
	if (leader == "LEADER_MINOR_CIV_MARITIME" or leaderInfo.InheritFrom == "LEADER_MINOR_CIV_MARITIME") then				
		return "MARITIME";
	end

	return BASE_GetCityStateType( playerID );
end

-- ===========================================================================
function GetBonusText( playerID:number, envoyTokenNum:number )
	local leader	:string = PlayerConfigurations[playerID]:GetLeaderTypeName();
	local leaderInfo:table	= GameInfo.Leaders[leader];
	if leaderInfo == nil or leaderInfo.InheritFrom == nil then
		UI.DataError("Cannot determine the type of city state bonus for player #: "..tostring(playerID) );
		return "UNKNOWN";
	end
	
	if (leader == "LEADER_MINOR_CIV_MARITIME" or leaderInfo.InheritFrom == "LEADER_MINOR_CIV_MARITIME") then
		local bonusTypeText = "";
		local bonusDetailsText = "";

		if envoyTokenNum == NUM_ENVOY_TOKENS_FOR_FIRST_BONUS then 
			bonusTypeText = Locale.Lookup("LOC_MINOR_CIV_SMALL_INFLUENCE_ENVOYS");
			bonusDetailsText = Locale.Lookup("LOC_MINOR_CIV_SCENARIO_MARITIME_TRAIT_SMALL_INFLUENCE_BONUS");
		elseif envoyTokenNum == NUM_ENVOY_TOKENS_FOR_SECOND_BONUS then
			bonusTypeText = Locale.Lookup("LOC_MINOR_CIV_MEDIUM_INFLUENCE_ENVOYS");
			bonusDetailsText = Locale.Lookup("LOC_MINOR_CIV_SCENARIO_MARITIME_TRAIT_MEDIUM_INFLUENCE_BONUS");
		elseif envoyTokenNum == NUM_ENVOY_TOKENS_FOR_THIRD_BONUS then
			bonusTypeText = Locale.Lookup("LOC_MINOR_CIV_LARGE_INFLUENCE_ENVOYS");
			bonusDetailsText = Locale.Lookup("LOC_MINOR_CIV_SCENARIO_MARITIME_TRAIT_LARGE_INFLUENCE_BONUS");
		else 
			UI.DataError("Unknown envoy number for city-state type bonus: " .. tostring(envoyTokenNum));
		end

		return bonusTypeText, bonusDetailsText; 
	end

	return BASE_GetBonusText( playerID, envoyTokenNum );
end