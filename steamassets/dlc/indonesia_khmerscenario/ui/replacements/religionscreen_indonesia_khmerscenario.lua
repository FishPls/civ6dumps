--[[
-- Created by Keaton VanAauken on Oct 3 2017
-- Copyright (c) Firaxis Games
--]]
-- ===========================================================================
-- INCLUDE BASE FILE
-- ===========================================================================
include("ReligionScreen");

-- ===========================================================================
--	CACHE BASE FUNCTIONS
-- ===========================================================================
BASE_AddLockedBeliefs = AddLockedBeliefs;

-- ===========================================================================
function AddLockedBeliefs(religion)
	-- In this scenario all beliefs are preset and already unlocked
end