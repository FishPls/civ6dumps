--[[
-- Created by Keaton VanAauken on Sep 29 2017
-- Copyright (c) Firaxis Games
--]]
-- ===========================================================================
-- INCLUDE BASE FILE
-- ===========================================================================
include("TradeOverview");
-- ===========================================================================
--	CACHE BASE FUNCTIONS
-- ===========================================================================
BASE_GetCityStateIcon = GetCityStateIcon;

-- ===========================================================================
function GetCityStateIcon(leaderName:string, leaderInfo:table)
	if (leader == "LEADER_MINOR_CIV_MARITIME" or leaderInfo.InheritFrom == "LEADER_MINOR_CIV_MARITIME") then
		return "ICON_CITYSTATE_MARITIME";
	end

	return BASE_GetCityStateIcon(leaderName, leaderInfo);
end