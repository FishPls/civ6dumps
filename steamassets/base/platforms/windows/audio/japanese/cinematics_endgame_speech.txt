Event	ID	Name			Wwise Object Path	Notes
	135590464	Stop_Cinematic_Endgame_Science			\Cinematics\Endgame\Stop_Cinematic_Endgame_Science	
	744417603	Stop_Cinematic_Endgame_Defeat			\Cinematics\Endgame\Stop_Cinematic_Endgame_Defeat	
	1181145526	Stop_Cinematic_Endgame_Domination			\Cinematics\Endgame\Stop_Cinematic_Endgame_Domination	
	1439142889	Stop_Cinematic_Endgame_Time			\Cinematics\Endgame\Stop_Cinematic_Endgame_Time	
	2226179010	Play_Cinematic_Endgame_Culture			\Cinematics\Endgame\Play_Cinematic_Endgame_Culture	
	2408214944	Stop_Cinematic_Endgame_Culture			\Cinematics\Endgame\Stop_Cinematic_Endgame_Culture	
	2461584252	Play_Cinematic_Endgame_Domination			\Cinematics\Endgame\Play_Cinematic_Endgame_Domination	
	2591573098	Play_Cinematic_Endgame_Science			\Cinematics\Endgame\Play_Cinematic_Endgame_Science	
	3678225813	Play_Cinematic_Endgame_Defeat			\Cinematics\Endgame\Play_Cinematic_Endgame_Defeat	
	3726950503	Play_Cinematic_Endgame_Time			\Cinematics\Endgame\Play_Cinematic_Endgame_Time	
	3867367741	Play_Cinematic_Endgame_Religion			\Cinematics\Endgame\Play_Cinematic_Endgame_Religion	
	4054125047	Stop_Cinematic_Endgame_Religion			\Cinematics\Endgame\Stop_Cinematic_Endgame_Religion	

Streamed Audio	ID	Name	Audio source file	Generated audio file	Wwise Object Path	Notes
	25271191	Cinematic_Endgame_Domination_DX	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\Voices\Japanese\Cinematic_Endgame_Domination_DX_9E69E797.wem	25271191.wem	\Actor-Mixer Hierarchy\Default Work Unit\Cinematics\Cinematics\Cinematics_Speech\Endgame\Cinematic_Endgame_Domination_DX	
	202639014	Cinematic_Endgame_Time_DX	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\Voices\Japanese\Cinematic_Endgame_Time_DX_9E69E797.wem	202639014.wem	\Actor-Mixer Hierarchy\Default Work Unit\Cinematics\Cinematics\Cinematics_Speech\Endgame\Cinematic_Endgame_Time_DX	
	373329477	Cinematic_Endgame_Religion_DX	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\Voices\Japanese\Cinematic_Endgame_Religion_DX_9E69E797.wem	373329477.wem	\Actor-Mixer Hierarchy\Default Work Unit\Cinematics\Cinematics\Cinematics_Speech\Endgame\Cinematic_Endgame_Religion_DX	
	510955836	Cinematic_Endgame_Defeat_DX	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\Voices\Japanese\Cinematic_Endgame_Defeat_DX_9E69E797.wem	510955836.wem	\Actor-Mixer Hierarchy\Default Work Unit\Cinematics\Cinematics\Cinematics_Speech\Endgame\Cinematic_Endgame_Defeat_DX	
	713753543	Cinematic_Endgame_Science_DX	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\Voices\Japanese\Cinematic_Endgame_Science_DX_9E69E797.wem	713753543.wem	\Actor-Mixer Hierarchy\Default Work Unit\Cinematics\Cinematics\Cinematics_Speech\Endgame\Cinematic_Endgame_Science_DX	
	1035116479	Cinematic_Endgame_Culture_DX	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\Voices\Japanese\Cinematic_Endgame_Culture_DX_9E69E797.wem	1035116479.wem	\Actor-Mixer Hierarchy\Default Work Unit\Cinematics\Cinematics\Cinematics_Speech\Endgame\Cinematic_Endgame_Culture_DX	

