Event	ID	Name			Wwise Object Path	Notes
	20563735	Receive_Culture_Boost			\UI\Receive_Culture_Boost	
	78569253	Notification_Border_Growth			\Notifications\Notification_Border_Growth	
	262954207	Notification_Misc_Negative			\Notifications\Notification_Misc_Negative	
	282381055	Notification_Player_Eliminated			\Notifications\Notification_Player_Eliminated	
	311308633	Receive_Gold_Boost			\UI\Receive_Gold_Boost	
	326108872	Receive_EXP_Boost			\UI\Receive_EXP_Boost	
	487792908	Alert_Advisor			\Notifications\Alert_Advisor	
	556385916	Notification_Discover_Barbarians			\Notifications\Notification_Discover_Barbarians	
	649907108	Drop_Dust_VFX			\Notifications\Drop_Dust_VFX	
	714349821	Alert_Positive			\Notifications\Alert_Positive	
	743061647	Build_Dust_Small_VFX			\Notifications\Build_Dust_Small_VFX	
	754297104	Form_Corps_VFX			\Notifications\Form_Corps_VFX	
	755801417	Discover_Natural_Wonder_VFX			\Notifications\Discover_Natural_Wonder_VFX	
	820588504	Play_ADVISOR_LINE_FTUE_1ALT_2			\Speech_Localized\Advisor\FTUE\Play_ADVISOR_LINE_FTUE_1ALT_2	
	820588507	Play_ADVISOR_LINE_FTUE_1ALT_1			\Speech_Localized\Advisor\FTUE\Play_ADVISOR_LINE_FTUE_1ALT_1	
	879279911	Notification_Misc_Neutral			\Notifications\Notification_Misc_Neutral	
	881080770	Set_View_2D			\Notifications\Set_View_2D	
	897858397	Set_View_3D			\Notifications\Set_View_3D	
	1069653675	Notification_New_Production			\Notifications\Notification_New_Production	
	1139428905	Unit_Promotion_Available			\Unit Events Common\Unit_Promotion_Available	
	1261163951	Play_MP_Host_Migration			\Multiplayer\Play_MP_Host_Migration	
	1288869233	Notification_Enemy_In_Borders			\Notifications\Notification_Enemy_In_Borders	
	1388214529	Notification_Other_Civ_Build_Wonder			\Notifications\Notification_Other_Civ_Build_Wonder	
	1422267220	Game_Begin_Button_Appear			\UI\Game_Begin_Button_Appear	
	1509729358	Notification_Discover_Natural_Wonder			\Notifications\Notification_Discover_Natural_Wonder	
	1527895060	Player_Unit_Captured			\Unit Events Common\Player_Unit_Captured	
	1620237916	Pride_Moment			\XP1\XP1_UI\Pride_Moment\Pride_Moment	
	1696706774	Receive_Religion_Boost			\UI\Receive_Religion_Boost	
	1738580231	Receive_Map_Boost			\UI\Receive_Map_Boost	
	1965219674	Receive_Production_Boost			\UI\Receive_Production_Boost	
	2076154416	Birth_Unit_VFX			\Notifications\Birth_Unit_VFX	
	2100535000	Notification_Espionage_Op_Failed			\Notifications\Notification_Espionage_Op_Failed	
	2377284971	Purchase_Tile			\UI\Purchase_Tile	
	2410426902	Notification_Great_Person_Available			\Notifications\Notification_Great_Person_Available	
	2457598895	Stop_Unit_Movement_Master			\Unit Events 3D\Stop_Unit_Movement_Master	
	2548518184	UI_Governor_Assign			\XP1\XP1_UI\Governors\UI_Governor_Assign	
	2661477235	Receive_HP_Boost			\UI\Receive_HP_Boost	
	3021559694	Game_Begin_Whoosh			\Notifications\Game_Begin_Whoosh	
	3074328029	New_Era_Reached			\Notifications\New_Era_Reached	
	3075902954	Notification_New_Tech			\Notifications\Notification_New_Tech	
	3267959079	Receive_Tech_Boost			\UI\Receive_Tech_Boost	
	3295098398	Start_Trade_Route			\UI\Start_Trade_Route	
	3335285995	Notification_Rebellion			\Notifications\Notification_Rebellion	
	3449136989	Game_Begin_Button_Click			\Notifications\Game_Begin_Button_Click	
	3465355425	Alert_Neutral			\Notifications\Alert_Neutral	
	3474057652	Notification_Espionage_Op_Success			\Notifications\Notification_Espionage_Op_Success	
	3500178301	Notification_Player_Build_Wonder			\Notifications\Notification_Player_Build_Wonder	
	3511928128	Notification_New_Civic			\Notifications\Notification_New_Civic	
	3601040241	Alert_Negative			\Notifications\Alert_Negative	
	3677132230	Notification_Unit_Death			\Notifications\Notification_Unit_Death	
	3733067743	Notification_New_Religion			\Notifications\Notification_New_Religion	
	3879413670	Notification_Discover_Ruins			\Notifications\Notification_Discover_Ruins	
	3977158900	Notification_War_Declared			\Notifications\Notification_War_Declared	
	4014632923	Build_Dust_VFX			\Notifications\Build_Dust_VFX	
	4081416206	Unit_Promoted			\Unit Events Common\Unit_Promoted	
	4216165771	Notification_Misc_Positive			\Notifications\Notification_Misc_Positive	

Switch Group	ID	Name			Wwise Object Path	Notes
	373350295	Game_Location			\Default Work Unit\Game_Location	

Switch	ID	Name	Switch Group			Notes
	712573564	Ancient_Era	Game_Location			
	984117391	Modern_Era	Game_Location			
	2183131863	Industrial_Era	Game_Location			
	2654112219	Medieval_Era	Game_Location			

State Group	ID	Name			Wwise Object Path	Notes
	3441926412	Game_Views			\Default Work Unit\Game_Views	

State	ID	Name	State Group			Notes
	138330285	Lens	Game_Views			
	319258907	Paused	Game_Views			
	706390200	Normal_View	Game_Views			
	748895195	None	Game_Views			
	2005704188	Main_Menu	Game_Views			
	2664707777	Leader_Screen	Game_Views			
	4264815106	Endgame	Game_Views			

Custom State	ID	Name	State Group	Owner		Notes
	803556523	Main_Menu	Game_Views	\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\3D Unit Sounds\3D Unit SFX		
	850765911	Main_Menu	Game_Views	\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications		

Game Parameter	ID	Name			Wwise Object Path	Notes
	587070286	Camera_Height			\Default Work Unit\Camera_Height	

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	3325497	Natural_Wonder_Discovered_Magic	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Natural_Wonder_Discovered_Magic_9E69E797_913C84C6.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\VFX_Driven\DiscoverNaturalWonder\Natural_Wonder_Discovered_Magic		1200192
	39412189	DiscoverGoodyHut_02	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\DiscoverGoodyHut_02_9E69E797_198568AF.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\Player Notifications\Alerts_Primary\NotificationDiscoverRuins\DiscoverGoodyHut_02		856128
	46898955	NotificationWarDeclared	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\DeclareWar_9E69E797_39B5F206.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\Player Notifications\NotificationWarDeclared		1062228
	50247175	FormCorpVFX_0	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\FormCorpVFX_0_9E69E797_913C84C6.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\VFX_Driven\FormCorp_VisEffect\FormCorpVFX_0		476112
	82367574	NotificationEspionageOpFailed	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\NotificationSpyFail_9E69E797_C04556BA.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\Player Notifications\Alerts_Primary\NotificationEspionageOpFailed		637812
	106831248	Alert_Advisor	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Alert_Advisor_9E69E797_C04556BA.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\Player Notifications\Alerts_Primary\Alert_Advisor		286548
	109426304	Build_Dust_Small_0	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Build_Dust_Small_0_9E69E797_913C84C6.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\VFX_Driven\Build_Dust_VisEffect_Small\Build_Dust_Small_0		370432
	115827728	BirthUnit2-001	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\BirthUnit2-001_9E69E797_913C84C6.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\VFX_Driven\BirthUnit\BirthUnit2-001		557024
	139398678	Build_Dust_Small_1	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Build_Dust_Small_1_9E69E797_913C84C6.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\VFX_Driven\Build_Dust_VisEffect_Small\Build_Dust_Small_1		278072
	142273397	DiscoverGoodyHut_04	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\DiscoverGoodyHut_04_9E69E797_198568AF.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\Player Notifications\Alerts_Primary\NotificationDiscoverRuins\DiscoverGoodyHut_04		860224
	153080811	Game_Begin_Button_Appear	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Game_Begin_Button_Appear_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\UI\UI\Game_Begin\Game_Begin_Button_Appear		719120
	191739437	UnitPromoted	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\UnitPromoted4_569C70B8_0851AA73.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\ForcedUI\Promotions\UnitPromoted		340192
	239866514	Notification_Generic_Neutral	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Notification_Generic_Neutral_97136F8B_C04556BA.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\Player Notifications\Notifications_Generic\Notification_Generic_Neutral		272388
	261103800	FormCorpVFX_1	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\FormCorpVFX_1_9E69E797_913C84C6.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\VFX_Driven\FormCorp_VisEffect\FormCorpVFX_1		417780
	264781842	BirthUnit2-000	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\BirthUnit2-000_9E69E797_913C84C6.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\VFX_Driven\BirthUnit\BirthUnit2-000		549496
	265184192	Natural_Wonder_Discovered_Glass_R	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Natural_Wonder_Discovered_Glass_9E69E797_913C84C6.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\VFX_Driven\DiscoverNaturalWonder\Natural_Wonder_Discovered_Glass_R		1202240
	295875066	Alert_Positive	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Alert_Positive_9E69E797_C04556BA.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\Player Notifications\Alerts_Primary\Alert_Positive		307784
	299365666	UnitPromotionAvailable	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\UnitPromotionAvailable_EFC065BD_913C84C6.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\ForcedUI\Promotions\UnitPromotionAvailable		426216
	314101473	BoostMap	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\BoostMap_2AC23C44_913C84C6.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\ForcedUI\Boosts\BoostMap		339276
	321890402	NotificationDiscoverBarbarians	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\DiscoverBarbarians_9E69E797_C04556BA.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\Player Notifications\Alerts_Primary\NotificationDiscoverBarbarians		606780
	402632172	NotificationNewReligion	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\NotificationNewReligion_9E69E797_C04556BA.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\Player Notifications\TurnBlockers_Secondary\NotificationNewReligion		1183592
	419771714	DiscoverNaturalWonder	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Notification_Generic_Positive_77D7FA2B_913C84C6.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\ForcedUI\DiscoverNaturalWonder		544088
	426453395	Notification_Generic_Positive	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Notification_Generic_Positive_2D424C52_C04556BA.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\Player Notifications\Notifications_Generic\Notification_Generic_Positive		554784
	441492253	Build_Government_02	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Build_Government_02_6162C132_913C84C6.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\ForcedUI\Boosts\AssignGovernor\Build_Government_02		396816
	454771501	Notification_Generic_Negative	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Notification_Generic_Negative_9E69E797_C04556BA.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\Player Notifications\Notifications_Generic\Notification_Generic_Negative		607968
	460640268	DiscoverGoodyHut_05	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\DiscoverGoodyHut_05_9E69E797_198568AF.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\Player Notifications\Alerts_Primary\NotificationDiscoverRuins\DiscoverGoodyHut_05		860224
	480960703	Build_Dust_VisEffect_1	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Build_Dust_VisEffect_1_9E69E797_913C84C6.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\VFX_Driven\Build_Dust_VisEffect\Build_Dust_VisEffect_1		410268
	491466986	Build_Dust_VisEffect_0	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Build_Dust_VisEffect_0_9E69E797_913C84C6.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\VFX_Driven\Build_Dust_VisEffect\Build_Dust_VisEffect_0		402728
	516557116	GoldBoost	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\GoldBoost_78456D41_913C84C6.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\ForcedUI\Boosts\GoldBoost		272812
	517719358	Notification_New_Civic_NEW	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Notification_New_Civic_NEW_613C249B_C04556BA.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\Player Notifications\TurnBlockers_Secondary\Notification_New_Civic_NEW		954736
	541500929	Game_Begin_Button_Click	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Game_Begin_Button_Click_55FE1C09.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\UI\UI\Game_Begin\Game_Begin_Button_Click		766764
	562168523	NotificationEspionageOpSuccess	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\NotificationSpySuccess_9E69E797_C04556BA.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\Player Notifications\Alerts_Primary\NotificationEspionageOpSuccess		439076
	584744369	DiscoverGoodyHut_01	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\DiscoverGoodyHut_01_9E69E797_198568AF.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\Player Notifications\Alerts_Primary\NotificationDiscoverRuins\DiscoverGoodyHut_01		860224
	615403655	NotificationPlayerBuiltWonder	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\NotificationPlayerWonderBuilt_9E69E797_A219FD90.wem		\Actor-Mixer Hierarchy\Default Work Unit\__StagedDeletion\NotificationPlayerBuiltWonder		1223916
	623959084	Notification_Rebellion_1	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Notification_Rebellion_1_9E69E797_C04556BA.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\Player Notifications\Alerts_Primary\Notification_Rebellion\Notification_Rebellion_1		675560
	636883856	CultureBoost	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\CultureBoost_9E69E797_913C84C6.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\ForcedUI\Boosts\CultureBoost		569184
	637211709	DiscoverGoodyHut_06	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\DiscoverGoodyHut_06_9E69E797_198568AF.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\Player Notifications\Alerts_Primary\NotificationDiscoverRuins\DiscoverGoodyHut_06		860224
	652806158	NotificationBorderGrowth	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\NotificationBorderGrowth_9E69E797_C04556BA.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\Player Notifications\NotificationBorderGrowth		333952
	699846797	ReligionBoost	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\ReligionBoost_C8AD5726_913C84C6.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\ForcedUI\Boosts\ReligionBoost		635680
	700080607	NotificationOtherCivBuiltWonder	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\OtherCivBuiltWonder_3959E535_C04556BA.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\Player Notifications\NotificationOtherCivBuiltWonder		487600
	712821035	ProductionBoost	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\ProductionBoost_9E4C0981_913C84C6.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\ForcedUI\Boosts\ProductionBoost		257836
	724383794	NotificationEnemyInBorders	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\EnemyInBorders_9E69E797_C04556BA.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\Player Notifications\NotificationEnemyInBorders		878440
	729738820	Drop_Dust_VisEffect_1	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Drop_Dust_VisEffect_1_9E69E797_913C84C6.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\VFX_Driven\Drop_Dust_VisEffect\Drop_Dust_VisEffect_1		451492
	733695413	Alert_Negative	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Alert_Negative_EB131F61_C04556BA.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\Player Notifications\Alerts_Primary\Alert_Negative		239892
	770837265	NotificationNewTech	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\NewTechnology_9E69E797_C04556BA.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\Player Notifications\TurnBlockers_Secondary\NotificationNewTech		711312
	818307086	Notification_Rebellion_2	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Notification_Rebellion_2_9E69E797_C04556BA.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\Player Notifications\Alerts_Primary\Notification_Rebellion\Notification_Rebellion_2		669916
	859622179	BirthUnit-000	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\BirthUnit-000_9E69E797_913C84C6.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\VFX_Driven\BirthUnit\BirthUnit-000		364708
	873887511	BirthUnit-001	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\BirthUnit-001_9E69E797_913C84C6.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\VFX_Driven\BirthUnit\BirthUnit-001		452008
	875166986	TechBoost	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\TechBoost_FC4F24F8_913C84C6.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\ForcedUI\Boosts\TechBoost		356844
	893473348	Alert_Neutral	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Alert_Neutral_2E5E6DE8_C04556BA.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\Player Notifications\Alerts_Primary\Alert_Neutral		124216
	914226250	Pride_Moment	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Pride_Moment_82F67D86_913C84C6.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\ForcedUI\Boosts\PrideMoment\Pride_Moment		1335872
	928826498	EXPBoost2	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\EXPBoost2_1F8542B4_913C84C6.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\ForcedUI\Promotions\EXPBoost2		302128
	931348453	Drop_Dust_VisEffect_0	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Drop_Dust_VisEffect_0_9E69E797_913C84C6.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\VFX_Driven\Drop_Dust_VisEffect\Drop_Dust_VisEffect_0		430716
	932707187	BoostBed-01	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\BoostBed-01_9E69E797_913C84C6.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\ForcedUI\Boosts\BoostBed-01		556776
	937954021	DiscoverGoodyHut_03	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\DiscoverGoodyHut_03_9E69E797_198568AF.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\Player Notifications\Alerts_Primary\NotificationDiscoverRuins\DiscoverGoodyHut_03		856128
	951429657	Notification_Rebellion_0	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Notification_Rebellion_0_9E69E797_C04556BA.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\Player Notifications\Alerts_Primary\Notification_Rebellion\Notification_Rebellion_0		1057524
	986099324	NotificationNewProduction	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\NewProduction_1D996CE0_C04556BA.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\Player Notifications\TurnBlockers_Secondary\NotificationNewProduction		622656
	995869317	Notifications_WhooshHit_Base	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\Notifications_WhooshHit_Base_9E69E797_C04556BA.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\Player Notifications\Notifications_WhooshHit_Base		416684
	1017680092	NotificationImpactSweetener	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\NotificationImpactSweetener_D749E2AF_C04556BA.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\Notifications\Notifications\Player Notifications\NotificationImpactSweetener		301536
	1070504842	Game_Begin_Whoosh	D:\project\depot\Civ6\Original\Civ6\Wwise\Civ6 Project\.cache\Linux\SFX\LeaderScreenEnter_9E69E797.wem		\Actor-Mixer Hierarchy\Default Work Unit\Game_Cues\UI\UI\Game_Begin\Whoosh\Game_Begin_Whoosh		577716

