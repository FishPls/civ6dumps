#!/bin/bash


SAVEIFS=$IFS
IFS=$(echo -en "\n\b")

directories="~/Diff/Sid Meier's Civilization VI
~/.steam/steam/steamapps/common/Sid Meier's Civilization VI"

for dire in $directories; do
  for filename in `ls $dire | grep '.*\.so*' | grep -v '\.txt$'`; do
#    echo $filename
#    echo $dire
  	echo $dire/$filename
    strings "$dire/$filename" > "/${dire}/${filename}.txt"
  done
  strings "~/Diff/Sid Meier's Civilization VI/Civ6" > "~/Diff/Sid Meier's Civilization VI/Civ6.txt"
  strings "~/.steam/steam/steamapps/common/Sid Meier's Civilization VI/Civ6" > "~/.steam/steam/steamapps/common/Sid Meier's Civilization VI/Civ6.txt"
done
echo "Strings dumped successfully!"
